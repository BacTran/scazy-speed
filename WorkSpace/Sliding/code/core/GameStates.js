define([
    'code/states/BootState',
    'code/states/PreloaderState',
    'code/states/MenuState',
    'code/states/InGameState',
    'code/states/IntroState'
    ],
function (BootState, PreloaderState, MenuState, InGameState, IntroState){
    var GameStates = function (game){
        game.state.add('Boot', BootState);
        game.state.add('Preloader', PreloaderState);
        game.state.add('MainMenu', MenuState);
        game.state.add('InGame', InGameState);
        game.state.add('Intro', IntroState);
    };

    return GameStates;
});