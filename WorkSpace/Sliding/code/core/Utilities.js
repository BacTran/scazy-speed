define([

    ],
function (){
    var Utilities = function (){

    };

    Utilities.prototype = {

        getViewport: function(){
            // get users screen size
            var h = 720;
            var w = 1280;
            return {width: w, height: h};
        }
    };

    return Utilities = new Utilities();
});