require.config({
  paths: {
    underscore      : 'libs/underscore-min',
    Backbone        : 'libs/backbone-min',
    BaseClass       : 'libs/JSCore/BaseClass',
    Phaser          : 'libs/phaser/phaser.min',
    jQuery          : 'libs/JQuery/jquery-1.11.1.min',
    jQueryui        : 'libs/JQuery/jquery-ui-1.9.2.custom',
    jsviews         : 'libs/JQuery/jsviews',
    jsrender        : 'libs/JQuery/jsrender',
    swig            : 'libs/swig.min',
    JQueryPlugins   : 'libs/JQuery/'
  },
  shim:{
    'jQuery': {
      exports: 'jQuery'
    },
    'BaseClass': {
      exports: 'BaseClass'
    },
    'Backbone': {
      deps: ['underscore', 'jQuery'],
      exports: 'Backbone'
    },
    'Backbone_Deep': {
      deps: ['Backbone'],
    },
    'underscore': {
      exports: '_'
    },
    'bootstrap': {
      deps: ['underscore', 'jQuery'],
    },
    'jQueryui': {
      deps: ['jQuery']
    },
    'jsviews': {
      deps: ['jQuery']
    },
    'jsrender': {
      deps: ['jQuery']
    }
  }
});

require([
  'Phaser',
  'code/core/Utilities',
  'code/core/GameStates'
], function (Phaser, Utilities, GameStates) {
  var viewport = Utilities.getViewport();
  var game = new Phaser.Game(viewport.width, viewport.height, Phaser.CANVAS, "viewport");
  game.States = new GameStates(game);
  game.state.start('Boot');
});