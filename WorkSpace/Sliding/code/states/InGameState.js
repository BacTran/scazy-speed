define(['underscore',
        'Phaser'
    ],
function (_, Phaser){
    var PIECE_WIDTH = 200,
        PIECE_HEIGHT = 200,
        BOARD_COLS,
        BOARD_ROWS;

    var UP, 
    	DOWN, 
    	LEFT, 
    	RIGHT,
    	ENTER,
    	EXIT,
    	REPLAY;

    var piecesGroup,
        piecesAmount,
        shuffledIndexArray = [];
    var game;
	
	var selected;
	var pieces;
	var bgMain;

	var btnThoat,
		btnChoiLai;

	var second,
		ten_second,
		ngat,
		minute,
		ten_minute;

	var second_value,
		ten_second_value,
		minute_value,
		ten_minute_value;

	var timeGroup;

    var InGameState = function (g){
        this.game = g;
        game = g;
        // this.player;
    }

    InGameState.prototype = {		
        create: function (){
			bgMain = this.game.add.sprite(0, 0, 'bgmain');
        	var piecesIndex = 0,
        	i, j,
        	piece;

        	BOARD_COLS = Math.floor(800 / PIECE_WIDTH);
    		BOARD_ROWS = Math.floor(600 / PIECE_HEIGHT);
    		piecesAmount = BOARD_COLS * BOARD_ROWS;
		
		    UP = game.input.keyboard.addKey(Phaser.Keyboard.UP);
		    UP.onDown.add(moveUpSelected, this);
		    DOWN = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
		    DOWN.onDown.add(moveDownSelected, this);
		    LEFT = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
		    LEFT.onDown.add(moveLeftSelected, this);
		    RIGHT = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
		    RIGHT.onDown.add(moveRightSelected, this);
		    ENTER = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
		    ENTER.onDown.add(letMovePiece, this);
		    EXIT = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
		    EXIT.onDown.add(letExit, this);
		    REPLAY = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
		    REPLAY.onDown.add(letReplay, this);

		    btnThoat = this.game.add.sprite(0, 0, 'btnThoat');
		    btnThoat.x = 1090;
		    btnThoat.y = 605;
		    btnChoiLai = this.game.add.sprite(0, 0, 'btnChoiLai');
		    btnChoiLai.x = 1090;
		    btnChoiLai.y = 500;

		    shuffledIndexArray = createShuffledIndexArray();

		    piecesGroup = game.add.group();
		    piecesGroup.x = 240;
		    piecesGroup.y = 60;

		    timeGroup = game.add.group();
		    timeGroup.x = 1100;
		    timeGroup.y = 100;

		    for (i = 0; i < BOARD_ROWS; i++)
		    {
		        for (j = 0; j < BOARD_COLS; j++)
		        {
		            if (shuffledIndexArray[piecesIndex]) {
		                piece = piecesGroup.create(j * PIECE_WIDTH, i * PIECE_HEIGHT, "pz_img1", shuffledIndexArray[piecesIndex]);
		            }
		            else { //initial position of black piece
		                piece = piecesGroup.create(j * PIECE_WIDTH, i * PIECE_HEIGHT);
		                piece.black = true;
		            }
		            piece.name = 'piece' + i.toString() + 'x' + j.toString();
		            piece.currentIndex = piecesIndex;
		            piece.destIndex = shuffledIndexArray[piecesIndex];
		            //piece.inputEnabled = true;
		            //piece.events.onInputDown.add(selectPiece, this);
		            piece.posX = j;
		            piece.posY = i;
		            piecesIndex++;
		        }
		    }
	
			selected = this.game.add.sprite(0, 0, 'choice');	
			piecesGroup.add(selected);

			ten_minute = this.game.add.text(0, 0, '0', { font: "40px Arial", fill: "#ffffff", align: "center" });
			timeGroup.add(ten_minute);
			minute = this.game.add.text(0, 0, '0', { font: "40px Arial", fill: "#ffffff", align: "center" });
			timeGroup.add(minute);
			minute.x = 30;
			ngat = this.game.add.text(0, 0, ':', { font: "40px Arial", fill: "#ffffff", align: "center" });
			timeGroup.add(ngat);
			ngat.x = 60;
			ten_second = this.game.add.text(0, 0, '0', { font: "40px Arial", fill: "#ffffff", align: "center" });
			timeGroup.add(ten_second);
			ten_second.x = 90;
			second = this.game.add.text(0, 0, '0', { font: "40px Arial", fill: "#ffffff", align: "center" });
			timeGroup.add(second);
			second.x = 120;

			second_value = 0;
			ten_second_value = 0;
			minute_value = 0;
			ten_minute_value = 0;

			game.time.events.loop(Phaser.Timer.SECOND, updateTime, this);
},

        render: function(){
         this.game.debug.text(this.game.time.fps || '--', 20, 20, "#ff0000");
        },
    };
        
function selectPiece(piece) {
	console.log("select");
    var blackPiece = canMove(piece);

    //if there is a black piece in neighborhood
    if (blackPiece) {
        movePiece(piece, blackPiece);
    }
}

function canMove(piece) {

    var foundBlackElem = false;

    piecesGroup.children.forEach(function(element) {
        if (element.posX === (piece.posX - 1) && element.posY === piece.posY && element.black ||
            element.posX === (piece.posX + 1) && element.posY === piece.posY && element.black ||
            element.posY === (piece.posY - 1) && element.posX === piece.posX && element.black ||
            element.posY === (piece.posY + 1) && element.posX === piece.posX && element.black) {
            foundBlackElem = element;
            return;
        }
    });

    return foundBlackElem;
}

function updateTime(){
	second_value++;

	if(second_value === 10){
		ten_second_value++;
		second_value = 0;
	}

	if(ten_second_value === 6){
		minute_value++;
		ten_second_value = 0;
	}

	if(minute_value === 10){
		ten_minute_value++;
		minute_value = 0;
	}

	timeGroup.children[4].setText('' + second_value);
	timeGroup.children[3].setText('' + ten_second_value);
	timeGroup.children[1].setText('' + minute_value);
	timeGroup.children[0].setText('' + ten_minute_value);
}

function movePiece(piece, blackPiece) {
    var tmpPiece = {
        posX: piece.posX,
        posY: piece.posY,
        currentIndex: piece.currentIndex
    };

    game.add.tween(piece).to({x: blackPiece.posX * PIECE_WIDTH, y: blackPiece.posY * PIECE_HEIGHT}, 300, Phaser.Easing.Linear.None, true);

    //change places of piece and blackPiece
    piece.posX = blackPiece.posX;
    piece.posY = blackPiece.posY;
    piece.currentIndex = blackPiece.currentIndex;
    piece.name ='piece' + piece.posX.toString() + 'x' + piece.posY.toString();

    //piece is the new black
    blackPiece.posX = tmpPiece.posX;
    blackPiece.posY = tmpPiece.posY;
    blackPiece.currentIndex = tmpPiece.currentIndex;
    blackPiece.name ='piece' + blackPiece.posX.toString() + 'x' + blackPiece.posY.toString();

    //after every move check if puzzle is completed
    checkIfFinished();
}

function moveLeftSelected(){
	var posX = selected.x;
	if(posX > 0){
		selected.x = posX - 200;
	}
}

function moveRightSelected(){
	var posX = selected.x;
	if(posX < 600){
		selected.x = posX + 200;
	}
}

function moveUpSelected(){
	var posY = selected.y;
	if(posY > 0){
		selected.y = posY - 200;
	}
}

function moveDownSelected(){
	var posY = selected.y;
	if(posY < 400){
		selected.y = posY + 200;
	}
}

function letMovePiece(){
	var posX = selected.x;
	var posY = selected.y;

	for(var i = 0; i < piecesGroup.children.length; i++){
		if((piecesGroup.children[i].posX * 200) === posX){
			if((piecesGroup.children[i].posY * 200) === posY){
				var blackPiece = canMove(piecesGroup.children[i]);

			    if (blackPiece) {
			        movePiece(piecesGroup.children[i], blackPiece);
			    }
			}
		}
	}
}

function letReplay(){
	console.log("let replay");
	this.create();
}

function letExit(){
	this.state.start('MainMenu');
}

function checkIfFinished() {

    var isFinished = true;

    piecesGroup.children.forEach(function(element) {
        if (element.currentIndex !== element.destIndex) {
            isFinished = false;
            return;
        }
    });

    if (isFinished) {
        showFinishedText();
    }

}

function showFinishedText() {

    var style = { font: "40px Arial", fill: "#000", align: "center"};

    var text = game.add.text(game.world.centerX, game.world.centerY, "Congratulations! \nYou made it!", style);

    text.anchor.set(0.5);

}

function createShuffledIndexArray() {

    var indexArray = [];

    for (var i = 0; i < piecesAmount; i++)
    {
        indexArray.push(i);
    }

    return shuffle(indexArray);

}

function shuffle(array) {

    var counter = array.length,
        temp,
        index;

    while (counter > 0)
    {
        index = Math.floor(Math.random() * counter);

        counter--;

        temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
    
}
    return InGameState;
});