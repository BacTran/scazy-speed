define(['Phaser'],
function (Phaser){
    var PIECE_WIDTH = 200,
        PIECE_HEIGHT = 200;

    var Preloader = function (game){
        this.game = game;
    }

    Preloader.prototype = {
        preload: function (){
            this.stage = {width : this.game.width, height: this.game.height};
            // set background color and preload image
            this.stage.backgroundColor = '#B4D9E7';
            // Load assets
            //background
            this.load.image('starfield', 'resources/images/background/starfield.png');
            this.load.image('menu', 'resources/images/background/menu.jpg');
            this.load.image('intro', 'resources/images/background/intro.jpg');
            //ui
            this.load.spritesheet('btnStart', 'resources/images/ui/btnStart.png', 276, 87);
            this.load.spritesheet('btnIntro', 'resources/images/ui/btnIntro.png', 276, 87);
            this.load.spritesheet('btnHelp', 'resources/images/ui/btnHelp.png', 276, 87);
            //audio
            this.game.load.audio('theme', 'resources/sounds/oedipus_wizball_highscore.mp3');
            this.game.load.audio('pistol', 'resources/sounds/pistol.wav');
            this.game.load.audio('pusher', 'resources/sounds/pusher.wav');
            this.game.load.audio('shot1', 'resources/sounds/shot1.wav');
            //ingame sprites
            this.game.load.spritesheet("pz_img1", "resources/images/entity/pooh1.jpg", PIECE_WIDTH, PIECE_HEIGHT);
			this.game.load.image('choice', 'resources/images/ui/choice.png');
			this.game.load.image('bgmain', 'resources/images/background/main.png');
			this.game.load.image('btnThoat', 'resources/images/ui/btnThoat.png');
			this.game.load.image('btnChoiLai', 'resources/images/ui/btnChoiLai.png');
            //show loading screen
            var bg = this.game.add.image(0, 0, 'background');
            bg.width = this.game.width;
            bg.height = this.game.height;
            var logo = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, 'logoSG');
            logo.anchor.setTo(0.5);
            logo.y = 150;
            this.preloadBar = this.add.sprite((this.game.width-311)/2, (this.game.height-27)/2, 'preloaderBar');
            this.load.setPreloadSprite(this.preloadBar);
        },
        create: function (){
            // show the bg image and the progress bar
            this.state.start('MainMenu');
        }
    };

    return Preloader;
});