define(['Phaser'],
function (Phaser){
    var EXIT;

    var IntroState = function (game){
        this.game = game;
    }

    IntroState.prototype = {
        create: function (){
            // display images
            var bg = this.add.sprite(0, 0, 'intro');

            EXIT = this.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
            EXIT.onDown.add(letExit, this);
            
        }
    };

    function letExit(){
        this.state.start('MainMenu');
    }

    return IntroState;
});