define(['Phaser'],
function (Phaser){
    var INGAME,
        INTRO;

    var MenuState = function (game){
        this.game = game;
    }

    MenuState.prototype = {
        create: function (){
            // display images
            var bg = this.add.sprite(0, 0, 'menu');
            bg.width = this.game.width;
            bg.height = this.game.height;
            var logo = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, 'logoSG');
            // add the button that will start the game
            var btnStart = this.add.button(this.game.width*0.5, this.game.height * 0.6,
                            'btnStart', this.startGame, this, 1, 0, 2);
            var btnIntro = this.add.button(this.game.width*0.5 - 180, this.game.height * 0.8,
                            'btnIntro', this.showIntro, this, 1, 0, 2);
            var btnHelp = this.add.button(this.game.width*0.5 + 180, this.game.height * 0.8,
                            'btnHelp', this.showHelp, this, 1, 0, 2);
            btnStart.anchor.setTo(0.5);
            btnIntro.anchor.setTo(0.5);
            btnHelp.anchor.setTo(0.5);
            logo.anchor.setTo(0.5);
            logo.y = 150;

            INGAME = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            INGAME.onDown.add(letInGame, this);

            INTRO = this.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
            INTRO.onDown.add(letShowIntro, this);
        }
    };

    function letInGame(){
        var fx = this.game.add.audio('shot1');
            fx.play();
            this.state.start('InGame');
    }

    function letShowIntro(){
        this.state.start('Intro');
    }

    return MenuState;
});