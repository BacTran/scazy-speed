define(['Phaser'],
function (){
    var BootState = function (game){
        this.game = game;
    }

    BootState.prototype = {
        preload: function (){
            this.load.image('preloaderBar', 'resources/images/ui/loading-bar.png');
            this.game.load.image('logoSG', 'resources/images/ui/LogoSG.png');
        },
        create: function (){
            // show the bg image and the progress bar
            this.state.start('Preloader');
        }
    };

    return BootState;
});