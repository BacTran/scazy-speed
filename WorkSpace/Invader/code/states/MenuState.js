define(['Phaser'],
function (Phaser){
    var popup;
    var INGAME,
        INTRO,
        HELP,
        CLOSE;
    var MenuState = function (game){
        this.game = game;
    }

    MenuState.prototype = {
        create: function (){
            var bg = this.add.sprite(0, 0, 'background');
            bg.width = this.game.width;
            bg.height = this.game.height;
            var logo = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, 'logoSG');
            // add the button that will start the game
            var btnStart = this.add.button(this.game.width*0.5, this.game.height * 0.6,
                            'btnStart', this.startGame, this, 1, 0, 2);
            var btnIntro = this.add.button(this.game.width*0.5 - 180, this.game.height * 0.8,
                            'btnIntro', this.showIntro, this, 1, 0, 2);
            var btnHelp = this.add.button(this.game.width*0.5 + 180, this.game.height * 0.8,
                            'btnHelp', this.showHelp, this, 1, 0, 2);
            btnStart.anchor.setTo(0.5);
            btnIntro.anchor.setTo(0.5);
            btnHelp.anchor.setTo(0.5);
            logo.anchor.setTo(0.5);
            logo.y = 150;
            this.imgHelp = this.add.sprite(0, 0, 'help');
            this.imgIntro = this.add.sprite(0, 0, 'intro');
            this.imgHelp.visible = false;
            this.imgIntro.visible = false;
            this.imgHelp.width=this.game.width;
            this.imgHelp.height=this.game.height;
            this.imgIntro.width=this.game.width;
            this.imgIntro.height=this.game.height;
            
            INGAME = this.game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            INGAME.onDown.add(letInGame, this);
            INTRO = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
            INTRO.onDown.add(letIntro,this);
            HELP = this.game.input.keyboard.addKey(Phaser.Keyboard.TWO);
            HELP.onDown.add(letHelp,this);
            CLOSE = this.game.input.keyboard.addKey(Phaser.Keyboard.THREE);
            CLOSE.onDown.add(letClose,this);
            //help.visible=false;
            // display images
            //var bg = this.add.sprite(0, 0, 'background');
           // bg.width = this.stage.width;
           // bg.height = this.stage.height;
            // add the button that will start the game
           // var startBtn = this.add.button(this.game.width*0.5, this.game.height * 0.5,
                          //  'btnStart', this.startGame, this, 1, 0, 2);
           // startBtn.anchor.setTo(0.5);
           // startBtn.scale.x = 0.4;
           // startBtn.scale.y = 0.4;
        },
           
    };
     function letInGame()
        {
             var fx = this.game.add.audio('shot1');
            fx.play();
            this.state.start('InGame');
        }
        function letIntro()
        {
             var fx = this.game.add.audio('shot1');
            fx.play();
            this.imgIntro.visible = true;
        } 
        function letHelp()
        {
             var fx = this.game.add.audio('shot1');
            fx.play();
            this.imgHelp.visible = true;
        } 
         function letClose()
        {
             var fx = this.game.add.audio('shot1');
            fx.play();
            this.imgIntro.visible = false;
            this.imgHelp.visible = false;
        }    
    return MenuState;
});