define(['Phaser'],
function (Phaser){
    var Preloader = function (game){
        this.game = game;
    }

    Preloader.prototype = {
        preload: function (){
            this.stage = {width : this.game.width, height: this.game.height};
            // set background color and preload image
            this.stage.backgroundColor = '#FFFFFF';
            this.preloadBar = this.add.sprite((this.game.width-311)/2, (this.game.height-27)/2, 'preloaderBar');
            this.load.setPreloadSprite(this.preloadBar);
            // Load assets
            //background
            this.load.image('starfield', 'resources/images/background/starfield.png');
            this.load.image('background', 'resources/images/background/background2.jpg');
            this.load.image('help', 'resources/images/ui/help.jpg');
            this.load.image('intro', 'resources/images/ui/intro2.jpg');
            //ui
            this.load.spritesheet('btnStart', 'resources/images/ui/btnStart.png', 276, 87);
            this.load.spritesheet('btnIntro', 'resources/images/ui/btnIntro.png', 276, 87);
            this.load.spritesheet('btnHelp', 'resources/images/ui/btnHelp.png', 276, 87);
            this.load.spritesheet('ok', 'resources/images/ui/ok.png', 276, 87);
            //audio
            this.game.load.audio('lazer', 'resources/sounds/lazer.mp3');
            this.game.load.audio('theme', 'resources/sounds/oedipus_wizball_highscore.mp3');
            this.game.load.audio('pistol', 'resources/sounds/pistol.mp3');
            this.game.load.audio('pusher', 'resources/sounds/pusher.mp3');
            this.game.load.audio('shot1', 'resources/sounds/shot1.mp3');
            this.game.load.audio('explode1fx', 'resources/sounds/explode1.mp3');
            this.game.load.audio('explosionfx', 'resources/sounds/explosion.mp3');
            //ingame sprites
            this.load.image('bullet', 'resources/images/entity/bullet.png');
            this.load.image('enemyBullet', 'resources/images/entity/enemy-bullet.png');
            this.load.image('ship', 'resources/images/entity/player.png');
            this.load.spritesheet('invader', 'resources/images/entity/invader32x32x4.png', 96, 32);
            this.load.spritesheet('kaboom', 'resources/images/entity/explode.png', 128, 128);

        },
        create: function (){
            // show the bg image and the progress bar
            var bg = this.game.add.image(0, 0, 'background');
            bg.width = this.stage.width;
            bg.height = this.stage.height;
            var logo = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, 'logoSG');
            logo.anchor.setTo(0.5);
            //var title = this.game.add.image(this.game.width /2 - 300, this.game.height /2, 'title');

            this.state.start('MainMenu');
        }
    };

    return Preloader;
});