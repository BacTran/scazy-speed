define(['underscore',
        'Phaser'
    ],
function (_, Phaser){
    var player;
    var aliens;
    var bullets;
    var bulletTime = 0;
    var cursors;
    var fireButton;
    var explosions;
    var starfield;
    var score = 0;
    var scoreString = '';
    var scoreText;
    var lives;
    var enemyBullet;
    var firingTimer = 0;
    var stateText;
    var livingEnemies = [];
    var game;
    var btn_Fire;
    var LEFT,
        RIGHT,
        REPLAY;
    var level=1;
    var levelString ='';
    var levelText;
    function createAliens () {

        for (var y = 0; y < 1; y++)
        {
            for (var x = 3; x < 8; x++)
            {
                var alien = aliens.create(x * 100, y * 60, 'invader');
                alien.anchor.setTo(0.5, 0.5);
                alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
                alien.play('fly');
                alien.body.moves = false;
            }
        }

        aliens.x = 100;
        aliens.y = 50;

        //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
        var tween = game.add.tween(aliens).to( { x: 200 }, 3000, Phaser.Easing.Linear.None, true, 0, 3000, true);

        //  When the tween loops it calls descend
        tween.onLoop.add(descend, this);
    }

    function setupInvader (invader) {

        invader.anchor.x = 0.5;
        invader.anchor.y = 0.5;
        invader.animations.add('kaboom');

    }

    function descend() {

        aliens.y += 10;

    }

    function collisionHandler (bullet, alien) {

        //  When a bullet hits an alien we kill them both
        bullet.kill();
        alien.kill();

        //  Increase the score
        score += 20;
        scoreText.text = scoreString + score;

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(alien.body.x, alien.body.y);
        explosion.play('kaboom', 30, false, true);

        if (aliens.countLiving() == 0)
        {
            score += 1000;
            scoreText.text = scoreString + score;
            level++;
            levelText.text =levelString+level;
            enemyBullets.callAll('kill',this);
            stateText.text = " You Won \n Level Up \n Press 1 to continue";
            stateText.visible = true;

            //the "click to restart" handler
            //if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER))
            //    {
            //        restart();
            //    }
            //
            //game.input.onTap.addOnce(restart,this);
        }

    }

    function enemyHitsPlayer (player,bullet) {
        
        var fx = game.add.audio('explosionfx');
        fx.play();

        bullet.kill();

        live = lives.getFirstAlive();

        if (live)
        {
            live.kill();
        }

        //  And create an explosion :)
        var explosion = explosions.getFirstExists(false);
        explosion.reset(player.body.x, player.body.y);
        explosion.play('kaboom', 30, false, true);

        // When the player dies
        if (lives.countLiving() < 1)
        {
            player.kill();
            enemyBullets.callAll('kill');
            level=1;
            levelText.text=levelString+level;
            stateText.text=" GAME OVER \n Press 1 to restart";
            stateText.visible = true;

            //the "click to restart" handler
           // game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            //game.input.onTap.addOnce(restart,this);
        }

    }

    function enemyFires () {

        //  Grab the first bullet we can from the pool
        enemyBullet = enemyBullets.getFirstExists(false);

        livingEnemies.length=0;

        aliens.forEachAlive(function(alien){

            // put every living enemy in an array
            livingEnemies.push(alien);
        });


        if (enemyBullet && livingEnemies.length > 0)
        {
            
            var random=game.rnd.integerInRange(0,livingEnemies.length-1);

            // randomly select one of them
            var shooter=livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);

            game.physics.arcade.moveToObject(enemyBullet,player,120);
            firingTimer = game.time.now + 1500;
        }

    }

    function fireBullet () {

        var fx = game.add.audio('lazer');
        fx.play();
        //  To avoid them being allowed to fire too fast we set a time limit
        if (player.alive && game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);
            if (bullet)
            {
                //  And fire it
                bullet.reset(player.x, player.y + 8);
                bullet.body.velocity.y = -1500;
                bulletTime = game.time.now + 200;
            }
        }

    }

    function resetBullet (bullet) {

        //  Called if the bullet goes out of the screen
        bullet.kill();

    }

    function restart () {

        //  A new level starts
        
        //resets the life count
        lives.callAll('revive');
        //  And brings the aliens back from the dead :)
        aliens.removeAll();
        createAliens();

        //revives the player
        player.revive();
        //hides the text
        stateText.visible = false;

    }
    var InGameState = function (g){
        this.game = g;
        game = g;
        // this.player;
    }

    InGameState.prototype = {
        create: function (){
            game.time.advancedTiming = true;//to show fps

            game.physics.startSystem(Phaser.Physics.ARCADE);
            music = game.add.audio('theme');
            music.loop = true;
            music.play();
            //  The scrolling starfield background
            starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
            starfield.width =game.width;
            starfield.height =game.height;
            //  Our bullet group
            bullets = game.add.group();
            bullets.enableBody = true;
            bullets.physicsBodyType = Phaser.Physics.ARCADE;
            bullets.createMultiple(30, 'bullet');
            bullets.setAll('anchor.x', 0.5);
            bullets.setAll('anchor.y', 1);
            bullets.setAll('outOfBoundsKill', true);
            bullets.setAll('checkWorldBounds', true);

            // The enemy's bullets
            enemyBullets = game.add.group();
            enemyBullets.enableBody = true;
            enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
            enemyBullets.createMultiple(30, 'enemyBullet');
            enemyBullets.setAll('anchor.x', 0.5);
            enemyBullets.setAll('anchor.y', 1);
            enemyBullets.setAll('outOfBoundsKill', true);
            enemyBullets.setAll('checkWorldBounds', true);

            //  The hero!
            player = game.add.sprite(game.width*0.5, game.height*0.9, 'ship');
            player.anchor.setTo(0.5, 0.5);
            game.physics.enable(player, Phaser.Physics.ARCADE);

            //  The baddies!
            aliens = game.add.group();
            aliens.enableBody = true;
            aliens.physicsBodyType = Phaser.Physics.ARCADE;

            createAliens();

            //  The score
            scoreString = 'Score : ';
            scoreText = game.add.text(10, 10, scoreString + score, { font: '34px Arial', fill: '#fff' });
            // level
            levelString = 'Level : ';
            levelText = game.add.text(game.world.width - 150, 80, levelString + level, { font: '34px Arial', fill: '#fff' });

            //  Lives
            lives = game.add.group();
            game.add.text(game.world.width - 150, 10, 'Lives : ', { font: '34px Arial', fill: '#fff' });

            //  Text
            stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#fff' });
            stateText.anchor.setTo(0.5, 0.5);
            stateText.visible = false;

            for (var i = 0; i < 3; i++) 
            {
                var ship = lives.create(game.world.width - 100 + (30 * i), 60, 'ship');
                ship.anchor.setTo(0.5, 0.5);
                ship.angle = 90;
                ship.alpha = 0.4;
            }
            //  An explosion pool
            explosions = game.add.group();
            explosions.createMultiple(30, 'kaboom');
            explosions.forEach(setupInvader, this);

            //  And some controls to play the game with
            cursors = game.input.keyboard.createCursorKeys();
            //fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
            fireButton= game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            fireButton.onDown.add(fireBullet,this); 
            // move player
            LEFT = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            LEFT.onDown.add(moveLeft, this);
            RIGHT = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
            RIGHT.onDown.add(moveRight, this);
            REPLAY = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
            REPLAY.onDown.add(restart,this);
            starfield.tilePosition.y += 3;  

        },
        update: function (){
            //  Scroll the background

            player.body.velocity.setTo(0, 0);
            game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
            game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
           
        },
        render: function(){
            this.game.debug.text(this.game.time.fps || '--', 120, 120, "#ff0000");
        },
    };
    
    function moveLeft()
    {
        if (player.x >0 && player.alive)
            {
                //  Reset the player, then check for movement key
                player.body.velocity.x = -8000;
                
            }

        if (player.alive)
            {
                //  Reset the player, then check for movement key

                //  Firing?

                if (game.time.now > firingTimer)
                {
                    enemyFires();
                }

                //  Run collision

            }
    }
    function moveRight()
    {
        if (player.x < 1280 && player.alive)
            {
                //  Reset the player, then check for movement key
                player.body.velocity.x = 8000;
            }

            if (player.alive)
            {
                //  Reset the player, then check for movement ke

                //  Firing?

                if (game.time.now > firingTimer)
                {
                    enemyFires();
                }

            }
    }
    return InGameState;
});