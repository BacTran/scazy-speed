define([
  'Phaser'
], function (Phaser) {

    HomeState = function (game) {
        this.game = game;
       
    };

    HomeState.prototype = {
        preload : function(){
            var game = this.game;
            game.load.image('background', '');
        },

        create: function() {
            var game = this.game;
            this.jumpButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        },

        update : function() {
            var game = this.game;
        },
    };


  return HomeState;
});

