define(['Phaser'],
function (){
    var CreditState = function (game){
        this.game = game;
    }

    CreditState.prototype = {
        preload: function (){
            // updates ( codes, events, ..)?
            // load player data ( cards, deck, noftifications..)
            this.game.load.text('tplCredit','game/views/Templates/tplCredit.html');

            // load resources  (card sprites, backgrounds) with callback for other States via Managers
            this.game.load.image('logoSG', 'game/resources/images/ui/brand/logo SGGame.png');
            this.game.load.image('background', 'game/resources/images/background/BgSwordMan_s.jpg');
            this.game.load.image('title', 'game/resources/images/ui/title/fancycards_logo1.png');

        },
        create: function (){
            // show the bg image and the progress bar
            var bg= this.game.add.image(0, 0, 'background');
            var logo= this.game.add.image(this.game.width / 2 - 300, this.game.height /2 - 100, 'logoSG');
            var title= this.game.add.image(this.game.width /2 - 300, this.game.height /2, 'title');

            this.game.add.template('tplCredit');
            this.game.Core.get("GUIManager").setGUIInput(true);
        },
        update: function (){
            
        }
    };

    return CreditState;
});