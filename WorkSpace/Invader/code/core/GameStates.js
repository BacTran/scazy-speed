define([
    'code/states/BootState',
    'code/states/PreloaderState',
    'code/states/MenuState',
    'code/states/InGameState'
    ],
function (BootState, PreloaderState, MenuState, InGameState){
    var GameStates = function (game){
        game.state.add('Boot', BootState);
        game.state.add('Preloader', PreloaderState);
        game.state.add('MainMenu', MenuState);
        game.state.add('InGame', InGameState);
    };

    return GameStates;
});