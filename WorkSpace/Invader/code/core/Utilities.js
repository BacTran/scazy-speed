define(["jQuery"],
function ($){
    var Utilities = function (){

    };

    Utilities.prototype = {

        getViewport: function(){
            // get users screen size
            var h = $(window).height();
            var w = $(window).width();
            return {width: w, height: h};
        }
    };

    return new Utilities();
});