﻿using UnityEngine;
using System.Collections;
using AssembleCSharp;

public class PlayerController : MonoBehaviour {
    public float speed;
    public float jump;
    private bool flybird;
    bool canfly;
    bool checkback=true;
    public static PlayerController playercontroll;
	// Use this for initialization
	void Start () {
        canfly = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void FixedUpdate()
    {
        flybird = Input.GetButton("Fire1");
        if (flybird && checkback==true && canfly==true)
        {
            //this.rigidbody2D.AddForce(new Vector2(0, speed));
            //this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, speed));
            this.GetComponent<Rigidbody2D>().velocity =new Vector2(speed, jump);
            //if()
        }
        if(flybird && checkback==false && canfly==true)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, jump);
        }
       // print("------------------ speed " + speed);
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag=="box")
        {
            checkback = false;
            this.transform.Rotate(0, 180, 0);
            Config.ischangeleft = true;
            Config.checkchange = 1;
        }
        if (other.tag == "box1")
        {
            checkback = true;
            this.transform.Rotate(0, 180, 0);
        }
        if(other.tag=="floor")
        {
            canfly = false;
            speed = 0;
            jump = 0;
        }
        if(other.tag=="coin")
        {
            Destroy(other.gameObject);
            Config.checkCreat = true;
            Config.demcoin = 1;
        }
    }
}
