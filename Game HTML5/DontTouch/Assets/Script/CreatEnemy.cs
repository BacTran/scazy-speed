﻿using UnityEngine;
using System.Collections;
using AssembleCSharp;

public class CreatEnemy : MonoBehaviour {
    public GameObject[] enemy;
	// Use this for initialization
	void Start () {
        //float y = Random.Range(-4.5f, 4.5f);
        for (int i = 0; i <enemy.Length;i++)
        {
            int y = Random.Range(-5, 5);
            Instantiate(enemy[i], new Vector3(-2.7f, y, 0), Quaternion.identity);
        }
          
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Config.ischangeleft && Config.checkchange==1)
        {
            for (int i = 0; i < enemy.Length; i++)
            {
                int y = Random.Range(-5, 5);
                Instantiate(enemy[i], new Vector3(-2.7f, y, 0), Quaternion.identity);
            }
            Config.checkchange = 0;
        }
    }
}
