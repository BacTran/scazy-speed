﻿using UnityEngine;
using System.Collections;
using AssembleCSharp;

public class CreatCoin : MonoBehaviour
{
    public bool iscreat;
    public GameObject coin;
    // Use this for initialization
    void Start()
    {
        iscreat = false;
        float x = Random.Range(-2, 2.2f);
        float y = Random.Range(-4.2f, 4.2f);
        Instantiate(coin, new Vector3(x, y, 0), Quaternion.identity);
    }

    // Update is called once per frame
    void Update()
    {
        if(Config.checkCreat==true && Config.demcoin==1)
        {
            float x = Random.Range(-2, 2.2f);
            float y = Random.Range(-4.2f, 4.2f);
            Instantiate(coin, new Vector3(x, y, 0), Quaternion.identity);
            Config.demcoin = 0;
        }
    }
    public IEnumerator Spawn()
    {
        while (iscreat)
        {
            Vector3 SpawnPosition = new Vector3(transform.position.x, transform.position.y, 0.0f);
            Quaternion SpawnRotation = Quaternion.identity;
            Instantiate(coin, SpawnPosition, SpawnRotation);
        };
        yield return new WaitForSeconds(0.001f);
    }
}
