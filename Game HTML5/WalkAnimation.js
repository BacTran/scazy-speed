(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [];


// symbols:
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.btn_play = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(1,1,1).p("Ao1jIIRrAAIAAGRIxrAAg");
	this.shape.setTransform(56.6,20.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#0066FF").s().p("Ao1DJIAAmRIRrAAIAAGRg");
	this.shape_1.setTransform(56.6,20.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,115.3,42.2);


(lib._3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(10,1,1).p("AAAjWIAAGt");
	this.shape.setTransform(0,21.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib._3, new cjs.Rectangle(-5,-5,10,53), null);


(lib._2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(10,1,1).p("AAAgpIAABT");
	this.shape.setTransform(0,4.2);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib._2, new cjs.Rectangle(-5,-5,10,18.4), null);


(lib._1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(10,1,1).p("AAAjPIAAGf");
	this.shape.setTransform(0,20.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib._1, new cjs.Rectangle(-5,-5,10,51.7), null);


// stage content:
(lib.WalkAnimation = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		this.btn_play.addEventListener("click", fl_ClickToHide.bind(this));
		
		function fl_ClickToHide()
		{
			this.btn_play.visible = false;
		}
		var ENTER = 13;
		var ONE = 49;
		//window.onkeydown=handleKeyDown;
		window.onkeydown=function handleKeyDown(e)
		{
			console.log("---------"+e.keyCode);
			if(!e)
			{
				var e= window.event;
			}
			switch(e.keyCode)
			{
				case ENTER:
					console.log("---------pressEnter");
					this.gotoAndStop(27);
					break;
				case ONE:
					console.log("-------- press One");
					this.gotoAndPlay(1);
					break;
			}
		}.bind(this);
		//this.stop(25);
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
	}
	this.frame_26 = function() {
		this.stop();
	}
	this.frame_29 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(26).call(this.frame_26).wait(3).call(this.frame_29).wait(1));

	// rigt-arm
	this.ikNode_18 = new lib._2();
	this.ikNode_18.parent = this;
	this.ikNode_18.setTransform(148.9,185,0.996,0.996,167,0,0,0.4,3.8);

	this.ikNode_17 = new lib._3();
	this.ikNode_17.parent = this;
	this.ikNode_17.setTransform(174.2,146,0.996,0.996,33.2,0,0,1.7,2.2);

	this.ikNode_16 = new lib._1();
	this.ikNode_16.parent = this;
	this.ikNode_16.setTransform(203.2,115.1,0.996,0.996,42.9,0,0,1.8,1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ikNode_16,p:{regX:1.8,rotation:42.9,x:203.2,regY:1,y:115.1}},{t:this.ikNode_17,p:{regY:2.2,rotation:33.2,x:174.2,y:146,regX:1.7}},{t:this.ikNode_18,p:{rotation:167,x:148.9,y:185,regX:0.4,regY:3.8}}]}).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:36.2,x:203.4,regY:1,y:115.1}},{t:this.ikNode_17,p:{regY:2.3,rotation:27.9,x:178,y:149.2,regX:1.7}},{t:this.ikNode_18,p:{rotation:157.3,x:156.4,y:190.2,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:29.5,x:203.4,regY:0.9,y:115.1}},{t:this.ikNode_17,p:{regY:2.3,rotation:22.6,x:182.1,y:151.9,regX:1.7}},{t:this.ikNode_18,p:{rotation:147.5,x:164.4,y:194.8,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:22.8,x:203.5,regY:0.9,y:115}},{t:this.ikNode_17,p:{regY:2.3,rotation:17.3,x:186.5,y:154.1,regX:1.7}},{t:this.ikNode_18,p:{rotation:137.8,x:172.9,y:198.4,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:16.1,x:203.4,regY:1,y:115.1}},{t:this.ikNode_17,p:{regY:2.3,rotation:12,x:191.2,y:155.7,regX:1.7}},{t:this.ikNode_18,p:{rotation:128.1,x:181.7,y:201.1,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:9.5,x:203.4,regY:1,y:115.1}},{t:this.ikNode_17,p:{regY:2.3,rotation:6.7,x:196.2,y:156.9,regX:1.8}},{t:this.ikNode_18,p:{rotation:118.3,x:190.7,y:203,regX:0.5,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:2.8,x:203.4,regY:1,y:115}},{t:this.ikNode_17,p:{regY:2.3,rotation:1.4,x:201,y:157.5,regX:1.7}},{t:this.ikNode_18,p:{rotation:108.6,x:199.9,y:203.8,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-3.9,x:203.4,regY:1,y:115}},{t:this.ikNode_17,p:{regY:2.3,rotation:-3.9,x:206,y:157.4,regX:1.7}},{t:this.ikNode_18,p:{rotation:98.8,x:209.1,y:203.7,regX:0.5,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-5.4,x:203.4,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.2,rotation:-8.2,x:207,y:157.2,regX:1.7}},{t:this.ikNode_18,p:{rotation:94.6,x:213.7,y:203.2,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-7,x:203.4,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.3,rotation:-12.4,x:208.2,y:157.2,regX:1.7}},{t:this.ikNode_18,p:{rotation:90.4,x:218.2,y:202.6,regX:0.5,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-8.5,x:203.4,regY:1,y:115}},{t:this.ikNode_17,p:{regY:2.3,rotation:-16.7,x:209.3,y:157,regX:1.7}},{t:this.ikNode_18,p:{rotation:86.1,x:222.6,y:201.4,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.8,rotation:-10,x:203.3,regY:1,y:115}},{t:this.ikNode_17,p:{regY:2.3,rotation:-20.9,x:210.5,y:156.8,regX:1.7}},{t:this.ikNode_18,p:{rotation:81.9,x:227,y:200.2,regX:0.5,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-11.6,x:203.3,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.3,rotation:-25.1,x:211.6,y:156.6,regX:1.7}},{t:this.ikNode_18,p:{rotation:77.7,x:231.3,y:198.5,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-13.1,x:203.4,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.3,rotation:-29.4,x:212.7,y:156.3,regX:1.8}},{t:this.ikNode_18,p:{rotation:73.4,x:235.4,y:196.7,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-14.7,x:203.4,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.2,rotation:-33.6,x:213.7,y:156,regX:1.7}},{t:this.ikNode_18,p:{rotation:69.2,x:239.4,y:194.6,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.8,rotation:-16.2,x:203.2,regY:1,y:115}},{t:this.ikNode_17,p:{regY:2.3,rotation:-37.8,x:215,y:155.7,regX:1.8}},{t:this.ikNode_18,p:{rotation:64.9,x:243.3,y:192.2,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-17.8,x:203.3,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.2,rotation:-42.1,x:215.9,y:155.4,regX:1.7}},{t:this.ikNode_18,p:{rotation:60.7,x:247.2,y:189.8,regX:0.5,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-19.3,x:203.3,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.3,rotation:-46.3,x:217,y:155.1,regX:1.7}},{t:this.ikNode_18,p:{rotation:56.5,x:250.6,y:187,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-20.3,x:203.3,regY:1,y:114.9}},{t:this.ikNode_17,p:{regY:2.3,rotation:-48.2,x:217.8,y:154.8,regX:1.7}},{t:this.ikNode_18,p:{rotation:70,x:252.3,y:185.5,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-21.3,x:203.2,regY:1,y:114.8}},{t:this.ikNode_17,p:{regY:2.3,rotation:-50.1,x:218.5,y:154.4,regX:1.7}},{t:this.ikNode_18,p:{rotation:83.5,x:254,y:184.1,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-22.3,x:203.3,regY:1,y:114.7}},{t:this.ikNode_17,p:{regY:2.3,rotation:-52,x:219.1,y:154.1,regX:1.7}},{t:this.ikNode_18,p:{rotation:97.1,x:255.6,y:182.6,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-23.3,x:203.2,regY:0.9,y:114.6}},{t:this.ikNode_17,p:{regY:2.3,rotation:-53.8,x:219.9,y:153.9,regX:1.7}},{t:this.ikNode_18,p:{rotation:110.6,x:257.1,y:181.1,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-24.3,x:203.2,regY:1,y:114.7}},{t:this.ikNode_17,p:{regY:2.3,rotation:-55.7,x:220.5,y:153.5,regX:1.7}},{t:this.ikNode_18,p:{rotation:124.1,x:258.7,y:179.5,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-25.2,x:203.1,regY:0.9,y:114.5}},{t:this.ikNode_17,p:{regY:2.3,rotation:-57.6,x:221.2,y:153.2,regX:1.7}},{t:this.ikNode_18,p:{rotation:137.6,x:260.2,y:177.9,regX:0.4,regY:3.8}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.8,rotation:-26.2,x:203.1,regY:1,y:114.6}},{t:this.ikNode_17,p:{regY:2.3,rotation:-59.5,x:221.9,y:152.8,regX:1.7}},{t:this.ikNode_18,p:{rotation:151.2,x:261.6,y:176.4,regX:0.4,regY:3.7}}]},1).to({state:[{t:this.ikNode_16,p:{regX:1.9,rotation:-27.2,x:203.2,regY:1,y:114.5}},{t:this.ikNode_17,p:{regY:2.3,rotation:-61.3,x:222.5,y:152.5,regX:1.7}},{t:this.ikNode_18,p:{rotation:164.7,x:262.9,y:174.7,regX:0.5,regY:3.8}}]},1).to({state:[]},1).wait(4));

	// left-arm
	this.ikNode_9 = new lib._2();
	this.ikNode_9.parent = this;
	this.ikNode_9.setTransform(259.7,184.8,0.996,0.996,50.8,0,0,1.7,4.3);

	this.ikNode_8 = new lib._3();
	this.ikNode_8.parent = this;
	this.ikNode_8.setTransform(226.7,151.5,0.997,0.997,-45.3,0,0,1,2);

	this.ikNode_7 = new lib._1();
	this.ikNode_7.parent = this;
	this.ikNode_7.setTransform(205.7,114.8,0.997,0.997,-30.1,0,0,2,1.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ikNode_7,p:{regY:1.8,rotation:-30.1,x:205.7,y:114.8,regX:2}},{t:this.ikNode_8,p:{regY:2,scaleX:0.997,scaleY:0.997,rotation:-45.3,x:226.7,y:151.5,regX:1}},{t:this.ikNode_9,p:{rotation:50.8,x:259.7,y:184.8,regX:1.7,regY:4.3}}]}).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:-24.5,x:205.6,y:114.8,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-44.2,x:223.2,y:153.4,regX:1}},{t:this.ikNode_9,p:{rotation:51.9,x:255.5,y:187.3,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:-19,x:205.6,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-43.1,x:219.4,y:154.9,regX:1.1}},{t:this.ikNode_9,p:{rotation:53.1,x:251,y:189.4,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:-13.4,x:205.7,y:114.8,regX:2.1}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-41.9,x:215.4,y:156,regX:1}},{t:this.ikNode_9,p:{rotation:54.2,x:246.2,y:191.1,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:-7.8,x:205.6,y:115,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-40.8,x:211.3,y:156.8,regX:1}},{t:this.ikNode_9,p:{rotation:55.4,x:241.5,y:192.5,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:-2.2,x:205.5,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-39.6,x:207.2,y:157.1,regX:1}},{t:this.ikNode_9,p:{rotation:56.5,x:236.6,y:193.4,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:3.4,x:205.5,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-38.5,x:203,y:157.1,regX:1}},{t:this.ikNode_9,p:{rotation:57.6,x:231.7,y:193.9,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.8,rotation:9,x:205.4,y:114.8,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-37.4,x:198.9,y:156.6,regX:1}},{t:this.ikNode_9,p:{rotation:58.8,x:226.9,y:194,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:10.7,x:205.4,y:114.8,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-35,x:197.6,y:156.4,regX:1}},{t:this.ikNode_9,p:{rotation:61.1,x:224,y:194.9,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:12.4,x:205.4,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-32.7,x:196.4,y:156,regX:1.1}},{t:this.ikNode_9,p:{rotation:63.4,x:221.2,y:195.6,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:14.2,x:205.4,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-30.4,x:195,y:155.8,regX:1}},{t:this.ikNode_9,p:{rotation:65.8,x:218.3,y:196.3,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.8,rotation:15.9,x:205.4,y:114.8,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-28,x:193.9,y:155.6,regX:1}},{t:this.ikNode_9,p:{rotation:68.1,x:215.5,y:196.9,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.8,rotation:17.6,x:205.4,y:114.8,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-25.7,x:192.7,y:155.1,regX:1}},{t:this.ikNode_9,p:{rotation:70.4,x:212.5,y:197.4,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:19.3,x:205.4,y:114.9,regX:2.1}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-23.4,x:191.4,y:154.8,regX:1}},{t:this.ikNode_9,p:{rotation:72.8,x:209.6,y:197.7,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:21,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.997,scaleY:0.997,rotation:-21,x:190.2,y:154.3,regX:1}},{t:this.ikNode_9,p:{rotation:75.1,x:206.6,y:198.1,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:22.7,x:205.5,y:114.9,regX:2.1}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-18.7,x:189,y:153.9,regX:1}},{t:this.ikNode_9,p:{rotation:77.4,x:203.6,y:198.1,regX:1.6,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:24.5,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-16.4,x:187.8,y:153.3,regX:1}},{t:this.ikNode_9,p:{rotation:79.8,x:200.8,y:198.2,regX:1.7,regY:4.2}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:26.2,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-14,x:186.7,y:152.8,regX:1}},{t:this.ikNode_9,p:{rotation:82.1,x:197.7,y:198.2,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:28.2,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-10.6,x:185.5,y:152.1,regX:1.1}},{t:this.ikNode_9,p:{rotation:85.5,x:193.7,y:198.1,regX:1.7,regY:4.2}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:30.2,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.996,scaleY:0.996,rotation:-7.2,x:184.1,y:151.5,regX:1}},{t:this.ikNode_9,p:{rotation:89,x:189.5,y:197.8,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:32.2,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.997,scaleY:0.997,rotation:-3.7,x:182.8,y:150.7,regX:1}},{t:this.ikNode_9,p:{rotation:92.4,x:185.4,y:197.2,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:34.2,x:205.3,y:114.9,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.997,scaleY:0.997,rotation:-0.3,x:181.6,y:149.9,regX:1}},{t:this.ikNode_9,p:{rotation:95.8,x:181.5,y:196.6,regX:1.7,regY:4.2}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:36.1,x:205.2,y:115,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.997,scaleY:0.997,rotation:3.1,x:180.3,y:149.1,regX:1}},{t:this.ikNode_9,p:{rotation:99.2,x:177.4,y:195.5,regX:1.6,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:38.1,x:205.2,y:115,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.997,scaleY:0.997,rotation:6.5,x:179.1,y:148.2,regX:1}},{t:this.ikNode_9,p:{rotation:102.7,x:173.3,y:194.4,regX:1.7,regY:4.3}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:40.1,x:205.2,y:115,regX:2}},{t:this.ikNode_8,p:{regY:2,scaleX:0.997,scaleY:0.997,rotation:10,x:178,y:147.1,regX:1}},{t:this.ikNode_9,p:{rotation:106.1,x:169.6,y:193.2,regX:1.7,regY:4.2}}]},1).to({state:[{t:this.ikNode_7,p:{regY:1.9,rotation:42.1,x:205.3,y:115,regX:2}},{t:this.ikNode_8,p:{regY:2.1,scaleX:0.997,scaleY:0.997,rotation:13.4,x:176.9,y:146.4,regX:1}},{t:this.ikNode_9,p:{rotation:109.5,x:165.6,y:191.5,regX:1.7,regY:4.3}}]},1).to({state:[]},1).wait(4));

	// left-leg
	this.ikNode_15 = new lib._2();
	this.ikNode_15.parent = this;
	this.ikNode_15.setTransform(249.7,264.8,0.998,0.998,92.8,0,0,1.4,3.4);

	this.ikNode_14 = new lib._3();
	this.ikNode_14.parent = this;
	this.ikNode_14.setTransform(238,219.7,0.998,0.998,-14.9,0,0,1.8,2);

	this.ikNode_13 = new lib._1();
	this.ikNode_13.parent = this;
	this.ikNode_13.setTransform(205.6,192.6,0.998,0.998,-50.1,0,0,1.2,1.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.4,rotation:-50.1,y:192.6,x:205.6}},{t:this.ikNode_14,p:{regX:1.8,rotation:-14.9,x:238,y:219.7,regY:2,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.998,scaleY:0.998,rotation:92.8,x:249.7,y:264.8,regY:3.4}}]}).to({state:[{t:this.ikNode_13,p:{regX:1.1,regY:1.5,rotation:-41.4,y:192.7,x:205.6}},{t:this.ikNode_14,p:{regX:1.7,rotation:-11.2,x:233.4,y:224.2,regY:2,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:96.5,x:242.3,y:270.1,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:-32.8,y:192.7,x:205.6}},{t:this.ikNode_14,p:{regX:1.7,rotation:-7.5,x:228.3,y:228.2,regY:2.1,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.998,scaleY:0.998,rotation:100.2,x:234.1,y:274.4,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:-24.1,y:192.7,x:205.6}},{t:this.ikNode_14,p:{regX:1.7,rotation:-3.8,x:222.7,y:231.3,regY:2.1,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:103.9,x:225.5,y:277.8,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:-15.4,y:192.7,x:205.5}},{t:this.ikNode_14,p:{regX:1.7,rotation:-0.1,x:216.6,y:233.4,regY:2.1,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:107.6,x:216.3,y:280,regY:3.5}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:-6.7,y:192.7,x:205.5}},{t:this.ikNode_14,p:{regX:1.7,rotation:3.6,x:210.2,y:234.6,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:111.3,x:207,y:281.1,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:2,y:192.7,x:205.4}},{t:this.ikNode_14,p:{regX:1.8,rotation:7.3,x:204,y:234.9,regY:2.1,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.997,scaleY:0.997,rotation:115,x:197.7,y:280.9,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.4,rotation:10.7,y:192.6,x:205.4}},{t:this.ikNode_14,p:{regX:1.7,rotation:11,x:197.4,y:234,regY:2,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.997,scaleY:0.997,rotation:118.7,x:188.3,y:279.7,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.1,regY:1.5,rotation:11.8,y:192.6,x:205.2}},{t:this.ikNode_14,p:{regX:1.7,rotation:13.3,x:196.6,y:234,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:121,x:185.6,y:279.3,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:13,y:192.6,x:205.3}},{t:this.ikNode_14,p:{regX:1.7,rotation:15.6,x:195.8,y:233.7,regY:2,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:123.3,x:182.9,y:278.6,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:14.1,y:192.7,x:205.3}},{t:this.ikNode_14,p:{regX:1.7,rotation:17.9,x:194.9,y:233.6,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:125.7,x:180.3,y:277.8,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:15.3,y:192.7,x:205.3}},{t:this.ikNode_14,p:{regX:1.8,rotation:20.2,x:194.2,y:233.4,regY:2.1,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:128,x:177.7,y:277,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:16.4,y:192.7,x:205.3}},{t:this.ikNode_14,p:{regX:1.7,rotation:22.6,x:193.2,y:233.1,regY:2.1,scaleX:0.998,scaleY:0.998}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:130.3,x:175.1,y:276,regY:3.5}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:17.6,y:192.6,x:205.3}},{t:this.ikNode_14,p:{regX:1.7,rotation:24.9,x:192.4,y:232.9,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:132.6,x:172.5,y:275,regY:3.5}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.4,rotation:18.7,y:192.6,x:205.3}},{t:this.ikNode_14,p:{regX:1.7,rotation:27.2,x:191.7,y:232.5,regY:2,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.997,scaleY:0.997,rotation:134.9,x:170.2,y:273.9,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:19.9,y:192.6,x:205.3}},{t:this.ikNode_14,p:{regX:1.7,rotation:29.5,x:190.8,y:232.4,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:137.2,x:167.6,y:272.7,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:21,y:192.7,x:205.1}},{t:this.ikNode_14,p:{regX:1.7,rotation:31.8,x:190,y:232,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:139.5,x:165.2,y:271.4,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:22.2,y:192.6,x:205.2}},{t:this.ikNode_14,p:{regX:1.8,rotation:34.1,x:189.3,y:231.8,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:141.8,x:162.8,y:270.1,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:22.5,y:192.7,x:205.1}},{t:this.ikNode_14,p:{regX:1.7,rotation:37,x:189,y:231.5,regY:2,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:144.7,x:160.7,y:268.7,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:22.8,y:192.6,x:205.1}},{t:this.ikNode_14,p:{regX:1.7,rotation:39.9,x:188.7,y:231.5,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.997,scaleY:0.997,rotation:147.7,x:158.7,y:267.1,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:23.2,y:192.7,x:205.1}},{t:this.ikNode_14,p:{regX:1.8,rotation:42.8,x:188.5,y:231.6,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.997,scaleY:0.997,rotation:150.6,x:156.7,y:265.4,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:23.5,y:192.7,x:205}},{t:this.ikNode_14,p:{regX:1.7,rotation:45.7,x:188.2,y:231.3,regY:2,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:153.5,x:154.7,y:263.7,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.1,regY:1.4,rotation:23.8,y:192.6,x:204.9}},{t:this.ikNode_14,p:{regX:1.7,rotation:48.7,x:188,y:231.3,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:156.4,x:152.8,y:261.9,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.4,rotation:24.2,y:192.6,x:205}},{t:this.ikNode_14,p:{regX:1.7,rotation:51.6,x:187.6,y:231.3,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:159.3,x:151,y:260,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:24.5,y:192.7,x:205}},{t:this.ikNode_14,p:{regX:1.7,rotation:54.5,x:187.4,y:231.1,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.5,scaleX:0.997,scaleY:0.997,rotation:162.2,x:149.3,y:258,regY:3.4}}]},1).to({state:[{t:this.ikNode_13,p:{regX:1.2,regY:1.5,rotation:24.8,y:192.7,x:204.9}},{t:this.ikNode_14,p:{regX:1.8,rotation:57.4,x:187.2,y:231.1,regY:2.1,scaleX:0.997,scaleY:0.997}},{t:this.ikNode_15,p:{regX:1.4,scaleX:0.997,scaleY:0.997,rotation:165.1,x:147.9,y:255.9,regY:3.5}}]},1).to({state:[]},1).wait(4));

	// right-leg
	this.ikNode_6 = new lib._2();
	this.ikNode_6.parent = this;
	this.ikNode_6.setTransform(150.5,258.6,0.997,0.997,136.1,0,0,1.3,3.6);

	this.ikNode_5 = new lib._3();
	this.ikNode_5.parent = this;
	this.ikNode_5.setTransform(188.3,231.1,0.998,0.998,53.9,0,0,1.7,1.1);

	this.ikNode_4 = new lib._1();
	this.ikNode_4.parent = this;
	this.ikNode_4.setTransform(205.6,192.7,0.998,0.998,24,0,0,1.4,1.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.ikNode_4,p:{regX:1.4,rotation:24,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:53.9,x:188.3,y:231.1,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:136.1,x:150.5,y:258.6,regX:1.3,regY:3.6}}]}).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:18.1,x:205.7,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:56.6,x:192.3,y:232.6,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:141.5,x:153.2,y:258.3,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:12.2,x:205.7,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:59.3,x:196.4,y:233.7,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:146.8,x:156.1,y:257.6,regX:1.4,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:6.4,x:205.6,y:192.8,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:62.1,x:200.7,y:234.4,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:152.2,x:159.3,y:256.3,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:0.5,x:205.7,y:192.8,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:64.8,x:204.9,y:234.7,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:157.6,x:162.5,y:254.6,regX:1.3,regY:3.5}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-5.3,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:67.5,x:209.4,y:234.4,regY:1,regX:1.7}},{t:this.ikNode_6,p:{rotation:162.9,x:165.8,y:252.3,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-11.2,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:70.2,x:213.5,y:233.8,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:168.3,x:169.3,y:249.5,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-17.1,x:205.7,y:192.9,regY:1.6}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:72.9,x:217.8,y:232.7,regY:1,regX:1.7}},{t:this.ikNode_6,p:{rotation:173.6,x:172.7,y:246.3,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-20,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:65.9,x:219.7,y:232.1,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:166.6,x:176.7,y:251,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-23,x:205.7,y:192.7,regY:1.6}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:58.9,x:221.8,y:231.3,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:159.6,x:181.3,y:255.3,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-25.9,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:51.9,x:223.7,y:230.4,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:152.6,x:186.5,y:259.1,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.4,rotation:-28.9,x:205.5,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:44.9,x:225.6,y:229.4,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:145.6,x:192.3,y:262.5,regX:1.3,regY:3.5}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-31.8,x:205.6,y:192.6,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:37.9,x:227.5,y:228.2,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:138.6,x:198.4,y:265.1,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-34.8,x:205.7,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:30.9,x:229.3,y:227.1,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:131.6,x:204.8,y:267.2,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-37.8,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:23.9,x:231.1,y:225.7,regY:1,regX:1.7}},{t:this.ikNode_6,p:{rotation:124.6,x:211.6,y:268.6,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-40.7,x:205.7,y:192.6,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:16.9,x:232.7,y:224.4,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:117.6,x:218.7,y:269.3,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-43.7,x:205.7,y:192.7,regY:1.6}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:9.9,x:234.2,y:223,regY:1.1,regX:1.6}},{t:this.ikNode_6,p:{rotation:110.6,x:225.9,y:269.2,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-46.6,x:205.7,y:192.7,regY:1.6}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:2.9,x:235.9,y:221.4,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:103.6,x:233.1,y:268.4,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-45.2,x:205.6,y:192.6,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:-0.1,x:235.1,y:222.1,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:100.6,x:234.8,y:269.2,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-43.7,x:205.8,y:192.7,regY:1.6}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:-3.1,x:234.3,y:222.8,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:97.6,x:236.6,y:270,regX:1.4,regY:3.5}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.4,rotation:-42.2,x:205.6,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:-6.1,x:233.6,y:223.6,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:94.7,x:238.2,y:270.4,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-40.8,x:205.8,y:192.7,regY:1.6}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:-9,x:232.7,y:224.2,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:91.7,x:239.8,y:270.8,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-39.3,x:205.7,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:-12,x:231.9,y:224.8,regY:1,regX:1.7}},{t:this.ikNode_6,p:{rotation:88.7,x:241.5,y:271,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-37.8,x:205.7,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:-15,x:231.1,y:225.4,regY:1,regX:1.7}},{t:this.ikNode_6,p:{rotation:85.7,x:243,y:271.1,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-36.4,x:205.7,y:192.7,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.997,scaleY:0.997,rotation:-18,x:230.2,y:226.1,regY:1.1,regX:1.6}},{t:this.ikNode_6,p:{rotation:82.7,x:244.5,y:271.1,regX:1.3,regY:3.6}}]},1).to({state:[{t:this.ikNode_4,p:{regX:1.5,rotation:-34.9,x:205.8,y:192.8,regY:1.5}},{t:this.ikNode_5,p:{scaleX:0.998,scaleY:0.998,rotation:-21,x:229.4,y:226.7,regY:1.1,regX:1.7}},{t:this.ikNode_6,p:{rotation:79.7,x:246,y:271,regX:1.4,regY:3.6}}]},1).to({state:[]},1).wait(4));

	// Layer 1
	this.btn_play = new lib.btn_play();
	this.btn_play.parent = this;
	this.btn_play.setTransform(423.7,124.3,1,1,0,0,0,56.6,20.1);
	new cjs.ButtonHelper(this.btn_play, 0, 1, 1);

	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgyBlIAAgFIAFAAQANAAAGgDQAFgDACgFQACgFAAgTIAAhmQAAgOgBgDQgCgEgEgCQgDgCgFAAQgIAAgKAEIgCgFIBHghIAFAAIAAChQAAASACAFQABAFAFAEQAGADALAAIAFAAIAAAFg");
	this.shape.setTransform(413.9,123.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#666666").ss(10,1,1).p("AAAlxIAALj");
	this.shape_1.setTransform(203.4,148);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#666666").ss(1,1,1).p("AFtAAQAAAsgJApQgUBYhABHQgHAHgHAHQgEAEgDAEQhpBjiSAAQhdgWhAgZQg8gZgggbQgEgEgEgEQhLhLgWhiQgKgpAAgsQAAiVBqhrQABAAAAgBQAjgkAogXQBSgwBkAAQBlAABSAwQAoAXAjAkQABABABAAQBpBrAACVg");
	this.shape_2.setTransform(203.4,74.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FF0033").s().p("AidE+Qg8gZgggbIgIgIQhMhLgWhiQgJgpAAgsQAAiVBphqIACgCQAjgkApgXQBRgwBkAAQBlAABRAwQAoAXAkAkIACACQBpBqAACVQAAAsgJApQgVBYhABHIgNAOIgHAIQhqBjiRAAQhegWg/gZg");
	this.shape_3.setTransform(203.4,74.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.btn_play}]}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.btn_play}]},23).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.btn_play}]},1).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.btn_play}]},1).to({state:[]},1).to({state:[]},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(416.1,237,340.3,231.8);
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;