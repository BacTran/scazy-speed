(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"TestHtml5An_atlas_", frames: [[0,942,620,350],[0,0,1300,940]]}
];


// symbols:



(lib.background = function() {
	this.spriteSheet = ss["TestHtml5An_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.bg = function() {
	this.spriteSheet = ss["TestHtml5An_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.Btn_play = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#666666").ss(1,1,1).p("A7klpMA3JAAAIAALTMg3JAAAg");
	this.shape.setTransform(176.5,36.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006699").s().p("A7kFqIAArTMA3JAAAIAALTg");
	this.shape_1.setTransform(176.5,36.2);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-1,-1,355,74.4);


// stage content:
(lib.TestHtml5An = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		/* Click to Go to Frame and Stop
		Clicking on the specified symbol instance moves the playhead to the specified frame in the timeline and stops the movie.
		Can be used on the main timeline or on movie clip timelines.
		
		Instructions:
		1. Replace the number 5 in the code below with the frame number you would like the playhead to move to when the symbol instance is clicked.
		2.Frame numbers in EaselJS start at 0 instead of 1
		*/
		
		stop();
		this.Btn_play.addEventListener("click", fl_ClickToGoToAndStopAtFrame.bind(this));
		
		function fl_ClickToGoToAndStopAtFrame()
		{
			this.gotoAndStop(2);
		}
		
		
		/* Mouse Click Event
		Clicking on the specified symbol instance executes a function in which you can add your own custom code.
		
		Instructions:
		1. Add your custom code on a new line after the line that says "// Start your custom code" below.
		The code will execute when the symbol instance is clicked.
		*/
		this.stop();
		this.Btn_play.addEventListener("click", fl_MouseClickHandler.bind(this));
		
		function fl_MouseClickHandler()
		{
			// Start your custom code
			// This example code displays the words "Mouse clicked" in the Output panel.
			this.play();
			// End your custom code
		}
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(2));

	// Btn_play
	this.button_1 = new lib.Btn_play();
	this.button_1.parent = this;
	this.button_1.setTransform(469.6,422.8);
	new cjs.ButtonHelper(this.button_1, 0, 1, 1);

	this.timeline.addTween(cjs.Tween.get(this.button_1).to({_off:true},1).wait(1));

	// MainMenu
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#006699").s().p("Ah1C8QgPgOAAgUQAAgRAKgLQALgLAPAAQAPAAAJAJQAJAKAAATQAAAMACADQADADAEAAQAHAAAIgJQAMgMAQgsIAIgXIhYjKQgUgugKgKQgJgLgOgEIAAgLICRAAIAAALQgOABgFAFQgGAFAAAHQAAALAOAhIAuBrIAghTQASgsAAgSQAAgKgIgHQgHgGgSgBIAAgLIBcAAIAAALQgOACgIAJQgJAJgUAzIhODKQgdBNgOARQgUAZgfAAQgYAAgPgOg");
	this.shape.setTransform(899.5,304.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#006699").s().p("AAdCEQgNgLgDgWQgxAsgoAAQgYAAgPgQQgPgPgBgXQAAggAbgYQAbgZBagpIAAgcQAAgfgEgHQgDgJgIgGQgJgGgNAAQgSAAgMAIQgJAGAAAHQABAGAIAJQALANAAALQAAAPgLAKQgLAKgQAAQgTAAgNgLQgMgLAAgPQAAgVARgUQAQgTAegKQAegKAgAAQAnAAAWARQAXAQAGAUQAEALABAtIAABpQAAATABAFQABAEADADQADADAEAAQAIAAAIgMIAJAHQgPAWgQAKQgPAKgVAAQgXAAgNgLgAgnAeQgMAQAAAQQABAOAJAJQAIAIANAAQAPAAASgQIAAhbQgjAVgRAXg");
	this.shape_1.setTransform(870.4,297.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#006699").s().p("AgZDEQgRgHgRgQIgtAeIgKAAIAAlcQAAgXgCgGQgDgIgGgEQgHgEgPgBIAAgLIByAAIAACXQAigjApAAQAcAAAZAQQAZARAOAeQAOAeAAAoQAAAsgSAlQgSAlgeATQgeATgmAAQgVAAgSgHgAghgZIAAB5QAAAmACAKQADARALALQALAKAQAAQAPAAALgIQALgJAHgZQAHgZAAhCQAAg+gQgXQgLgRgUAAQgYAAgXAcg");
	this.shape_2.setTransform(837.2,291.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#006699").s().p("AhLDHQgfgIgNgMQgNgNAAgOQAAgOAKgKQAKgLAcgHQgngTAAghQAAgVAQgTQAQgSAhgOQgngOgSgYQgRgXAAgfQAAgoAhgdQAggeA0AAQAaAAAdAMIBdAAIAAAgIg1AAQAOAOAGAMQAHAQAAARQAAAdgQAWQgQAVgcANQgcAMgVAAIgjgCQgNAAgJAIQgJAIAAAMQAAALAIAHQAIAGASAAIAyAAQA9AAAXANQAhATAAAnQAAAZgPAVQgPAUgZAJQgmAPgwAAQglAAgegHgAhCB6QgRAJAAAPQAAAPARANQASAMAvAAQAoAAAYgLQAXgLAAgUQAAgHgFgFQgIgJgPgDQgRgDhEAAQgeAAgJAFgAgmipQgMARAAA1QAAAsAMARQALAQAQAAQARAAALgQQAMgRAAguQAAg1gNgTQgJgOgQAAQgRAAgMASg");
	this.shape_3.setTransform(791.7,303.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#006699").s().p("AAJCNIAAgLQAQgBAHgLQAEgIAAgdIAAh5QAAghgCgJQgDgJgGgFQgHgEgHgBQgYABgWAjIAACSQAAAfAGAIQAFAIAQACIAAALIiLAAIAAgLQASgBAHgKQAFgGAAggIAAiZQAAgggGgHQgGgIgSgCIAAgLIBwAAIAAAkQAVgYASgKQAUgKAWAAQAbAAARAPQASAOAGAXQAEAQAAAwIAABpQAAAgAGAIQAGAHASACIAAALg");
	this.shape_4.setTransform(759.6,297.2);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#006699").s().p("AheCmQghgrAAg5QAAg7AigrQAigsA7AAQAjAAAfATQAeASARAiQAPAhAAAoQAAA7gdAnQgjAvhAAAQg8AAgigrgAgYgwQgKALgEAiQgEAgAAA7QABAgAEAbQADAVAKAKQALALANAAQAMAAAKgHQALgKADgSQAHgcAAhUQgBgxgFgTQgGgSgLgJQgHgGgNAAQgNAAgLALgAghhrIAhhlIBZAAIhgBlg");
	this.shape_5.setTransform(727.8,291.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#006699").s().p("AgZDEQgRgHgRgQIgtAeIgKAAIAAlcQAAgXgCgGQgDgIgGgEQgHgEgPgBIAAgLIByAAIAACXQAigjApAAQAcAAAZAQQAZARAOAeQAOAeAAAoQAAAsgSAlQgSAlgeATQgeATgmAAQgVAAgSgHgAghgZIAAB5QAAAmACAKQADARALALQALAKAQAAQAPAAALgIQALgJAHgZQAHgZAAhCQAAg+gQgXQgLgRgUAAQgYAAgXAcg");
	this.shape_6.setTransform(695.5,291.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#006699").s().p("AAJCNIAAgLQAQgBAHgLQAEgIAAgdIAAh5QAAghgCgJQgDgJgGgFQgHgEgHgBQgYABgWAjIAACSQAAAfAGAIQAFAIAQACIAAALIiLAAIAAgLQASgBAHgKQAFgGAAggIAAiZQAAgggGgHQgGgIgSgCIAAgLIBwAAIAAAkQAVgYASgKQAUgKAWAAQAbAAARAPQASAOAGAXQAEAQAAAwIAABpQAAAgAGAIQAGAHASACIAAALg");
	this.shape_7.setTransform(647.9,297.2);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#006699").s().p("AAdECQgNgLgDgWQgxAsgoAAQgXAAgQgPQgPgQAAgXQAAgfAbgZQAbgZBZgqIAAgbQAAgfgDgIQgEgJgIgFQgJgHgMAAQgUAAgMAJQgHAFgBAHQAAAGAJAJQALANAAAMQAAAOgLAKQgKAKgSAAQgSAAgMgLQgNgLAAgPQAAgVAQgTQASgTAegKQAdgKAgAAQAmAAAXAQQAXAQAHAUQADAMAAAsIAABqQABATABAFQABAFAEACQADADADAAQAIAAAIgLIAIAHQgOAVgPAKQgRAKgTAAQgYAAgNgLgAgnCcQgLAQAAAQQgBAOALAKQAGAIAOAAQAPAAASgRIAAhbQgiAVgSAXgAhBhIQgagagDgqIATAAQAIASAQAKQAPAJAdAAQAfAAAQgJQAQgJAIgTIATAAQgDAqgZAaQgbAaggAAQgjAAgagagAgniqIAghiIBXAAIhdBig");
	this.shape_8.setTransform(617,284.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#006699").s().p("AgZDEQgRgHgRgQIgtAeIgKAAIAAlcQAAgXgCgGQgDgIgGgEQgHgEgPgBIAAgLIByAAIAACXQAigjApAAQAcAAAZAQQAZARAOAeQAOAeAAAoQAAAsgSAlQgSAlgeATQgeATgmAAQgVAAgSgHgAghgZIAAB5QAAAmACAKQADARALALQALAKAQAAQAPAAALgIQALgJAHgZQAHgZAAhCQAAg+gQgXQgLgRgUAAQgYAAgXAcg");
	this.shape_9.setTransform(583.8,291.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#006699").s().p("AhYBiQgZgmgBg3QAAhEAmgoQAlgpAxAAQApAAAfAiQAeAiADBDIiVAAQACA1AaAhQAUAXAcAAQARAAAPgJQAPgKAQgaIAKAHQgXAugbASQgbATgjAAQg8AAgfgvgAgShuQgSAaAAAuIAAAKIBPAAQgBgvgFgSQgEgRgMgKQgFgFgLAAQgOAAgJAPg");
	this.shape_10.setTransform(539.9,297.6);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#006699").s().p("ABZCNIAAgLQARgBAIgLQAFgJAAgcIAAh0QAAglgDgKQgDgKgGgFQgGgFgJAAQgMABgNAIQgNAKgNASIAACSQAAAeAEAIQAIALASAAIAAALIiNAAIAAgLQAMAAAHgFQAGgFACgIQACgGABgZIAAh0QgBglgDgKQgCgJgHgGQgHgFgIAAQgLAAgLAGQgNAJgPAWIAACSQAAAdAFAJQAGAKASABIAAALIiNAAIAAgLQARgBAHgKQAGgGAAggIAAiZQgBgggFgHQgGgIgSgCIAAgLIBwAAIAAAkQAWgYAUgKQASgKAXAAQAbAAAQAMQASANAKAZQAXgaAWgNQAVgLAYAAQAdAAAQANQARANAHAUQAGAUAAAsIAABvQAAAgAGAHQAFAIATACIAAALg");
	this.shape_11.setTransform(501.5,297.2);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#006699").s().p("AAdCEQgNgLgDgWQgxAsgoAAQgYAAgPgQQgPgPAAgXQAAggAbgYQAbgZBZgpIAAgcQAAgfgEgHQgDgJgIgGQgKgGgLAAQgUAAgMAIQgHAGgBAHQAAAGAJAJQALANAAALQAAAPgLAKQgLAKgRAAQgSAAgMgLQgNgLAAgPQAAgVAQgUQARgTAegKQAegKAgAAQAmAAAXARQAXAQAGAUQAFALAAAtIAABpQgBATACAFQACAEACADQADADAEAAQAIAAAIgMIAJAHQgPAWgPAKQgRAKgTAAQgYAAgNgLgAgnAeQgMAQABAQQAAAOAKAJQAGAIAOAAQAPAAASgQIAAhbQgiAVgSAXg");
	this.shape_12.setTransform(462,297.4);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#006699").s().p("AhWDAQgmgPgcgZQgdgZgPggQgVgoAAgyQAAhZA/g9QA/g+BeAAQAeAAAXAEQAOADAdALQAcALAGAAQAIAAAGgGQAIgGAGgRIALAAIAACMIgLAAQgUg5gogeQgmgegvAAQgrAAgeAZQgeAZgMAtQgNAtAAAuQAAA5AOArQANArAfAUQAdAVApgBQAPAAAPgDQAOgCAQgHIAAhSQABgXgEgHQgDgHgKgGQgLgGgOABIgKAAIAAgLIDBAAIAAALQgWABgIAFQgJAEgFALQgDAFABAWIAABSQgnARgpAJQgpAJgsAAQg4AAglgQg");
	this.shape_13.setTransform(423.8,291.5);

	this.instance = new lib.bg();
	this.instance.parent = this;
	this.instance.setTransform(-3,0,0.991,0.762);

	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f().s("#666666").ss(1,1,1).p("AA9joIXXAAIAAMoI9FAAIAAm5IAAlvgAkxCHIziAAIAArGIZQAAIAAFX");
	this.shape_14.setTransform(587,518.4);

	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f("#006699").s().p("AkxJAIAAm6IziAAIAArFIZQAAIAAFXIluAAIAAFuIAAluIFuAAIXYAAIAAMog");
	this.shape_15.setTransform(587,518.4);

	this.instance_1 = new lib.background();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-2,0,2.076,2.069);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.instance},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_15},{t:this.shape_14},{t:this.instance},{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape},{t:this.instance_1}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(637,360,1287.7,716);
// library properties:
lib.properties = {
	width: 1280,
	height: 720,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/TestHtml5An_atlas_.png?1478677056329", id:"TestHtml5An_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;