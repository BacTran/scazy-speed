﻿using UnityEngine;
using System.Collections;

public class CreatEnemyLeft : MonoBehaviour {
    public GameObject[] enemy;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < enemy.Length; i++)
        {
            int y = Random.Range(-5, 5);
            Instantiate(enemy[i], new Vector3(2.8f, y, 0), Quaternion.identity);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
