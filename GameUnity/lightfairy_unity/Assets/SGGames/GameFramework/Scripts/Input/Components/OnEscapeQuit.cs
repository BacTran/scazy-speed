﻿




using UnityEngine;

namespace SGGames.InputHelper.Components
{
    /// <summary>
    /// Quit the application when the escape key or android back button is pressed
    /// </summary>
    public class OnEscapeQuit : MonoBehaviour
    {
        void Update()
        {
            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

        }
    }
}