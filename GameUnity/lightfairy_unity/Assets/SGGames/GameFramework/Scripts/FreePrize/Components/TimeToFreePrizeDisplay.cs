﻿




using System;
using SGGames.GameObjects.Components;
using SGGames.Localisation;
using UnityEngine;

namespace SGGames.FreePrize.Components
{
    /// <summary>
    /// Shows the amount of time until the free prize is available
    /// </summary>
    [RequireComponent(typeof(UnityEngine.UI.Text))]
    public class TimeToFreePrizeDisplay : RunOnState
    {
        UnityEngine.UI.Text _text;

        public new void Awake()
        {
            base.Awake();

            _text = GetComponent<UnityEngine.UI.Text>();
        }


        public override void RunMethod()
        {
            TimeSpan time = FreePrizeManager.Instance.GetTimeToPrize();
            _text.text = LocaliseText.Format("FreePrize.NewPrize", time.Hours, time.Minutes, time.Seconds);
 }
    }
}