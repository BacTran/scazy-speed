﻿




using SGGames.GameObjects.Components;

namespace SGGames.Facebook.Components
{
    /// <summary>
    /// Shows an enabled or a disabled gameobject based upon whether the specified facebook permissions are granted
    /// </summary>
    public class EnableIfPermissionsGranted : EnableDisableGameObject
    {
        public bool PermissionUserFriends;
        public bool PermissionPublishActions;

        public override bool IsConditionMet()
        {
#if FACEBOOK_SDK
            return !((PermissionUserFriends && !FacebookManager.Instance.HasUserFriendsPermission()) ||
                            PermissionPublishActions && !FacebookManager.Instance.HasPublishActionsPermission());
#else
            return false;
#endif
        }
    }
}