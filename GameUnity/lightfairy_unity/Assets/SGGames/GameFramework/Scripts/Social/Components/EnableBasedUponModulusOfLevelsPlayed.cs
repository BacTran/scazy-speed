﻿




using SGGames.GameObjects.Components;
using SGGames;

namespace SGGames.Social.Components
{
    /// <summary>
    /// Shows an enabled or a disabled gameobject based upon the modulus of the number of levels played
    /// </summary>
    public class EnableBasedUponModulusOfLevelsPlayed : EnableDisableGameObject
    {
        public int Modulus;

        public override bool IsConditionMet()
        {
            return GameManager.Instance.TimesLevelsPlayed % Modulus == 0;
        }
    }
}