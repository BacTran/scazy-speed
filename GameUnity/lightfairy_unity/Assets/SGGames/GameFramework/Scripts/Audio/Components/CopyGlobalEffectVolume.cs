﻿



using SGGames;
using UnityEngine;

namespace SGGames.Audio.Components
{
    /// <summary>
    /// Copy the global effect volume to the attached Audio Source
    /// TODO: We should have notification when teh global effect volume changes and update this.
    /// </summary>
    [RequireComponent(typeof(AudioSource))]
    public class CopyGlobalEffectVolume : MonoBehaviour
    {
        void Awake()
        {
            if (GetComponent<AudioSource>() != null)
            {
                GetComponent<AudioSource>().volume = GameManager.Instance.EffectAudioVolume;
            }
        }
    }
}