﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SGGames.GameItems;
using SGGames.GameItems.ObjectModel;

namespace SGGames.Levels.ObjectModel
{
    /// <summary>
    /// A simple implementation of a Level GameItemsManager for setting up Levels using the standard object model class.
    /// 
    /// Name and Description are collected from the localisation file.
    /// </summary>
    public class LevelGameItemsManager : GameItemsManager<Level, GameItem>
    {
        public enum LevelUnlockModeType { Custom, Completion, Coins }
        int _unlockInitLevelNum;
        readonly int _numberOfLevels;
        readonly LevelUnlockModeType _levelUnlockModeType;
        readonly int _valueToUnlock;

        public delegate void OnLevelLoadDelegate(Level level);
        public event OnLevelLoadDelegate OnLevelLoad;

        public LevelGameItemsManager(int numberOfLevels, LevelUnlockModeType levelUnlockModeType = LevelUnlockModeType.Completion, int valueToUnlock = -1, int unlockInitLevelNum = 1)
        {
            if (GameManager.Instance != null)
            {
                if (GameManager.Instance.DebugMode)
                {
                    Debug.Log("LevelGameManager : Init with number of levels: " + numberOfLevels );
                }
            }
            _numberOfLevels = numberOfLevels;
            _levelUnlockModeType = levelUnlockModeType;
            _valueToUnlock = valueToUnlock;
            _unlockInitLevelNum = unlockInitLevelNum;
        }

        protected override void LoadItems()
        {
            Items = new List<Level>(_numberOfLevels);

            for (var count = 0; count < _numberOfLevels; count++ )
            {

                if (_levelUnlockModeType == LevelUnlockModeType.Completion)
                {
                    Items[count] = new Level(count + 1);
                }
                else
                {
                    Items[count] = new Level(count + 1, valueToUnlock: _valueToUnlock);
                }

                if (Items[count].Number <= _unlockInitLevelNum)
                {
                    //Debug.Log("Unlock " + Items[count].Number);
                    Items[count].Unlock();
                }
                else
                {
                    //Debug.Log("LetLock " + Items[count].Number);
                    Items[count].LetLock();
                }
            };
        }

        public int LastUnlockedNumber()
        {
            int max = 0;

            for (var count = 0; count < _numberOfLevels; count++)
            {
                if (Items[count] != null)
                {
                    if (Items[count].IsUnlocked)
                    {
                        max = Items[count].Number;
                    }
                }
            };

            return max;
        }

        public int LastUnlockedIndex()
        {
            int max = 0;

            for (var count = 0; count < _numberOfLevels; count++)
            {
                if (Items[count] != null)
                {
                    if (Items[count].IsUnlocked)
                    {
                        max = count;
                    }
                }
            };

            return max;
        }
    }
}