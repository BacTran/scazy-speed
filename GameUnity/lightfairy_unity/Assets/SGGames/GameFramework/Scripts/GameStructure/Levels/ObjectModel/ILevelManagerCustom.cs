﻿using UnityEngine;
using System.Collections;

namespace SGGames.Levels.ObjectModel
{
    public interface ILevelManagerCustom
    {

        GameItems.GameItemsManager<Level, GameItems.ObjectModel.GameItem> CreateLevelItemsManager();
    }
}