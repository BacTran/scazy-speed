﻿




using SGGames.GameItems;
using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;
using SGGames.Levels.ObjectModel;

namespace SGGames.Levels.Components
{
    public class UnlockLevelButton : UnlockGameItemButton<Level>
    {
        protected override GameItemsManager<Level, GameItem> GetGameItemsManager()
        {
            return GameManager.Instance.Levels;
        }
    }
}