﻿



using System.Collections;
using System.Collections.Generic;
using SGGames.GameItems;
using SGGames.GameItems.ObjectModel;

namespace SGGames.Worlds.ObjectModel
{
    /// <summary>
    /// A simple implementation of a World GameItemsManager for setting up Worlds using the standard object model class.
    /// 
    /// Name and Description are collected from the localisation file.
    /// </summary>
    public class WorldGameItemsManager : GameItemsManager<World, GameItem>
    {
        readonly int _numberOfWorlds;

        public WorldGameItemsManager(int numberOfWorlds)
        {
            _numberOfWorlds = numberOfWorlds;
        }

        protected override void LoadItems()
        {
            Items = new List<World>(_numberOfWorlds);

            for (var count = 0; count < _numberOfWorlds; count++ )
            {
                Items[count] = new World(count + 1);
            }
        }
    }
}