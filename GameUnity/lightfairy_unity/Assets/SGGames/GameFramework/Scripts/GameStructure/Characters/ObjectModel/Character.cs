﻿




using SGGames.GameItems.ObjectModel;
using SGGames.Players.ObjectModel;
using UnityEngine;

namespace SGGames.Characters.ObjectModel
{
    /// <summary>
    /// Character Game Item
    /// </summary>
    public class Character : GameItem
    {
        public Character(int levelNumber, string name = null, bool localiseName = true, string description = null, bool localiseDescription = true, Sprite sprite = null, int valueToUnlock = -1, PlayerProfile player = null) //, GameItem parent = null)
            : base(levelNumber, name: name, localiseName: localiseName, description: description, localiseDescription: localiseDescription, sprite: sprite, valueToUnlock: valueToUnlock, player: player, identifierBase: "Character", identifierBasePrefs: "C") //parent: parent, 
        {
        }
    }
}
