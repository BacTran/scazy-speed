﻿




using UnityEngine;

namespace SGGames.Networking
{
    /// <summary>
    /// 
    /// NOTE: This class is beta and subject to changebreaking change without warning.
    /// </summary>
    public class NetworkGameSetupJoinButton : MonoBehaviour
    {
        public NetworkPlayManager.NetworkGame NetworkGame;

        public void Awake()
        {
        }

        public void OnClick()
        {
            NetworkGameSetupInterfaceManager.Instance.JoinGame(NetworkGame.Address);
        }
    }
}