﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SGGames.Tile
{
    public class TileMapController : MonoBehaviour
    {
        public GameObject tile;
        public Material spriteMaterial;
        public Texture2D startTexture;
        public Texture2D endTexture;
        public List<Texture2D> tileTextures;
        public List<Vector2> tileBehaviours;

        public float xMove, yMove, spacer, gap;
        public float scale = 1;
        public float tileSize = 10;
        public int order = 2;
        
        protected int rows, columns;
        protected Vector2[,] gridPositions;
        protected Vector2 min, max;
        protected List<GameObject> tiles;
        protected TileProperties selected;
        protected IntTileMap mapDecoder;

        protected bool moveHorizontal, moveVertical;
        protected int left, right, up, down;
        protected Vector2 startMousePos, newMousePos;

        public bool useGridFramework = false;
        public int tileLayer;

        public virtual void InitLevel()
        {
            tileSize = tileTextures[0].width;

            // tileBehaviours contains the input + output directions for each pipe
            // 0 - up
            // 1 - right
            // 2 - down
            // 3 - left

            // insert a blank tileBehaviour as the first entry in the list
            tileBehaviours.Insert(0, Vector2.zero);

            // set up map decoder, create call backs
            mapDecoder = new IntTileMap(tileBehaviours);
        }

        public virtual void ClearLevel()
        {
            foreach (GameObject t in tiles)
                Destroy(t);
        }

        public virtual void CreateTileProperties(float posX, int gridX, float posY, int gridY, int tileType)
        {
            GameObject tile = CreateTile(new Vector2(posX, posY), tileTextures[tileType - 1], true);
            TileProperties properties = tile.GetComponent<TileProperties>();

            // assign properties and delegate actions
            properties.xGrid = gridX;
            properties.yGrid = gridY;
            properties.value = tileType;
            properties.TouchDelegate = OnTouchTile;
            properties.DragDelegate = OnDragTile;
            properties.ReleaseDelegate = OnReleaseTile;

            tiles.Add(tile);
        }

        public virtual GameObject CreateTile(Vector2 pos, Texture2D tex, bool collider)
        {
            float colliderScale = tileSize / 100;

            // create tile game object
            GameObject newTile = Instantiate(tile, new Vector3(pos.x, pos.y, 0), Quaternion.identity) as GameObject;
            newTile.transform.parent = transform;
            newTile.transform.localPosition = new Vector3(pos.x, pos.y, 0);
            //newTile.transform.localScale = new Vector3(scale, scale, scale);

            // create sprite and give it the correct texture
            Sprite sprite = new Sprite();
            sprite = Sprite.Create(tex, new Rect(0, 0, tileSize, tileSize), new Vector2(0, 0), 1f);

            SpriteRenderer spriteRenderer = newTile.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            spriteRenderer.material = spriteMaterial;
            spriteRenderer.sortingOrder = 1;

            // if its a draggable tile setup collider, else delete it
            if (collider)
            {
                BoxCollider2D collider2D = newTile.GetComponent<BoxCollider2D>();
                //collider2D.size = new Vector2(colliderScale, colliderScale);
                //collider2D.offset = new Vector2(colliderScale / 2, colliderScale / 2);
            }
            else
            {
                Destroy(newTile.GetComponent<BoxCollider2D>());
            }
            newTile.layer = tileLayer;
            return newTile;
        }

        public virtual void CreateTileGrid()
        {
            rows = mapDecoder.rows;
            columns = mapDecoder.columns;

            //This is a very poor implementation!
            /*
            if (columns > 4)
                scale = 4 / (float)columns;
            else
                scale = 1;

            gap = ((tileSize / 100) * 0.025f) * scale;
            spacer = ((tileSize / 100) + gap) * scale;
            */
            // grid positions holds the world position of each tile. Should intergrate with GridFramework?
            gridPositions = new Vector2[columns, rows];

            tiles = new List<GameObject>();

            

            // create the grid of tiles, row by row
            for (int i = rows - 1; i >= 0; i--)
            {
                

                for (int j = columns - 1; j >= 0; j--)
                {
                    Vector2 pos = posGridToWorld(j,i);
                    int tileType = mapDecoder.GetTile(j, i);

                    if (tileType != 0)
                    {
                        CreateTileProperties(pos.x, j, pos.y, i, tileType);
                    }

                    gridPositions[j, i] = pos;

                    //posX -= spacer;
                }

                //posY -= spacer;
            }
        }
        public Vector2 posGridToWorld(int gridX, int gridY)
        {
            //float posY = (((rows - 2) * 0.5f * spacer) + (gap / 2)) * tileSize;
            //float posX = (((columns - 2) * 0.5f * spacer) + (gap / 2)) * tileSize;
            float posX = gridX * tileSize;
            float posY = gridY * tileSize;

            return new Vector2(posX, posY);
        }
        public virtual void OnTouchTile(GameObject tile)
        {
            // if the tile is selected, check to see if it can be
            // moved in any direction
            if (selected == null)
            {
                selected = tile.GetComponent<TileProperties>();
                startMousePos = GetMousePos();

                moveHorizontal = moveVertical = false;

                yMove = selected.transform.position.y;
                xMove = selected.transform.position.x;
            }
        }

        public virtual void OnDragTile(Vector2 touch)
        {
            // allow movement only in available directions
            // and limit range of movement
            if (selected != null)
            {
                // if tile is in starting position, detect if mouse movement
                // is predominantly horizontal or vertical and lock to that axis
                if (selected.transform.position == new Vector3(gridPositions[selected.xGrid, selected.yGrid].x,
                                                              gridPositions[selected.xGrid, selected.yGrid].y, 0))
                {
                    newMousePos = GetMousePos();

                    if (newMousePos != startMousePos)
                    {
                        if (Mathf.Abs(newMousePos.x - startMousePos.x) > Mathf.Abs(newMousePos.y - startMousePos.y))
                        {
                            moveHorizontal = true;
                            moveVertical = false;
                        }
                        else
                        {
                            moveHorizontal = false;
                            moveVertical = true;
                        }
                    }
                }

                Vector2 pos = new Vector2(selected.xGrid, selected.yGrid);

                // based on whether the user is trying to move the tile horizontally
                // or vertically, find out if there are any empty spaces in that
                // direction, and if so how many (to work out movement limits)
                if (moveHorizontal)
                {
                    xMove = touch.x - selected.touchOffset.x;

                    left = selected.xGrid - mapDecoder.GetMoveLimit(pos, 3);
                    right = selected.xGrid + mapDecoder.GetMoveLimit(pos, 1);

                    min = gridPositions[left, selected.yGrid];
                    max = gridPositions[right, selected.yGrid];

                    if (xMove > max.x) xMove = max.x;
                    if (xMove < min.x) xMove = min.x;

                    selected.transform.position = new Vector3(xMove, yMove, 0);
                }

                if (moveVertical)
                {
                    yMove = touch.y - selected.touchOffset.y;

                    up = selected.yGrid + mapDecoder.GetMoveLimit(pos, 0);
                    down = selected.yGrid - mapDecoder.GetMoveLimit(pos, 2);

                    min = gridPositions[selected.xGrid, down];
                    max = gridPositions[selected.xGrid, up];

                    if (yMove > max.y) yMove = max.y;
                    if (yMove < min.y) yMove = min.y;

                    selected.transform.position = new Vector3(xMove, yMove, 0);
                }
            }
        }

        public virtual void OnReleaseTile(GameObject tile)
        {
            // on release, snap the tile into the closest
            // available grid space
            if (tile == selected.gameObject && selected != null)
            {
                int xSnap = selected.xGrid;
                int ySnap = selected.yGrid;

                if (moveHorizontal)
                {
                    for (int i = left; i <= right; i++)
                    {
                        if (Mathf.Abs(selected.transform.position.x - gridPositions[i, selected.yGrid].x) < (tileSize / 200))
                        {
                            xSnap = i;
                            break;
                        }
                    }
                }

                if (moveVertical)
                {
                    for (int i = down; i <= up; i++)
                    {
                        if (Mathf.Abs(selected.transform.position.y - gridPositions[selected.xGrid, i].y) < (tileSize / 200))
                        {
                            ySnap = i;
                            break;
                        }
                    }
                }

                xMove = gridPositions[xSnap, selected.yGrid].x;
                yMove = gridPositions[selected.xGrid, ySnap].y;

                selected.transform.position = new Vector3(xMove, yMove, 0);

                // update map decoder with the new layout
                if (selected.xGrid != xSnap || selected.yGrid != ySnap)
                {
                    mapDecoder.SetTile(xSnap, ySnap, selected.value);
                    mapDecoder.SetTile(selected.xGrid, selected.yGrid, 0);
                    selected.xGrid = xSnap;
                    selected.yGrid = ySnap;
                }

                selected = null;
            }

            // check to see if the level has been solved
            CheckLevel();
        }
        public virtual Vector2 GetMousePos()
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 10;
            return Camera.main.ScreenToWorldPoint(mousePos);
        }

        public virtual void CreateLevel(string mapName) { }
        public virtual void CheckLevel() { }
    }
}