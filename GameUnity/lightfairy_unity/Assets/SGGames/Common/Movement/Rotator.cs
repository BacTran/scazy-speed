using UnityEngine;
using System.Collections;
namespace SGGames
{
    /// <summary>
    /// Rotator.
    /// </summary>
    public class Rotator : MonoBehaviour
    {
        /// <summary>
        /// The rotation speed.
        /// </summary>
        public float rotationSpeed = 90;

        /// <summary>
        /// Up vector
        /// </summary>
        public Vector3 upVec = Vector3.up;
        public bool enable;
        public float initAngle = 0;
        public bool autoRotate = false;
        public bool rotateOnMouseDown = true;
        public Vector3 currentAngle;
        public int axis = 2;
        public GameObject targetObject;

        public bool useTween = false;
        public bool useLerp = false;
        public float lerpSpeed = 1;
        public float tweenTime = 1;
        public bool resetBeforeRotate = false;
        void Start()
        {
            if (targetObject == null)
            {
                targetObject = gameObject;
            }
            targetObject.transform.eulerAngles = new Vector3(currentAngle.x, currentAngle.y, initAngle);
        }

        void Update()
        {
            if (targetObject == null)
            {
                targetObject = gameObject;
            }
            if (autoRotate)
            {
                targetObject.transform.Rotate(upVec * Time.deltaTime * rotationSpeed);
            }

        }

        public virtual void LetRotate(float angle)
        {
            if (targetObject == null)
            {
                targetObject = gameObject;
            }


            float currentValue = 0;
            currentAngle = targetObject.transform.eulerAngles;
            if (resetBeforeRotate)
            {
                currentValue = initAngle;
                ApplyRotation(currentValue);
            }
            if (axis == 0)
            { 
                currentValue = currentAngle.x; 
            }
            else if (axis == 1)
            { 
                currentValue = currentAngle.y; 
            }
            else if (axis == 2)
            {
                currentValue = currentAngle.z;
            }



            if (!useTween)
            {
                //Calculate target value. Modulo 360
                if (currentValue + angle >= 360)
                {
                    currentValue = 0;
                }
                else
                {
                    currentValue += angle;
                }

                ApplyRotation(currentValue);
            }
            else
            {
                ApplyRotationTween(angle);
            }
            //Debug.Log(targetObject.transform.eulerAngles.z);
        }

        private void ApplyRotationTween(float angle)
        {
            if (axis == 0)
            {
                LeanTween.rotateX(targetObject, currentAngle.x + angle, tweenTime);
            }
            else if (axis == 1)
            {
                LeanTween.rotateY(targetObject, currentAngle.y + angle, tweenTime);
            }
            else if (axis == 2)
            {
                LeanTween.rotateZ(targetObject, currentAngle.z + angle, tweenTime);
            }
        }

        private void ApplyRotation(float currentValue)
        {
            if (axis == 0)
            {
                currentAngle.x = currentValue;
            }
            else if (axis == 1)
            {
                currentAngle.y = currentValue;
            }
            else if (axis == 2)
            {
                currentAngle.z = currentValue;
            }

            targetObject.transform.eulerAngles = new Vector3(currentAngle.x, currentAngle.y, currentAngle.z);
        }

        void OnMouseDown()
        {
            if (enable)
            {
                if (rotateOnMouseDown)
                {
                    LetRotate(rotationSpeed);
                }
            }
        }

        public static void rotateTowardsTarget(Vector3 targetPos,
                                            Transform t0,
                                            float rotateScalar)
        {
            Quaternion targetRotation =
                Quaternion.LookRotation(targetPos - t0.position, Vector3.up);
            t0.rotation =
                Quaternion.Slerp(t0.rotation, targetRotation,
                                Time.deltaTime * rotateScalar);
        }

        public void EnableRotator(bool state)
        {
            enable = state;
        }
    }
}
