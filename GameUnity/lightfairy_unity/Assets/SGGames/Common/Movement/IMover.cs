﻿using UnityEngine;
using System.Collections;

namespace SGGames
{
    public interface IMover
    {
        void LetMove();

        void FinishMove();

        bool IsMoving();
    }
}
