using UnityEngine;
using System.Collections;
using UnityEngine.UI;
namespace SGGames {
/// <summary>
///  misc functions
/// </summary>
public class Misc : MonoBehaviour {

	
		public static void setIntValue(string str,int val)
	{
		PlayerPrefs.SetInt(str,val);
	}
	
	public static int getIntValue(string nom)
	{
		return PlayerPrefs.GetInt(nom,0);
	}

    public static bool isMobilePlatform()
    {
        return RuntimePlatform.IPhonePlayer == Application.platform ||
            Application.platform == RuntimePlatform.Android ||
            RuntimePlatform.BlackBerryPlayer == Application.platform;

    }
	
}
}
