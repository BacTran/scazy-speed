﻿




using UnityEngine;

namespace SGGames.Display.Placement.Components
{
    /// <summary>
    /// Rotate this gameobject at a given rate.
    /// </summary>
    public class FixedRotation : MonoBehaviour
    {
        public Space Space;
        public float XAngle;
        public float YAngle;
        public float ZAngle;

        // Update is called once per frame
        void Update()
        {
            transform.Rotate(XAngle * Time.deltaTime, YAngle * Time.deltaTime, ZAngle * Time.deltaTime, Space);
        }
    }
}