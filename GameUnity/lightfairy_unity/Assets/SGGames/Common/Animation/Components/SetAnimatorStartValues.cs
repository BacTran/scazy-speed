﻿using UnityEngine;

namespace SGGames.ExAnimation.Components
{
    /// <summary>
    /// Set the specified start values on the animator
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class SetAnimatorStartValues : MonoBehaviour
    {
        public string[] IntValueNames;
        public int[] IntValueValues;

        void Awake()
        {
            Animator animator = GetComponent<Animator>();
            animator.SetInteger(IntValueNames[0], IntValueValues[0]);
        }
    }
}