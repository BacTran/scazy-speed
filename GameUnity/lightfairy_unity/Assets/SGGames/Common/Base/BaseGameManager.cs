using UnityEngine;
using System.Collections;
namespace SGGames {
/// <summary>
/// Base game manager.
/// </summary>
public class BaseGameManager  {
    static int m_paused = 0;
    ///is the game paused
    public static bool getPaused()
    {
        return m_paused == 1;
    }

    ///set the game to paused or unpaused
    public static void setPaused(bool paused)
    {
        m_paused = (paused) ? 1 : 0;

        if (m_paused == 1)
        {
            Time.timeScale = 0.0f;

        }
        else
        {
            Time.timeScale = 1f;
        }
    }

    ///toggle the game pause state	
    public static void togglePause()
    {
        m_paused ^= 1;
        setPaused(m_paused == 1);
    }

    public delegate void OnGameStart();
    public static event OnGameStart onGameStart;
    public static void startGame()
    {
        if (onGameStart != null)
        {
            onGameStart();
        }
    }

    public delegate void OnGamePause(bool pause);
    public static event OnGamePause onGamePause;
    public static void pauseGame(bool pause)
    {
        if (onGamePause != null)
        {
            onGamePause(pause);
        }
    }

    public delegate void OnGameOver(bool victory);
    public static event OnGameOver onGameOver;
    public static void gameover(bool victory)
    {
        if (onGameOver != null)
        {
            onGameOver(victory);
        }
    }

}
}
