using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace SGGames {
	/// <summary>
	/// Base result state.
	/// </summary>
	public class BaseResultState : MonoBehaviour 
	{
		private GameObject m_resultsPanel;
		private GameObject m_playPanel;
		private Text m_resultsText;
		public void Awake()
		{
			m_resultsPanel = GameObject.Find ("ResultsPanel");
			m_playPanel = GameObject.Find ("PlayPanel");
			//m_resultsText = Misc.getText("ResultsText");

		}


		public void OnEnable()
		{
			//BaseGameManager.onGameOver += onGameOver;
		}
		
		public void OnDisable()
		{
			//BaseGameManager.onGameOver -= onGameOver;
		}
		
		public void onGameOver(bool vic)
		{	
		}
		
	}
}
