﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ColorPropSwitch : MonoBehaviour {
    public bool useTextureSwitch = true;
    public bool useColorRendererSwitch = false;
    public bool usePrefabSwitch = false;
    public List<Texture2D> textures;
    public List<Color> colors;
    public List<GameObject> prefabs;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public static int ColorToInt(Color c)
    {
        if (c == Color.white)
        {

            return 0;
        }
        else if (c == Color.red)
        {
            return 1;
        }
        else if (c == Color.green)
        {
            return 2;
        }
        else if (c == Color.blue)
        {
            return 3;
        }
        return 0;
    }
    public void SwitchByColor(Color c)
    {
        if (useTextureSwitch)
        {
            Renderer renderer = GetComponent<Renderer>();
            Texture2D tex = textures[ColorToInt(c)];
            renderer.material.color = Color.white;
            
            renderer.material.SetTexture("_MainTex", tex);
        }

        if (useColorRendererSwitch)
        {
            LineRenderer lineRenderer = GetComponent<LineRenderer>();
            Color newColor = Color.white;
            newColor = colors[ColorToInt(c)];
            lineRenderer.material.color = newColor;
        }

        if (usePrefabSwitch)
        {
            GameObject newGO = Instantiate(prefabs[ColorToInt(c)]);
            newGO.SetActive(true);
            newGO.transform.localPosition = transform.position;
            newGO.transform.parent = transform;
        }
    }

    public void SwitchByEmotion(string emotion)
    {

    }
}
