﻿using UnityEngine;
using System.Collections;
using System;

namespace SGGames.Block
{
    [Serializable]
    public class MBlock
    {
        public string type = "empty";
        public string direction;
        public Color color = Color.white;
        public bool useColorSwitch = true;
        public bool useTextureSwitch = false;

        public int status = 0;
        public int x = 0;
        public int y = 0;
        //For ray saving
        public string inDir;
        public string outDir;

        public MBlock()
            : base()
        {
        }

        public MBlock(string type)
            : base()
        {
            this.type = type;
        }

        public void copy(MBlock mBlock)
        {
            this.type = mBlock.type;
            this.direction = mBlock.direction;
            this.color = mBlock.color;
            this.status = mBlock.status;
            this.x = mBlock.x;
            this.y = mBlock.y;
            this.inDir = mBlock.inDir;
            this.outDir = mBlock.outDir;
        }

        public virtual MBlock clone()
        {
            MBlock result = new MBlock();
            result.copy(this);
            return result;
        }
    }
}
