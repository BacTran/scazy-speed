﻿using UnityEngine;
using System;
using System.Collections;
using GoogleMobileAds.Api;


public class AdsController : MonoBehaviour
{
    public BannerView bannerView;
    public InterstitialAd interstitial;
    public RewardBasedVideoAd rewardBasedVideo;

    [Header("Ids")]
    public string bannerIdAndroid = "ca-app-pub-4470300775788325/2840445293";
    public string bannerIdIOS = "ca-app-pub-4470300775788325/2840445293";
    public string bannerIdWindow = "ca-app-pub-4470300775788325/2840445293";

    public string interstitialIdAndroid = "ca-app-pub-4470300775788325/5793911693";
    public string interstitialIdIOS = "ca-app-pub-4470300775788325/5793911693";
    public string interstitialIdWindow = "ca-app-pub-4470300775788325/5793911693";

    public string rewardVideoIdAndroid = "";
    public string rewardVideoIdIOS = "";
    public string rewardVideoIdWindow = "";

    [Header("Settings")]
    public int bannerClosedNum = 0;
    public int bannerMaxClosedNum = 10;
    public int interstitialClosedNum = 0;
    public int interstitialMaxClosedNum = 10;

    public bool bannerTop = false;
    public bool retryLoad = false;
    public string adsTags = "";
    

    void Awake()
    {
        
    }
    // Use this for initialization

    void Start()
    {
        RequestBanner();
        RequestInterstitial();
        CreateReward();
    }

    public void CreateReward()
    {
        // Get singleton reward based video ad reference.
        rewardBasedVideo = RewardBasedVideoAd.Instance;

        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;
    }
    // Update is called once per frame
    void Update()
    {
        //interstitialShow();
    }
    private void RequestInterstitial()
    {
        #if UNITY_ANDROID
                string adUnitId = interstitialIdAndroid;
        #elif UNITY_IPHONE
                        string adUnitId = interstitialIdIOS;
#else
                        string adUnitId = "unexpected_platform";
#endif
        if (interstitial != null)
        {
            interstitial.Destroy();
        }
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);

        interstitial.OnAdLoaded += HandleInterstitialLoaded;
        interstitial.OnAdFailedToLoad += HandleInterstitialFailedToLoad;
        interstitial.OnAdClosed += HandleInterstitialClosed;
        interstitial.OnAdLeavingApplication += HandleInterstitialLeavingApplication;

        // Create an empty ad request.
        AdRequest request = createAdRequest();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);

        interstitialClosedNum = 0;
    }
    // Returns an ad request with custom ad targeting.
    private AdRequest createAdRequest()
    {
    #if UNITY_EDITOR
            return new AdRequest.Builder()
                    .AddTestDevice(AdRequest.TestDeviceSimulator)
                    .AddKeyword("game")
                    .AddExtra("color_bg", "9B30FF")
                    .Build();
    #else
            return new AdRequest.Builder()
            .AddKeyword("game")
            .AddKeyword("application")
            .AddKeyword("puzzle")
            .AddExtra("color_bg", "9B30FF")
            .Build();
    #endif

    }
    public void HandleInterstitialLeavingApplication(object sender, System.EventArgs e)
    {
        //Do nothing
    }

    public void HandleInterstitialClosed(object sender, System.EventArgs e)
    {
        interstitialClosedNum++;
    }

    public void HandleInterstitialFailedToLoad(object sender, AdFailedToLoadEventArgs e)
    {
        //Do nothing
    }

    public void HandleInterstitialLoaded(object sender, System.EventArgs e)
    {
        //interstitial
        //Do nothing
    }


    private void RequestBanner()
    {
        #if UNITY_ANDROID
                        string adUnitId = bannerIdAndroid;
        #elif UNITY_IPHONE
                        string adUnitId = bannerIdIOS;
        #else
        string adUnitId = "unexpected_platform";
        #endif

        if (bannerView != null)
        {
            bannerView.Destroy();
        }
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(adUnitId, AdSize.Banner, bannerTop?AdPosition.Top:AdPosition.Bottom);

        // Register for ad events.
        bannerView.OnAdLoaded += HandleAdLoaded;
        bannerView.OnAdFailedToLoad += HandleAdFailedToLoad;
        bannerView.OnAdLoaded += HandleAdOpened;
        bannerView.OnAdClosed += HandleAdClosed;
        bannerView.OnAdLeavingApplication += HandleAdLeftApplication;
        // Create an empty ad request.
        AdRequest request = createAdRequest();
        // Load the banner with the request.
        bannerView.LoadAd(request);

        bannerClosedNum = 0;
    }

    void OnDestroy()
    {
        bannerView.Destroy();
        interstitial.Destroy();
    }
    public void ShowBanner()
    {
        if (bannerView != null)
        {
            bannerView.Show();
        }
    }

    public void HideBanner()
    {
        if (bannerView != null)
        {
            bannerView.Hide();
        }
        //bannerView.Destroy();
    }
    #region Banner callback handlers

    public void HandleAdLoaded(object sender, EventArgs args)
    {
        bannerView.Hide();
        //print("HandleAdLoaded event received.");
    }

    public void HandleAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleAdOpened(object sender, EventArgs args)
    {
        //print("HandleAdOpened event received");
    }

    void HandleAdClosing(object sender, EventArgs args)
    {
        bannerClosedNum++;
        //print("HandleAdClosing event received");
    }

    public void HandleAdClosed(object sender, EventArgs args)
    {
        //print("HandleAdClosed event received");
    }

    public void HandleAdLeftApplication(object sender, EventArgs args)
    {
        //print("HandleAdLeftApplication event received");
    }

    #endregion


    public void ShowInterstitial()
    {
        if (interstitial != null)
        {
            //Debug.Log("q/c to");
            if (interstitial.IsLoaded())
            {
                interstitial.Show();
            }
        }
    }

    public void HideInterstitial()
    {
        if (interstitial.IsLoaded())
        {
            
        }
    }

    private void RequestRewardBasedVideo()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
            string adUnitId = rewardVideoIdAndroid;
#elif (UNITY_5 && UNITY_IOS) || UNITY_IPHONE
            string adUnitId = rewardVideoIdIOS;
#else
            string adUnitId = "unexpected_platform";
#endif

        rewardBasedVideo.LoadAd(createAdRequest(), adUnitId);
    }

    public void ShowRewardBasedVideo()
    {
        if (rewardBasedVideo.IsLoaded())
        {
            rewardBasedVideo.Show();
        }
        else
        {
            //print("Reward based video ad is not ready yet.");
        }
    }

    #region RewardBasedVideo callback handlers

    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        //print("HandleRewardBasedVideoLoaded event received.");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        //print("HandleRewardBasedVideoFailedToLoad event received with message: " + args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        //print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        //print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        //print("HandleRewardBasedVideoClosed event received");
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        //print("HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        //print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion
}