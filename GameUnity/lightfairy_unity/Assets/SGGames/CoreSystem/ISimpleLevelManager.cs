﻿using System;
using SGGames.Levels.ObjectModel;

namespace SGGames
{
    public interface ISimpleLevelManager
    {
        bool GoNextLevel();
        bool HasMoreLevel();
        Level LoadLevel(string path);
        void PlayLevel(int i);
        ILinearLevelGame Game();
    }
}
