﻿using UnityEngine;
using System.Collections;

public interface IStepGame {

    void Reset();
    void NextStep();

    bool IsMoving();
    void IsFinished();

    bool IsValidMove(IStep step);

    void DoStep(IStep step, System.Action action);
}
