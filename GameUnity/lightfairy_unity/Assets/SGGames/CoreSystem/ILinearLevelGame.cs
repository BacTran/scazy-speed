﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SGGames
{
    public interface ILinearLevelGame
    {
        void StartLevel();
        bool CheckComplete();
        void FinishLevel();
        bool NextLevel();
    }
}
