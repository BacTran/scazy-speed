﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SGGames.Levels.ObjectModel;
using System;
using SGGames;

namespace SGGames
{
    public class SimpleLevelManager : LevelGameItemsManager, ISimpleLevelManager
    {
        private List<Level> levels = new List<Level>();
        private Level currentLevel;
        private ILinearLevelGame game;
        private int currentLevelIndex;

        public SimpleLevelManager(ILinearLevelGame game, int numOfLevel)
            : base(numOfLevel)
        {
            this.game = game;
        }
        public virtual bool HasMoreLevel()
        {
            currentLevelIndex = levels.IndexOf(currentLevel);

            if (currentLevelIndex + 1 < levels.Count)
            {

                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool GoNextLevel()
        {
            currentLevelIndex = levels.IndexOf(currentLevel);

            if (HasMoreLevel())
            {
                PlayLevel(currentLevelIndex + 1);
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual void PlayLevel(int i)
        {
            currentLevel = levels[i];
            game.StartLevel();
        }

        public virtual Level LoadLevel(String path)
        {
            return null;
        }

        public virtual ILinearLevelGame Game()
        {
            return game;
        }
    }
}