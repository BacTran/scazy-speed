﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SGGames.Block;
using X_UniTMX;

namespace SGGames.RayTracing
{
    public class LightRayMap : MBlockMap
    {
        public bool converted = false;
        public List<MLight> lights = new List<MLight>();
        public bool winState = false;
        public string data;
        public bool useTiled = false;
        Map tmxMap;
        System.Action onFinishConvert;

        public LightRayMap()
            : base()
        {
        }

        public override void addBlock(int x, int y, MBlock block)
        {
            base.addBlock(x, y, block);
        }

        public void convertData(System.Action onFinishConvert = null)
        {
            this.onFinishConvert = onFinishConvert;
            convertMap(data);
        }

        public MBlock getNextBlock(MRay ray)
        {
            return getNextBlock(ray.currentBlock, ray.direction);
        }

        /**
    -----------------
    Format of a Level:
    -----------------
    Lights
    ┴ ├ ┬ ┤
    ▲ ► ▼ ◄ 
    
    T Spliter
    ╧ ╟ ╤ ╢

    X Spliter
    ╩ ╠ ╦ ╣

    Colors
    R G B

    Mirrors
    / \

    Targets
    r g b w

    Wall
    ▓
        **/
        public void convertMap(string mapDataStr)
        {
            if (mapDataStr.StartsWith("<?xml"))
            {
                useTiled = true;
            }
            if (!useTiled)
            {
                CreateMapFromTextData(mapDataStr);
                SaveData();
                CreateLights();
                converted = true;

                if (onFinishConvert != null)
                {
                    onFinishConvert();
                }
            }
            else
            {
                Map.MapLoadingParam param = new Map.MapLoadingParam();
                param.createMeshOnInit = false;
                param.loadDataOnly = true;
                tmxMap = new Map(mapDataStr, "tmx", null, null, 0, param, OnTiledMapFinishedLoading);
            }
        }

        public override MBlock CloneTypeCheck(MBlock mBlock)
        {
            if (mBlock != null)
            {
                if (mBlock.GetType() == typeof(MLight))
                {
                    MLight newInstance = new MLight();
                    newInstance.copy((MLight)mBlock);
                    return newInstance;
                }
                else if (mBlock.GetType() == typeof(MWall))
                {
                    MWall newInstance = new MWall();
                    newInstance.copy((MWall)mBlock);
                    return newInstance;
                }
                else if (mBlock.GetType() == typeof(MMirror))
                {
                    MMirror newInstance = new MMirror();
                    newInstance.copy((MMirror)mBlock);
                    return newInstance;
                }
                else if (mBlock.GetType() == typeof(MSplit))
                {
                    MSplit newInstance = new MSplit();
                    newInstance.copy((MSplit)mBlock);
                    return newInstance;
                }
                else if (mBlock.GetType() == typeof(MTarget))
                {
                    MTarget newInstance = new MTarget();
                    newInstance.copy((MTarget)mBlock);
                    return newInstance;
                }
                else if (mBlock.GetType() == typeof(MColorBlock))
                {
                    MColorBlock newInstance = new MColorBlock();
                    newInstance.copy((MColorBlock)mBlock);
                    return newInstance;
                }
                else if (mBlock.GetType() == typeof(MBlock))
                {
                    MBlock newInstance = new MBlock();
                    newInstance.copy((MBlock)mBlock);
                    return newInstance;
                }
            }
            return null;
        }

        private void CreateMapFromTextData(string mapDataStr)
        {
            // Read in level file and set the size, adding extra to account for borders we'll add
            string[] datalines = mapDataStr.Split(new char[] { '\n' });
            width = datalines[0].Length - 1;
            height = datalines.Length;

            Debug.Log("CREATE LEVEL : " + width + " " + height);

            // Loop through the level data and build the level by examining each item's neighbors

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    //Debug.Log(" create block"+i+" "+j+" " + datalines[i][j]);
                    // Make walls
                    string aBlock = "" + datalines[i][j];

                    switch (aBlock)
                    {
                        case "▓":
                            // Make wall
                            /*
                                var tileNum = 0;
                                if (lines[i-1][j] != "X") {tileNum += 1;}	// Up
                                if (lines[i+1][j] != "X") {tileNum += 2;}	// Down
                                if (lines[i][j+1] != "X") {tileNum += 4;}	// Right
                                if (lines[i][j-1] != "X") {tileNum += 8;}	// Left
                                */
                            MWall newWall = new MWall();
                            addBlock(j, i, newWall);

                            break;

                        case "┴":
                        case "├":
                        case "┬":
                        case "┤":
                        case "▲":
                        case "►":
                        case "▼":
                        case "◄":
                            // Make light
                            MLight newLight = new MLight();

                            switch (aBlock)
                            {
                                case "┴":
                                case "▲": newLight.direction = "down"; break;
                                case "├":
                                case "►": newLight.direction = "right"; break;
                                case "┬":
                                case "▼": newLight.direction = "up"; break;
                                case "┤":
                                case "◄": newLight.direction = "left"; break;
                            }

                            addBlock(j, i, newLight);
                            break;

                        case "╧":
                        case "╟":
                        case "╤":
                        case "╢":
                            // Make T-Split
                            MSplit newSplitT = new MSplit();
                            newSplitT.type = "Tsplit";

                            switch (aBlock)
                            {
                                case "╧": newSplitT.direction = "up"; break;
                                case "╟": newSplitT.direction = "left"; break;
                                case "╤": newSplitT.direction = "down"; break;
                                case "╢": newSplitT.direction = "right"; break;
                            }

                            addBlock(j, i, newSplitT);
                            break;

                        case "╩":
                        case "╠":
                        case "╦":
                        case "╣":
                            // Make X-Split
                            MSplit newSplitX = new MSplit();
                            newSplitX.type = "Xsplit";

                            switch (aBlock)
                            {
                                case "╩": newSplitX.direction = "up"; break;
                                case "╠": newSplitX.direction = "right"; break;
                                case "╦": newSplitX.direction = "down"; break;
                                case "╣": newSplitX.direction = "left"; break;
                            }

                            addBlock(j, i, newSplitX);
                            break;

                        case "r":
                        case "g":
                        case "b":
                        case "w":
                            // Make target
                            MTarget newTarget = new MTarget();

                            switch (aBlock)
                            {
                                case "r": newTarget.color = Color.red; break;
                                case "g": newTarget.color = Color.green; break;
                                case "b": newTarget.color = Color.blue; break;
                                case "w": newTarget.color = Color.white; break;
                            }

                            newTarget.status = 0;
                            addBlock(j, i, newTarget);
                            break;

                        case "R":
                        case "G":
                        case "B":
                        case "W":
                            // Make Color
                            MColorBlock newColorBlock = new MColorBlock();

                            switch (aBlock)
                            {
                                case "R": newColorBlock.color = Color.red; break;
                                case "G": newColorBlock.color = Color.green; break;
                                case "B": newColorBlock.color = Color.blue; break;
                                case "W": newColorBlock.color = Color.white; break;
                            }

                            addBlock(j, i, newColorBlock);
                            break;

                        case "/":
                        case "\\":
                            // Make mirrors
                            MMirror newMirror = new MMirror();

                            switch (aBlock)
                            {
                                case "/": newMirror.direction = "/"; break;
                                case "\\": newMirror.direction = "\\"; break;
                            }

                            addBlock(j, i, newMirror);
                            break;

                        case "_":
                        case " ":
                            MBlock newEmpty = new MBlock();
                            newEmpty.type = "empty";
                            addBlock(j, i, newEmpty);

                            break;
                        default:
                            break;
                        //throw new System.ArgumentException(" Strange symbol at x :" + j +", y : "+i);

                    }
                }
            }
        }
        public static string fromTextToTiled(string code)
        {
            switch (code)
            {
                case "▓": return "1";

                case "┴": return "11";
                case "├": return "10";
                case "┬": return "9";
                case "┤": return "12";
                case "▲": return "11";
                case "►": return "10";
                case "▼": return "9";
                case "◄": return "12";


                case "╧": return "17";
                case "╟": return "18";
                case "╤": return "19";
                case "╢": return "20";


                case "╩": return "21";
                case "╠": return "22";
                case "╦": return "23";
                case "╣": return "24";


                case "r": return "14";
                case "g": return "15";
                case "b": return "16";
                case "w": return "13";


                case "R": return "26";
                case "G": return "27";
                case "B": return "28";
                case "W": return "25";


                case "/": return "5";
                case "\\": return "6";


                case "_": return "0";
                case " ": return "0";

                default: return "0";
            }
        }
        private void CreateLights()
        {
            lights.Clear();
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height - 1; j++)
                {
                    if (mapData[i, j]!=null && mapData[i, j].GetType() == typeof(MLight))
                    {
                        lights.Add(mapData[i, j] as MLight);
                    }
                }
            }

            //Debug.Log("Num of lights " + lights.Count);
        }

        private void OnTiledMapFinishedLoading(Map mapObj)
        {
            TileLayer layer = ((TileLayer)mapObj.GetLayer("Tile"));
            width = mapObj.Width;
            height = mapObj.Height;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    X_UniTMX.Tile tile = layer.Tiles[x, y];
                    if (tile != null)
                    {
                        if (tile.GetPropertyAsString("type") == "rock" || tile.GetPropertyAsString("type") == "wall")
                        {
                            MWall newWall = new MWall();
                            newWall.type = tile.GetPropertyAsString("type");
                            addBlock(x, y, newWall);
                        }
                        else if (tile.GetPropertyAsString("type") == "mirror")
                        {
                            MMirror newMirror = new MMirror();
                            newMirror.type = tile.GetPropertyAsString("type");
                            newMirror.direction = tile.GetPropertyAsString("direction");

                            addBlock(x, y, newMirror);
                        }
                        else if (tile.GetPropertyAsString("type") == "Tsplit" || tile.GetPropertyAsString("type") == "Xsplit")
                        {
                            MSplit newSplit = new MSplit();
                            newSplit.type = tile.GetPropertyAsString("type");
                            newSplit.direction = tile.GetPropertyAsString("direction");
                            addBlock(x, y, newSplit);
                        }
                        else if (tile.GetPropertyAsString("type") == "light")
                        {
                            MLight newLight = new MLight();
                            newLight.direction = tile.GetPropertyAsString("direction");
                            newLight.type = tile.GetPropertyAsString("type");
                            switch (tile.GetPropertyAsString("color"))
                            {
                                case "r":
                                case "red": newLight.color = Color.red; break;
                                case "g":
                                case "green": newLight.color = Color.green; break;
                                case "b":
                                case "blue": newLight.color = Color.blue; break;
                                case "w":
                                case "white": newLight.color = Color.white; break;
                            }
                            addBlock(x, y, newLight);
                        }
                        else if (tile.GetPropertyAsString("type") == "block")
                        {
                            MColorBlock newColorBlock = new MColorBlock();
                            newColorBlock.type = "color";
                            switch (tile.GetPropertyAsString("color"))
                            {
                                case "r":
                                case "red": newColorBlock.color = Color.red; break;
                                case "g":
                                case "green": newColorBlock.color = Color.green; break;
                                case "b":
                                case "blue": newColorBlock.color = Color.blue; break;
                                case "w":
                                case "white": newColorBlock.color = Color.white; break;
                            }

                            addBlock(x, y, newColorBlock);
                        }
                        else if (tile.GetPropertyAsString("type") == "target")
                        {
                            MTarget newTarget = new MTarget();
                            newTarget.type = "target";
                            switch (tile.GetPropertyAsString("color"))
                            {
                                case "r":
                                case "red": newTarget.color = Color.red; break;
                                case "g":
                                case "green": newTarget.color = Color.green; break;
                                case "b":
                                case "blue": newTarget.color = Color.blue; break;
                                case "w":
                                case "white": newTarget.color = Color.white; break;
                            }

                            addBlock(x, y, newTarget);
                        }
                        else
                        {
                            Debug.Log("Unsupported type" + x + " " + y);
                        }
                    }
                    else
                    {
                        MBlock newEmpty = new MBlock();
                        newEmpty.type = "empty";
                        addBlock(x, y, newEmpty);
                    }
                }
            }
            SaveData();
            CreateLights();
            converted = true;
            if (onFinishConvert != null)
            {
                onFinishConvert();
            }

            
        }

        public int getLightName(MLight block)
        {
            return lights.IndexOf(block);
        }

        public bool checkWinState()
        {
            int count = 0;
            foreach (MBlock aBlock in mapData)
            {
                if (aBlock != null)
                {
                    if (aBlock.type == "target")
                    {
                        if (aBlock.status == 0)
                        {
                            count++;
                            //Debug.Log("Not win");

                            this.winState = false;
                            return false;
                        }
                    }
                }
            }

            //Debug.Log("Win" + count);
            this.winState = true;
            return true;
        }

        public void clearWinState()
        {
            foreach (var aBlock in mapData)
            {
                if (aBlock.type == "target")
                {
                    aBlock.status = 0;

                }
            }
            this.winState = false;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public override void ResetData()
        {
            base.ResetData();
            CreateLights();
        }
    }
}