﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

namespace SGGames.RayTracing
{

    [CustomEditor(typeof(GameStage))]
    public class GameStageEditor : Editor
    {
        int currentLevel = 0;
        LightRayLevelManagerCustomization levelCustom;

        public void LoadLevel(int index)
        {
            GameStage stage = target as GameStage;
            if (stage.map == null)
            {
                stage.map = new LightRayMap();
            }
            else
            {
                //SceneGraphUtils.DestroyAllChildren(stage.transform);
                //stage.map.clearBlocks();
                //stage.resetStage();
            }
            stage.map.convertMap(levelCustom.levelData[index].text);
            stage.buildGameFromMap(stage.map);
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GameStage stage = target as GameStage;
            currentLevel = EditorGUILayout.IntField("CurrentLevel", currentLevel);

            if (GUILayout.Button("ClearLevel"))
            {
                //SceneGraphUtils.DestroyAllChildren(stage.transform);
                //stage.resetStage();
                /*
                if (stage.map != null)
                {
                    stage.map.clearBlocks();
                    stage.resetStage();
                }
                 */ 
            }

            if (GUILayout.Button("LoadLevel"))
            {
                LoadLevel(currentLevel);
            }

            if (GUILayout.Button("SaveLevel"))
            {
                SaveLevel(currentLevel);
            }

            if (GUILayout.Button("Convert to tmx"))
            {
                ConvertAllToTmx();
            }

            levelCustom = EditorGUILayout.ObjectField("LevelManager", levelCustom, typeof( LightRayLevelManagerCustomization)) as LightRayLevelManagerCustomization;
            if (levelCustom == null) { 
                EditorGUILayout.LabelField("Missing:", "Select an LevelManager object first");
            }
        }

        private void ConvertAllToTmx()
        {
            /*
            foreach (TextAsset text in levelCustom.levelData)
            {
                File.CreateText(EditorApplication.applicationContentsPath+"");
            }
             * */

            Debug.Log("Path " + EditorApplication.applicationContentsPath);
              
        }
        private void SaveLevel(int index)
        {

        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}