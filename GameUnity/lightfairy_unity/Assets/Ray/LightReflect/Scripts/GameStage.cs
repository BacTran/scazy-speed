﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SGGames.Block;
using SGGames.Levels;

namespace SGGames.RayTracing
{
    public class GameStage : MonoBehaviour
    {
        public LightRayGame game;
        public LightRayMap map;
        public GameObject stageBg;

        [Header("Blocks")]
        public GameObject wall;
        public GameObject lightPrefab;
        public GameObject mirrorPrefab;
        public GameObject targetPrefab;
        public GameObject groundPrefab;
        public GameObject rayPrefab;
        public GameObject tsplitPrefab;
        public GameObject xsplitPrefab;
        public GameObject colorBlockPrefab;
        public int flipyint = 9;
        bool tiled = true;
        public Vector3 stagePosCenter;

        public float rayWidth = 1;
        public Plane groundPlane;
        public Transform rayParent;
        public bool hasTag = false;
        public string mapObjectTag = "mapObject";

        private List<GameObject> raysArr = new List<GameObject>();

        [Header("Settings")]
        public bool useBlockTween = true;
        public bool useRayBending = false;

        [Header("Audio")]
        public AudioSource audioSource;
        public AudioClip soundDrop, soundBack;
        public AudioClip soundStageCreate;
        public AudioClip soundStar;
        public AudioClip soundWow;

        [Header("Character")]
        public FlowerCharacterController flowerCharacter;
        public List<FlowerCharacterController> flowerCharacters;
        public FairyCharacter fairyCharacter;

        [Header("Help")]
        public GameObject circleHelpObject;
        public GameObject handHelpObject;
        public GameObject crossHelpObject;
        void Awake()
        {

        }

        void Start()
        {
            game = FindObjectOfType<LightRayGame>();
            groundPlane = new Plane(Vector3.up, stagePosCenter);
            if (audioSource == null)
            {
                audioSource = GetComponent<AudioSource>();
            }
        }

        public void startMap(LightRayMap newMap, System.Action onBuildFinishCallback = null)
        {
            if (this.map == newMap && newMap != null)
            {
                Debug.Log("Reset map instead of creating new");
                newMap.ResetData();
            }
            else
            {
                this.map = newMap;
            }

            if (!newMap.converted)
            {
                newMap.convertData(() =>
                {
                    BuildGameWithAnim(newMap, onBuildFinishCallback);
                });
            }
            else
            {
                BuildGameWithAnim(newMap, onBuildFinishCallback);
            }
        }

        private void BuildGameWithAnim(LightRayMap map, System.Action onBuildFinishCallback)
        {
            if (stageBg != null)
            {
                Vector3 currentScale = stageBg.transform.localScale;
                stageBg.transform.localScale = new Vector3(currentScale.x * 0.2f, currentScale.y, currentScale.z);
                LeanTween.scaleX(stageBg, currentScale.x, 0.8f).setEase(LeanTweenType.easeInOutElastic).setOnComplete(() =>
                {

                    int levelNumber = LevelManager.Instance.GetCurrentLevel().Number;
                    audioSource.PlayOneShot(soundStageCreate);

                    ChangeCharacter(levelNumber);

                    buildGameFromMap(map);
                    updateMap();
                });
                if (onBuildFinishCallback != null)
                {
                    LeanTween.delayedCall(3, onBuildFinishCallback);
                }
            }
            else
            {
                int levelNumber = LevelManager.Instance.GetCurrentLevel().Number;
                ChangeCharacter(levelNumber);
                buildGameFromMap(map);
                updateMap();
            }
        }

        private void ChangeCharacter(int levelNumber)
        {
            foreach (FlowerCharacterController flower in flowerCharacters)
            {
                if (flower != null)
                {
                    flower.gameObject.SetActive(false);
                }
            }
            if (levelNumber < 20)
            {
                flowerCharacter = flowerCharacters[0];
            }
            else if (levelNumber < 40)
            {
                flowerCharacter = flowerCharacters[1];
            }
            else if (levelNumber < 60)
            {
                flowerCharacter = flowerCharacters[2];
            }
            else
            {
                flowerCharacter = flowerCharacters[3];
            }
            if (flowerCharacter != null)
            {
                flowerCharacter.gameObject.SetActive(true);
                flowerCharacter.SetEmotion("happy");
                LeanTween.delayedCall(3, () =>
                {
                    if (flowerCharacter != null)
                    {
                        flowerCharacter.SetEmotion("idle");
                    }
                });
            }

            if (fairyCharacter != null)
            {
                fairyCharacter.ShowIntro(levelNumber);
            }
        }

        public void prepareMap(LightRayMap map)
        {
            if (this.map != map)
            {
                if (this.map != null)
                {
                    resetStage();

                }
                this.map = map;
            }

        }
        void createGround()
        {
            GameObject newGround = Instantiate(groundPrefab);
            newGround.transform.position = (new Vector3(5, 0, 5));
            newGround.tag = "mapObject";
        }
        public void buildGameFromMap(LightRayMap map)
        {
            int count = 0;
            //createGround();
            for (int i = 0; i < map.height; i++)
            {
                for (int j = 0; j < map.width; j++)
                {
                    var b = map.getBlock(j, i);
                    if (b != null)
                    {
                        count++;
                        createMapObject(j, i, b);
                    }
                    else
                    {
                        //Debug.Log("null at " + j + " " + i + " count" + count);
                    }

                }
            }

        }
        void win()
        {
            // explode all the target
        }

        void createMapObject(int j, int i, MBlock block)
        {
            GameObject resultBlock = null;
            bool hasController = true;

            if (block == null)
            {
                Debug.LogError("Block is null at" + j + " " + i);
            }

            switch (block.type)
            {
                case "wall":
                    // Make walls
                    GameObject newWall = CreateWallBlock(j, i);
                    resultBlock = newWall;
                    hasController = false;

                    break;
                case "light":
                    // Make light
                    GameObject newLight = CreateLightBlock(block);
                    resultBlock = newLight;
                    //hasController = false;

                    break;
                case "mirror":
                    // Make mirror
                    GameObject newMirror = Instantiate(mirrorPrefab);
                    if (block.direction == "\\")
                    {
                        newMirror.transform.eulerAngles = new Vector3(90, 180, 0);
                    }
                    resultBlock = newMirror;
                    break;
                case "color":
                    // Make color
                    GameObject newColorBlock = Instantiate(colorBlockPrefab);
                    //newColorBlock.GetComponent<MeshRenderer>().material.color = block.color;
                    resultBlock = newColorBlock;
                    break;
                case "target":
                    // Make target
                    GameObject newTarget = Instantiate(targetPrefab);
                    //newTarget.GetComponent<MeshRenderer>().material.color = block.color;
                    resultBlock = newTarget;
                    hasController = false;

                    break;
                case "Tsplit":
                    // Make Xsplit

                    GameObject newTsplit = Instantiate(tsplitPrefab);
                    var tsplitAngle = Directions.directionToInt(block.direction);
                    newTsplit.transform.eulerAngles = new Vector3(90, 90 * tsplitAngle, 0);
                    resultBlock = newTsplit;
                    break;
                case "Xsplit":
                    // Make Xsplit

                    GameObject newXsplit = Instantiate(xsplitPrefab);
                    var xsplitAngle = Directions.directionToInt(block.direction);
                    newXsplit.transform.eulerAngles = new Vector3(90, 90 * xsplitAngle, 0);
                    resultBlock = newXsplit;

                    break;
            }

            if (resultBlock != null)
            {
                resultBlock.SetActive(true);
                resultBlock.transform.localPosition = toWorldPosFromGrid(j, i);
                resultBlock.transform.parent = transform;
                if (resultBlock.GetComponent<ColorPropSwitch>() != null)
                {
                    block.useColorSwitch = false;
                    block.useTextureSwitch = true;
                    resultBlock.GetComponent<ColorPropSwitch>().SwitchByColor(block.color);
                }
                else
                {
                    block.useColorSwitch = true;
                    block.useTextureSwitch = false;
                    resultBlock.GetComponent<MeshRenderer>().material.color = block.color;

                }
                //if (resultBlock.GetComponent<MBlock>() == null)
                //{
                //MBlock blockComp = resultBlock.AddComponent<MBlock>();
                //blockComp.copy(block);
                //}
                if (hasController)
                {
                    LightRayBlockController controller = resultBlock.AddComponent<LightRayBlockController>();
                    controller.gameStage = this;
                    controller.thisBlock = block;
                    if (hasTag)
                    {
                        resultBlock.tag = mapObjectTag;
                    }
                }
                if (useBlockTween)
                {
                    resultBlock.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);
                    LeanTween.scale(resultBlock, new Vector3(1.4f, 1.4f, 1), 0.4f).setEase(LeanTweenType.easeInOutElastic).setDelay((block.x + block.y * 10) * 0.01f).setOnComplete(() =>
                    {
                        resultBlock.transform.localScale = new Vector3(1f, 1f, 1f);
                    });
                }
            }
        }

        private GameObject CreateLightBlock(MBlock block)
        {
            GameObject newLight = Instantiate(lightPrefab);
            int lightAngle = Directions.directionToInt(block.direction);
            newLight.transform.eulerAngles = new Vector3(90, 90 * lightAngle, 0);
            newLight.name = "Light ";

            //newLight.GetComponent<MeshRenderer>().material.color = block.color;
            return newLight;
        }

        private GameObject CreateWallBlock(int j, int i)
        {
            GameObject newWall = Instantiate(wall);

            if (tiled)
            {
                int tileNum = 0;
                MBlock mUp = map.getBlock(i, j + 1); // Up
                MBlock mRight = map.getBlock(i + 1, j);	// Right
                MBlock mDown = map.getBlock(i, j - 1); // Down
                MBlock mLeft = map.getBlock(i - 1, j); // Left

                int rowIndex = 0;
                int colIndex = 0;
                if (mDown == null || (mDown.type != "wall"))
                { // Up is Empty
                    if (mUp == null || (mUp.type != "wall"))
                    {
                        rowIndex = 3;
                    }
                    else
                    {
                        rowIndex = 0;
                    }

                }
                else
                {
                    if (mUp == null || (mUp.type != "wall"))
                    {
                        rowIndex = 1;
                    }
                    else
                    {
                        rowIndex = 2;
                    }
                }

                if (mLeft == null || !(mLeft.type == "wall"))
                { // Right is Empty
                    if (mRight == null || (mRight.type != "wall"))
                    {
                        colIndex = 3;
                    }
                    else
                    {
                        colIndex = 0;
                    }
                }
                else
                {
                    if (mRight == null || (mRight.type != "wall"))
                    {
                        colIndex = 1;
                    }
                    else
                    {
                        colIndex = 2;
                    }

                }

                int colCount = 4;
                int rowCount = 4;

                int rowNumber = 0;
                int colNumber = 0;
                int fps = 10;
                Vector2 offset;
                Vector2 size = new Vector2(1.0f / colCount, 1.0f / rowCount);

                offset = new Vector2((colIndex + colNumber) * size.x, (1.0f - size.y) - (rowIndex + rowNumber) * size.y);


                //newWall.renderer.material.SetTextureOffset("_MainTex",offset);
                //newWall.renderer.material.SetTextureScale("_MainTex",size);
            }
            return newWall;
        }

        public void displayRays()
        {

            foreach (MRay ray in game.rays)
            {
                var indexint = game.rays.IndexOf(ray);

                // create ray display
                GameObject newRayGO = GameObject.Instantiate(rayPrefab);

                newRayGO.transform.position = new Vector3(0, 0, 0);
                newRayGO.name = "Ray " + ray.name;
                //MRay rayComp = newRayGO.AddComponent<MRay>();
                //rayComp.copy(ray);
                LineRenderer lineRenderer = newRayGO.GetComponent<LineRenderer>();
                List<MBlock> blocks = ray.path.blocks;

                lineRenderer.SetWidth(rayWidth, rayWidth);
                //lineRenderer.SetColors(ray.color, ray.color);
                /*
                if (newRayGO.GetComponent<ColorPropSwitch>() != null)
                {
                    newRayGO.GetComponent<ColorPropSwitch>().SwitchByColor(ray.color);
                }
                else
                {
                  lineRenderer.material.color = ray.color;
                }
                */
                if (ray.color == Color.white)
                {
                    lineRenderer.material.color = ray.color;
                }
                else if (ray.color == Color.red)
                {
                    lineRenderer.material.color = new Color(1, 0.3f, 0.3f);
                }
                else if (ray.color == Color.green)
                {
                    lineRenderer.material.color = new Color(0.3f, 1, 0.3f);
                }
                else if (ray.color == Color.blue)
                {
                    lineRenderer.material.color = new Color(0.25f, 0.8f, 0.95f);
                }

                Vector3[] points = new Vector3[blocks.Count];
                List<Vector3> posList = new List<Vector3>();
                for (int i = 0; i < blocks.Count; i++)
                {
                    var block = blocks[i];
                    if (block.type == "mirror")
                    {
                        if (useRayBending)
                        {
                            Vector3 pos1;
                            Vector3 pos2;

                            pos1 = toWorldPosFromGridSide(block.x, block.y, Directions.directionToInt(block.inDir), true);
                            pos2 = toWorldPosFromGridSide(block.x, block.y, Directions.directionToInt(block.outDir), false);

                            //Debug.Log( i + " x: " + block.x + " y: " + block.y + " in: " + block.inDir + " out: " + block.outDir);
                            //Debug.Log("" + i + "in: " + pos1 + " out: " + pos2);
                            posList.Add(GetRayOffsetPos(pos1));
                            posList.Add(GetRayOffsetPos(pos2));
                        }
                        else
                        {
                            Vector3 pos = toWorldPosFromGrid(block.x, block.y);
                            posList.Add(GetRayOffsetPos(pos));
                        }
                    }
                    else if (block.type == "light")
                    {
                        //Vector3 pos = toWorldPosFromGridDirection(block.x, block.y, Directions.directionToInt(block.direction));
                        Vector3 pos = toWorldPosFromGrid(block.x, block.y);
                        posList.Add(GetRayOffsetPos(pos));
                        //Debug.Log("" + i + "dir :" + block.direction);
                    }
                    else
                    {
                        Vector3 pos = toWorldPosFromGrid(block.x, block.y);
                        posList.Add(GetRayOffsetPos(pos));
                    }
                }
                lineRenderer.SetVertexCount(posList.Count);
                lineRenderer.SetPositions(posList.ToArray());
                raysArr.Add(newRayGO);

                newRayGO.transform.parent = rayParent;
                newRayGO.SetActive(true);
            }

        }

        public Vector3 GetRayOffsetPos(Vector3 pos)
        {
            return pos - new Vector3(0, 0.03f, 0);
        }
        public void resetStage()
        {
            //Debug.Log("Reset stage");
            clearRays();

            Transform[] allChildren = transform.GetComponentsInChildren<Transform>();
            for (int index = 0; index < allChildren.Length; index++)
            {
                if (allChildren[index] != transform)
                {
                    Destroy(allChildren[index].gameObject);
                }
            }
            /*
            GameObject[] mapObjects;
            if (hasTag)
            {
                SceneGraphUtils.DestroyByTag(mapObjectTag);
            }
            else
            {
                //SceneGraphUtils.DestroyAllChildren(transform);
            }
            */

        }


        void clearDecorations()
        {
            GameObject[] mapObjects = GameObject.FindGameObjectsWithTag("helpObject");
            foreach (GameObject aObj in mapObjects)
            {
                Destroy(aObj);
            }
        }
        public void clearRays()
        {
            if (raysArr.Count > 0)
            {
                // clear rays
                game.ClearRays();
                foreach (GameObject aRay in raysArr)
                {
                    Destroy(aRay);
                }
                raysArr.Clear();
            }
            else
            {
                //Debug.Log("Nothing to clear");
            }
        }

        public void updateMap()
        {
            clearRays();
            //map.clearWinState();
            game.CastAllRays();
            displayRays();

        }
        public Vector3 toWorldPos(Vector3 pos)
        {
            float mw = map.width / 2f;
            float mh = map.height / 2f;
            return new Vector3(pos.x + 0.5f - mw, pos.y, -(pos.z + 0.5f - mh));
        }
        public Vector3 toWorldPosDirection(Vector3 pos, int connerType = -1)
        {
            float mw = map.width / 2f;
            float mh = map.height / 2f;

            if (connerType == Directions.directionToInt("up"))
            {
                return new Vector3(pos.x + 0.5f - mw, pos.y, -(pos.z + 1 - mh));
            }
            else if (connerType == Directions.directionToInt("down"))
            {
                return new Vector3(pos.x + 0.5f - mw, pos.y, -(pos.z - mh));
            }
            else if (connerType == Directions.directionToInt("left"))
            {
                return new Vector3(pos.x + 1 - mw, pos.y, -(pos.z + 0.5f - mh));
            }
            else if (connerType == Directions.directionToInt("right"))
            {
                return new Vector3(pos.x - mw, pos.y, -(pos.z + 0.5f - mh));
            }
            else
            {
                return toWorldPos(pos);
            }
        }
        public Vector3 toWorldPosFromGrid(int gx, int gy)
        {
            float mw = map.width / 2f;
            float mh = map.height / 2f;
            return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy + 0.5f - mh));

        }
        public Vector3 toWorldPosFromGridSide(int gx, int gy, int connerType = -1, bool isInRay = true)
        {
            float mw = map.width / 2f;
            float mh = map.height / 2f;

            if (isInRay)
            {
                if (connerType == Directions.directionToInt("up"))
                {
                    return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy - mh));
                }
                else if (connerType == Directions.directionToInt("down"))
                {
                    return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy + 1 - mh));
                }
                else if (connerType == Directions.directionToInt("left"))
                {
                    return new Vector3(gx + 1 - mw, stagePosCenter.y, -(gy + 0.5f - mh));
                }
                else if (connerType == Directions.directionToInt("right"))
                {
                    return new Vector3(gx - mw, stagePosCenter.y, -(gy + 0.5f - mh));
                }
            }
            else
            {
                if (connerType == Directions.directionToInt("up"))
                {
                    return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy + 1 - mh));
                }
                else if (connerType == Directions.directionToInt("down"))
                {
                    return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy - mh));
                }
                else if (connerType == Directions.directionToInt("left"))
                {
                    return new Vector3(gx - mw, stagePosCenter.y, -(gy + 0.5f - mh));
                }
                else if (connerType == Directions.directionToInt("right"))
                {
                    return new Vector3(gx + 1 - mw, stagePosCenter.y, -(gy + 0.5f - mh));
                }
            }
            return toWorldPosFromGrid(gx, gy);

        }
        public Vector3 toWorldPosFromGridDirection(int gx, int gy, int connerType = -1)
        {
            float mw = map.width / 2f;
            float mh = map.height / 2f;

            if (connerType == Directions.directionToInt("up"))
            {
                return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy + 1 - mh));
            }
            else if (connerType == Directions.directionToInt("down"))
            {
                return new Vector3(gx + 0.5f - mw, stagePosCenter.y, -(gy - mh));
            }
            else if (connerType == Directions.directionToInt("left"))
            {
                return new Vector3(gx + 1 - mw, stagePosCenter.y, -(gy + 0.5f - mh));
            }
            else if (connerType == Directions.directionToInt("right"))
            {
                return new Vector3(gx - mw, stagePosCenter.y, -(gy + 0.5f - mh));
            }
            else
            {
                return toWorldPosFromGrid(gx, gy);
            }
        }
        public Vector2 toMapPos(Vector3 pos)
        {
            var xint = Mathf.RoundToInt(pos.x - 0.5f + map.width / 2f);
            var yint = Mathf.RoundToInt(-(pos.z + 0.5f - map.height / 2f));

            float x = xint;
            float y = yint;

            if (xint < 0)
            {
                x = 0;
            }
            else if (xint > map.width)
            {
                x = map.width;
            }

            if (yint < 0)
            {
                y = 0;
            }
            else if (yint > map.height)
            {
                y = map.height;
            }

            return new Vector2(x, y);
        }

        void Update()
        {
            //start tracing
        }

        public void ShowHelpAt(int gx, int gy, string type, Color color, float delayTime = 0, float duration = 3)
        {
            GameObject helpObject = null;

            if (type == "circle")
            {
                helpObject = circleHelpObject;
            }
            else if (type == "hand")
            {
                helpObject = handHelpObject;
            }
            else if (type == "cross")
            {
                helpObject = crossHelpObject;
            }

            if (helpObject)
            {
                LeanTween.delayedCall(delayTime, () =>
                {
                    if (helpObject)
                    {
                        helpObject.SetActive(true);
                        helpObject.transform.position = toWorldPosFromGrid(gx, gy);
                        helpObject.GetComponent<Renderer>().material.color = color;
                        LeanTween.scale(helpObject, Vector3.one * 1.4f, 0.5f).setLoopCount(10).setLoopPingPong().setOnComplete(() =>
                        {
                            helpObject.SetActive(false);
                            helpObject.GetComponent<Renderer>().material.color = Color.white;
                        });
                    }
                });

            }
        }

        public void HideHelps()
        {
            circleHelpObject.SetActive(false);
            handHelpObject.SetActive(false);
            crossHelpObject.SetActive(false);
        }
    }
}