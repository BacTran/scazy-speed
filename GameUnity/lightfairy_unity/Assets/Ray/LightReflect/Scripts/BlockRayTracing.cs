﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SGGames.Block;
using System;

namespace SGGames.RayTracing
{
    //[Serializable]
    public class MRayPath: IRayPath 
    {
        public List<MBlock> blocks = new List<MBlock>();

        public MRayPath()
        {
        }



        public List<IRayUnit> Steps()
        {
            throw new System.NotImplementedException();
        }
    }

    public class MRay: IRayUnit
    {
        public string name;
        public MRayPath path;
        public string direction;
        public MBlock lastHit;
        public MBlock lastHit2;
        public bool enable = true;
        public bool finish = false;
        public Color color;
        public MBlock startBlock;
        public MBlock currentBlock;
        public MRay parent;
        public float value;
        public MBlock source;
        public float intensity;
        public float intValue;

        public object Data()
        {
            throw new System.NotImplementedException();
        }

        public IRayCaster Source()
        {
            throw new System.NotImplementedException();
        }

        public void Start()
        {
            
        }

        public bool CheckEnd()
        {
            throw new System.NotImplementedException();
        }

        public IRayUnit PreviousUnit()
        {
            throw new System.NotImplementedException();
        }

        public float TimeTravel()
        {
            throw new System.NotImplementedException();
        }

        public List<IRayProp> Props()
        {
            throw new System.NotImplementedException();
        }


        public string Direction()
        {
            return direction;
        }

        public void copy(MRay ray)
        {
            name = ray.name;
            path = ray.path;
            direction = ray.direction;
            lastHit = ray.lastHit;
            lastHit2 = ray.lastHit2;
            enable = ray.enable;
            finish = ray.finish;
            color = ray.color;
            startBlock = ray.startBlock;
            currentBlock = ray.currentBlock;
            parent = ray.parent;
            value = ray.value;
            source = ray.source;
            intensity = ray.intensity;
            intValue = ray.intValue;
        }
    }
}