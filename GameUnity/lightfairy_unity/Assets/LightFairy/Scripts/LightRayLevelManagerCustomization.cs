﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SGGames.Levels.ObjectModel;
using SGGames.GameItems;
using SGGames.GameItems.ObjectModel;
using SGGames.Levels;

namespace SGGames.RayTracing
{
    public class LightRayLevelItemManager : LevelGameItemsManager
    {
        LightRayLevelManagerCustomization levelCustom;
        LightRayGame game;
        bool preloadLevelData = false;

        public LightRayLevelItemManager(LightRayLevelManagerCustomization levelCustom, int numOfItems)
            : base(numOfItems)
        {
            this.levelCustom = levelCustom;
        }

        protected override void LoadItems()
        {
            int numOfLevel = levelCustom.levelData.Count;
            Items = new List<Level>(numOfLevel);
            //Debug.Log("Num of level :" + numOfLevel);

            for (int i = 0; i < numOfLevel; i++)
            {
                LightRayMap map = new LightRayMap();
                if (levelCustom.levelData[i] != null)
                {
                    if (preloadLevelData)
                    {
                        map.convertMap(levelCustom.levelData[i].text);
                    }

                    LightRayGameLevel level = new LightRayGameLevel(i + 1);
                    map.data = levelCustom.levelData[i].text;
                    level.map = map;
                    
                    Items.Insert(i,level);
                }
            }
        }
    }

    public class LightRayLevelManagerCustomization : MonoBehaviour, ILevelManagerCustom
    {
        public List<TextAsset> levelData;
        public int startAtLevelIndex = 0;
        //public LightRayGame game;

        void Awake()
        {
            //game = FindObjectOfType<LightRayGame>();
        }
		void Start(){
            startAtLevelIndex = (GameManager.Instance.Levels as LevelGameItemsManager).LastUnlockedIndex();
			LevelManager.Instance.SetStartAtLevel(startAtLevelIndex);
		}
        void CreateRandomLevels()
        {

        }

        void CreateRandomMap()
        {
        }
        public GameItemsManager<Level, GameItem> CreateLevelItemsManager()
        {
            if (levelData.Count == 0)
            {
                Debug.LogError("No level data!");
                return null;
            }
            else
            {
                
                LightRayLevelItemManager itemManager = new LightRayLevelItemManager(this, levelData.Count);
                return itemManager;
            }
        }
    }
}