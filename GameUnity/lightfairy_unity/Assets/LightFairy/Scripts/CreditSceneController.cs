﻿using UnityEngine;
using System.Collections;

public class CreditSceneController : MonoBehaviour {
    UIScrollView scrollView;

	// Use this for initialization
	void Start () {
        scrollView = FindObjectOfType<UIScrollView>();
        
	}
	
	// Update is called once per frame
	void Update () {
        if (scrollView)
        {
            scrollView.Scroll(-Time.deltaTime);
        }
	}
}
