﻿using UnityEngine;
using System.Collections;
using SGGames.RayTracing;

public class SimpleGameDialog : MonoBehaviour
{
    public LightRayGame game;
    public bool isPauseGameOnShow = true;
    public bool isHideOtherOnShow = true;
    public bool isTweenAnim = true;
    public bool isUseBlackMatte = true;
    public GameObject blackMatte;

    void Awake()
    {
    }
    // Use this for initialization
    void Start()
    {
        if (game == null)
        {
            game = FindObjectOfType<LightRayGame>();
        }
        //Debug.Log("Dialog Enable!");
    }

    public void Show()
    {
        if (game == null)
        {
            game = FindObjectOfType<LightRayGame>();
        }
        if (isHideOtherOnShow)
        {
            SimpleGameDialog[] dialogs = FindObjectsOfType<SimpleGameDialog>();
            foreach (SimpleGameDialog d in dialogs)
            {
                if (d != this)
                {
                    d.gameObject.SetActive(false);
                }
            }
        }
        if (isPauseGameOnShow)
        {
            
            if (game != null)
            {
                if (game.adsController)
                {
                    game.adsController.ShowBanner();
                }
                LeanTween.pauseAll();
                game.LetPause();
            }
            else
            {
                Debug.Log("Game is null");
            }
        }

        gameObject.SetActive(true);
        if (isTweenAnim)
        {
            if (isUseBlackMatte)
            {
                LeanTween.alpha(blackMatte, 0, 0f);
                LeanTween.alpha(blackMatte, 1, 0.2f);
                blackMatte.SetActive(true);
            }
            Vector3 oldScale = gameObject.transform.localScale;
            gameObject.transform.localScale = oldScale * 0.2f;
            LeanTween.scale(gameObject, oldScale , 0.5f).setEase(LeanTweenType.easeOutElastic).setOnComplete(()=>{
            });
        }
        else
        {
        }
    }

    public void Hide()
    {
        if (isPauseGameOnShow)
        {
            if (game != null)
            {
                if (game.adsController)
                {
                    game.adsController.HideBanner();
                }
                game.LetResume();
                
            }
        }
        if (isUseBlackMatte)
        {
            
            LeanTween.alpha(blackMatte, 0, 0.2f).setOnComplete(()=>{
                blackMatte.SetActive(false);
            });
        }
        if (isTweenAnim)
        {
            Vector3 oldScale = gameObject.transform.localScale;

            LeanTween.scale(gameObject, oldScale * 0.2f, 0.5f).setOnComplete(() =>
            {   
                gameObject.SetActive(false);
                gameObject.transform.localScale = oldScale;
            });
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
