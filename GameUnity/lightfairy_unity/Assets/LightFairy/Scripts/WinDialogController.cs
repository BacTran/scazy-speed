﻿using UnityEngine;
using System.Collections;

public class WinDialogController : SimpleGameDialog {
    public System.Action acceptCallback;
    public System.Action resetCallback;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ChooseAccept()
    {
        if (acceptCallback != null)
        {
            acceptCallback();
        }
    }
    public void ChooseReset()
    {
        if (resetCallback != null)
        {
            resetCallback();
        }
    }

    public void Show(System.Action acceptCallback = null, System.Action resetCallback = null)
    {
        this.acceptCallback = acceptCallback;
        this.resetCallback = resetCallback;
        gameObject.SetActive(true);
        GetComponent<Animator>().Play("WinDialog_Show");
        Vector3 oldScale = transform.localScale;
        transform.localScale = oldScale * 0.1f;
        LeanTween.scale(gameObject, oldScale, 0.5f).setEase(LeanTweenType.easeOutElastic);
    }
}
