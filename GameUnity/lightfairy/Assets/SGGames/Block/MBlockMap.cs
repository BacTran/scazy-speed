﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SGGames.RayTracing;

namespace SGGames.Block
{
    public class MBlockMap
    {
        public int width = 10;
        public int height = 10;
        public MBlock[,] mapData;
        public MBlock[,] mapDataInit;

        public MBlockMap()
        {
            this.mapData = new MBlock[width, height];
            this.mapDataInit = new MBlock[width, height];
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual MBlock getBlock(int x, int y)
        {
            if (((x < 0) || (x >= width)) || ((y < 0) || (y >= height)))
            {
                //throw new System.ArgumentException("X , Y are out of range x: " + x +" y : "+y);
                return null;

            }
            else
            {
                return mapData[x, y];
            }
        }
        public virtual void addBlock(int x, int y, MBlock block)
        {
            if (((x < 0) || (x > width)) || ((y < 0) || (y > height)))
            {
                throw new System.ArgumentException("X , Y are out of range x: " + x + " y : " + y);

            }
            else
            {
                mapData[x, y] = block;
                block.x = x;
                block.y = y;
            }
        }
        public virtual void moveBlock(MBlock block, Vector2 newPos)
        {
            int x = Mathf.RoundToInt(newPos.x);
            int y = Mathf.RoundToInt(newPos.y);
            var empty = getBlock(x, y);

            if (empty.type == "empty")
            {

                var newEmpty = new MBlock();
                mapData[block.x, block.y] = newEmpty;
                newEmpty.x = block.x;
                newEmpty.y = block.y;

                block.x = x;
                block.y = y;
                mapData[x, y] = block;
            }
        }

        public virtual void clearBlocks()
        {
            this.mapData = new MBlock[width, height];
        }

        public List<MBlock> getAllBlockByType(string type)
        {
            List<MBlock> result = new List<MBlock>();
            foreach (MBlock b in mapData)
            {
                if (b != null)
                {
                    if (b.type == type)
                    {
                        result.Add(b);
                    }
                }
            }
            return result;
        }
        public void SaveData()
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height - 1; j++)
                {
                    if (mapData[i, j] != null)
                    {
                        mapDataInit[i, j] = CloneTypeCheck(mapData[i, j]);
                    }
                    else
                    {
                        mapDataInit[i, j] = null;
                    }
                }
            }
        }
        public virtual MBlock CloneTypeCheck(MBlock mBlock)
        {
            return null;
        }

        public virtual void ResetData()
        {
            Debug.Log("Reset map data");
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height - 1; j++)
                {
                    if (mapDataInit[i, j] != null)
                    {
                        mapData[i, j] = CloneTypeCheck(mapDataInit[i, j]);
                    }
                    else
                    {
                        mapData[i, j] = null;
                    }
                }
            }
        }
        public virtual MBlock emptyBlock()
        {
            return new MBlock();
        }
        public virtual MBlock getNextBlock(MBlock currentBlock, string direction)
        {
            int nextx = -1;
            int nexty = -1;
            var currentx = currentBlock.x;
            var currenty = currentBlock.y;
            switch (direction)
            {
                case "up": nextx = currentx; nexty = currenty + 1; break;
                case "down": nextx = currentx; nexty = currenty - 1; break;
                case "left": nextx = currentx - 1; nexty = currenty; break;
                case "right": nextx = currentx + 1; nexty = currenty; break;
            }
            return getBlock(nextx, nexty);
        }
    }
}
