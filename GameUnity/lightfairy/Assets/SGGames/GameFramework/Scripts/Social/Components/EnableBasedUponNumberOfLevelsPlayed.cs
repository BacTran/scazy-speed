﻿




using SGGames.GameObjects.Components;
using SGGames;

namespace SGGames.Social.Components
{
    /// <summary>
    /// Shows an enabled or a disabled gameobject based upon the number of levels played
    /// </summary>
    public class EnableBasedUponNumberOfLevelsPlayed : EnableDisableGameObject
    {
        public int NumberOfLevels;

        public override bool IsConditionMet()
        {
            return GameManager.Instance.TimesLevelsPlayed >= NumberOfLevels;
        }
    }
}