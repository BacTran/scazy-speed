﻿




using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;
using SGGames.Levels.ObjectModel;

namespace SGGames.Levels.Components
{
    /// <summary>
    /// Creates instances of all Level GameItems
    /// </summary>
    public class CreateLevelButtons : CreateGameItemButtons<LevelButton, Level>
    {
        protected override GameItem[] GetGameItems()
        {
            return GameManager.Instance.Levels.Items.ToArray();
        }
    }
}