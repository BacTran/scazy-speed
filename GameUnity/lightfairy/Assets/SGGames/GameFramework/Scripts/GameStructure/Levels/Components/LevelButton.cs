﻿#if UNITY_PURCHASING
using SGGames.Billing.Components;
#endif
using System.Runtime.Remoting.Messaging;
using SGGames.GameItems;
using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;
using SGGames.Levels.ObjectModel;
using SGGames.UI.Other.Components;
using UnityEngine;

namespace SGGames.Levels.Components
{
    /// <summary>
    /// Level Details Button
    /// </summary>
    public class LevelButton : GameItemButton<Level>
    {
        public new void Awake()
        {
            base.Awake();

#if UNITY_PURCHASING
            if (PaymentManager.Instance != null)
                PaymentManager.Instance.LevelPurchased += UnlockIfNumberMatches;
#endif
        }

        protected new void OnDestroy()
        {
#if UNITY_PURCHASING
            if (PaymentManager.Instance != null)
                PaymentManager.Instance.LevelPurchased -= UnlockIfNumberMatches;
#endif

            base.OnDestroy();
        }

        protected override GameItemsManager<Level, GameItem> GetGameItemsManager()
        {
            return GameManager.Instance.Levels;
        }
    }
}