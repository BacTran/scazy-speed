﻿




using System;
using UnityEngine;
using UnityEngine.SceneManagement;

using SGGames.UI.Dialogs.Components;
using SGGames.GameObjects.Components;
using SGGames.Levels.ObjectModel;
using SGGames.GameItems;
using SGGames.GameItems.ObjectModel;
namespace SGGames.Levels
{
    /// <summary>
    /// Manages the concept of a level
    /// </summary>
    public class LevelManager : Singleton<LevelManager>
    {
        /// <summary>
        /// Whether LevelStarted should be called on Start()
        /// </summary>
        public bool AutoStart;
        public string levelSelectSceneName = "LevelSelect";
        [Header("Basic Level Setup")]
        public LevelGameItemsManager.LevelUnlockModeType LevelUnlockMode;
        public int NumberOfStandardLevels = 10;
        public int CoinsToUnlockLevels = 10;
        public int UnlockInitLevelNum = 4;

        public DateTime StartTime { get; set; }
        public float SecondsRunning { get; set; }
        public bool IsLevelStarted { get; set; }
        public bool IsLevelFinished { get; set; }
        public bool IsLevelRunning { get { return IsLevelStarted && !IsLevelFinished; } }
        public Level Level { get { return GameManager.Instance.Levels != null ? GameManager.Instance.Levels.Selected : null; } }

        public GameItemsManager<Level, GameItem> LevelItemsManager
        {
            get
            {
                return GameManager.Instance.Levels;
            }
        }


        public delegate void OnLevelFinishDelegate(Level level);
        public event OnLevelFinishDelegate OnLevelFinish;
        public ILevelManagerCustom levelManagerCustom;

        protected override void GameSetup()
        {
            base.GameSetup();
            SetupLevels();
            Reset();
        }

        private void SetupLevels()
        {
            if (GetComponent<ILevelManagerCustom>() == null)
            {
                GameManager.Instance.Levels = new LevelGameItemsManager(NumberOfStandardLevels, LevelUnlockMode, CoinsToUnlockLevels, UnlockInitLevelNum);
            }
            else
            {
                GameManager.Instance.Levels = GetComponent<ILevelManagerCustom>().CreateLevelItemsManager();
            }
            GameManager.Instance.Levels.Load();
        }
        void Start()
        {
            if (AutoStart)
                LevelStarted();
        }

        public void Reset()
        {
            IsLevelStarted = false;
            IsLevelFinished = false;

            if (Level != null)
            {
                Level.Coins = 0;
                Level.Score = 0;
            }
        }

        public void ShowLevelSelect()
        {
            Debug.Log("Show level select");
            DialogManager.Instance.Show("GeneraMessage", "Are you sure to quit level?", null, null, null, null, null, null, GoToLevelSelectScene, DialogInstance.DialogButtonsType.OkCancel);
        }
        public void GoToLevelSelectScene(DialogInstance dialog)
        {
            if (dialog.DialogResult == DialogInstance.DialogResultType.Cancel)
            {

            }
            else
            {
                DialogManager.Instance.RemoveAllDialogs();
                if (levelSelectSceneName != null)
                {
                    SceneManager.LoadScene(levelSelectSceneName);
                }
            }
        }
        public void LevelStarted()
        {
            StartTime = DateTime.Now;
            SecondsRunning = 0f;
            IsLevelStarted = true;
        }

        public void LevelFinished()
        {
            IsLevelFinished = true;
            GameManager.Instance.TimesLevelsPlayed++;
            GameManager.Instance.TimesPlayedForRatingPrompt++;
        }


        public bool NextLevel()
        {
            //Unlock
            if (!HasMoreLevel())
            {
                return false;
            }
            else
            {
                int index = LevelItemsManager.Items.IndexOf(LevelItemsManager.Selected);
                LevelItemsManager.Selected = LevelItemsManager.Items[index + 1];
                return true;
            }
        }

        public bool HasMoreLevel()
        {
            return LevelItemsManager.Selected.Number != LevelItemsManager.Items.Count;
        }
        public Level GetCurrentLevel()
        {
            return LevelItemsManager.Selected;
        }
        public void SetStartAtLevel(int startAtLevelIndex)
        {
            LevelItemsManager.ChangeSelectedIndex(startAtLevelIndex);
        }
        public void SetStartAtLevelNumber(int levelNumber)
        {
            LevelItemsManager.ChangeSelectedNumber(levelNumber);
        }
        void Update()
        {
            if (IsLevelStarted && !IsLevelFinished)
            {
                SecondsRunning += Time.deltaTime;
            }
        }

        public int CurrentLevelIndex()
        {
            return LevelItemsManager != null ? LevelItemsManager.Selected.Number : 0;
        }
    }
}