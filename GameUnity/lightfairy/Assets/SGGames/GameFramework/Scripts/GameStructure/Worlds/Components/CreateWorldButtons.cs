﻿




using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;
using SGGames.Worlds.ObjectModel;

namespace SGGames.Worlds.Components
{
    /// <summary>
    /// Creates instances of all World GameItems
    /// </summary>
    public class CreateWorldButtons : CreateGameItemButtons<WorldButton, World>
    {
        protected override GameItem[] GetGameItems()
        {
            return GameManager.Instance.Worlds.Items.ToArray();
        }
    }
}