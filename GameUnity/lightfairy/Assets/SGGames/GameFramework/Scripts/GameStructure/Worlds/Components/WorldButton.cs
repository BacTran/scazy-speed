﻿




#if UNITY_PURCHASING
using SGGames.Billing.Components;
#endif
using System.Runtime.Remoting.Messaging;
using SGGames.GameItems;
using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;
using SGGames.Worlds.ObjectModel;
using SGGames.UI.Other.Components;
using UnityEngine;

namespace SGGames.Worlds.Components
{
    /// <summary>
    /// World Details Button
    /// </summary>
    public class WorldButton : GameItemButton<World>
    {
        public new void Awake()
        {
            base.Awake();

#if UNITY_PURCHASING
            if (PaymentManager.Instance != null)
                PaymentManager.Instance.WorldPurchased += UnlockIfNumberMatches;
#endif
        }

        protected new void OnDestroy()
        {
#if UNITY_PURCHASING
            if (PaymentManager.Instance != null)
                PaymentManager.Instance.WorldPurchased -= UnlockIfNumberMatches;
#endif

            base.OnDestroy();
        }

        protected override GameItemsManager<World, GameItem> GetGameItemsManager()
        {
            return GameManager.Instance.Worlds;
        }
    }
}