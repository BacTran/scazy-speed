﻿




using SGGames.Characters.ObjectModel;
using SGGames.GameItems;
using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;

namespace SGGames.Characters.Components
{
    /// <summary>
    /// Unlock GameItem button for Characters 
    /// </summary>
    public class UnlockCharacterButton : UnlockGameItemButton<Character>
    {
        protected override GameItemsManager<Character, GameItem> GetGameItemsManager()
        {
            return GameManager.Instance.Characters;
        }
    }
}