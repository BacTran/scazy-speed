﻿




#if UNITY_PURCHASING
using SGGames.Billing.Components;
#endif
using SGGames.Characters.ObjectModel;
using SGGames.GameItems;
using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;

namespace SGGames.Characters.Components
{
    /// <summary>
    /// Character Details Button
    /// </summary>
    public class CharacterButton : GameItemButton<Character>
    {

        public new void Awake()
        {
            base.Awake();

#if UNITY_PURCHASING
            if (PaymentManager.Instance != null)
                PaymentManager.Instance.CharacterPurchased += UnlockIfNumberMatches;
#endif
        }

        protected new void OnDestroy()
        {
#if UNITY_PURCHASING
            if (PaymentManager.Instance != null)
                PaymentManager.Instance.CharacterPurchased -= UnlockIfNumberMatches;
#endif

            base.OnDestroy();
        }

        protected override GameItemsManager<Character, GameItem> GetGameItemsManager()
        {
            return GameManager.Instance.Characters;
        }
    }
}