﻿




using SGGames.Characters.ObjectModel;
using SGGames.GameItems.Components;
using SGGames.GameItems.ObjectModel;

namespace SGGames.Characters.Components
{
    /// <summary>
    /// Creates instances of all Character Game Items
    /// </summary>
    public class CreateCharacterButtons : CreateGameItemButtons<CharacterButton, Character>
    {
        protected override GameItem[] GetGameItems()
        {
            return GameManager.Instance.Characters.Items.ToArray();
        }
    }
}