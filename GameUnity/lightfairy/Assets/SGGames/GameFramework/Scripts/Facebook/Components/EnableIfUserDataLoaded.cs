﻿




using SGGames.GameObjects.Components;

namespace SGGames.Facebook.Components
{
    /// <summary>
    /// Shows an enabled or a disabled gameobject based upon whether the users data is loaded
    /// </summary>
    public class EnableIfUserDataLoaded : EnableDisableGameObject
    {
        public override bool IsConditionMet()
        {
#if FACEBOOK_SDK
            return FacebookManager.Instance.IsLoggedIn && FacebookManager.Instance.IsUserDataLoaded;
#else
            return false;
#endif
        }
    }
}