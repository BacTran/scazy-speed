﻿using SGGames.Display.Placement;
using UnityEngine;

namespace SGGames.InputHelper
{
    /// <summary>
    /// Input helper functions
    /// </summary>
    public class InputHelper
    {
        public static Vector3 GetMousePositionOnXzPlane()
        {
            return PositionHelper.GetPositionOnXzPlane(UnityEngine.Input.mousePosition);
        }

        public static Vector3 GetMousePositionOnXzPlane(float y)
        {
            return PositionHelper.GetPositionOnXzPlane(UnityEngine.Input.mousePosition, y);
        }

        public static Vector3 GetMousePositionOnPlane(Plane plane)
        {
            return PositionHelper.GetPositionOnPlane(UnityEngine.Input.mousePosition, plane);
        }
    }
}