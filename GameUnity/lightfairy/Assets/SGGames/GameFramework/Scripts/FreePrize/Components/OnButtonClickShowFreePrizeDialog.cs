﻿




using SGGames.GameObjects.Components;
using UnityEngine.Assertions;

namespace SGGames.FreePrize.Components
{
    /// <summary>
    /// Show the free prize dialog when the button is clicked.
    /// 
    /// This automatically hooks up the button onClick listener
    /// </summary>
    public class OnButtonClickShowFreePrizeDialog : OnButtonClick
    {
        public override void OnClick()
        {
            Assert.IsTrue(FreePrizeManager.IsActive, "You need to add the FreePrizeManager to the scene.");

            FreePrizeManager.Instance.ShowFreePrizeDialog();
        }
    }
}