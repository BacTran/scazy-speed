﻿




using SGGames.GameObjects.Components;

namespace SGGames.Localisation.Components
{
    /// <summary>
    /// Enabled or a disabled a gameobject based upon whether the facebook SDK is installed
    /// </summary>
    public class EnableIfMultipleLanguages : EnableDisableGameObject
    {
        public override bool IsConditionMet()
        {
            return Localisation.LocaliseText.AllowedLanguages.Length > 1;
        }
    }
}