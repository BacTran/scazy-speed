﻿




using UnityEngine.Networking;

namespace SGGames.Networking
{
    /// <summary>
    /// 
    /// NOTE: This class is beta and subject to changebreaking change without warning.
    /// </summary>
    public class SetupLocalNetworking : NetworkDiscovery
    {
        public override void OnReceivedBroadcast(string fromAddress, string data)
        {
            //MyDebug.LogF("SetupLocalNetworking,OnReceivedBroadcast: {0} ({1})", fromAddress, data);

            NetworkPlayManager.Instance.OnReceivedBroadcast(fromAddress, data);
        }

    }
}