﻿




using SGGames;
using SGGames.Levels;
using UnityEngine;

namespace SGGames.Display.Placement.Components
{
    /// <summary>
    /// Move this gameobject at a given rate.
    /// </summary>
    public class FixedMovement : MonoBehaviour
    {
        /// <summary>
        /// Scrolling Speed
        /// </summary>
        public Vector3 Speed = new Vector3(0, 0, 1);

        void Update()
        {
#pragma warning disable 618
            if (!LevelManager.Instance.IsLevelRunning || GameManager.Instance.IsPaused)
#pragma warning restore 618
                return;

            transform.Translate(Speed * Time.deltaTime);
        }
    }
}