﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace SGGames
{
    public class SceneGraphUtils
    {
        public static Text getText(string str)
        {
            Text rc = null;
            GameObject go = GameObject.Find(str);
            if (go)
            {
                rc = go.GetComponent<Text>();
            }
            return rc;
        }

        public static void setChildrenActive(GameObject go,
                                                    bool active)
        {
            Transform t0 = go.transform;
            int t = t0.childCount;
            for (int i = 0; i < t; i++)
            {
                if (t0.gameObject != go)
                {
                    t0.gameObject.SetActive(active);
                }
            }
        }
        public static void SetActive(GameObject go,
                                                bool active)
        {
            Transform t0 = go.transform;
            int t = t0.childCount;
            for (int i = 0; i < t; i++)
            {
                t0.gameObject.SetActive(active);
            }
        }

        public static void createAndDestroyGameObject(GameObject effectGO,
                                                        Vector3 pos,
                                                        float effectTTL)
        {
            /*
            if (effectGO)
            {
                GameObject g0 = (GameObject)Instantiate(effectGO, pos, Quaternion.identity);
                if (g0)
                {
                    Destroy(g0, effectTTL);
                }
            }
             */

        }

        public static void slideOut(GameObject go, bool slideOut)
        {
            if (go)
            {
                Animator animator = go.GetComponent<Animator>();
                if (animator)
                {
                    animator.SetBool("SlideOut", slideOut);
                }

            }

        }

        public static void fadeInFadeOut(GameObject go1, GameObject go2)
        {
            if (go1)
            {
                go1.SetActive(true);
            }

            if (go2)
            {
                go2.SetActive(true);
            }

            slideOut(go1, true);
            slideOut(go2, false);
        }

        public static Component getComponentInChildrenNotSelf(Transform t1, string scriptName)
        {
            Component rc = null;
            for (int i = 0; i < t1.childCount; i++)
            {
                Transform t0 = t1.GetChild(i);
                if (t0 != t1)
                {
                    rc = t0.GetComponent(scriptName);
                    i = t1.childCount;
                }
            }
            return rc;
        }
        public static void setChildrenActiveRecursively(Transform t1, bool state)
        {
            for (int i = 0; i < t1.childCount; i++)
            {
                Transform t0 = t1.GetChild(i);
                if (t0 != t1)
                {
                    t0.gameObject.SetActive(state);
                }
            }
        }
    }
}