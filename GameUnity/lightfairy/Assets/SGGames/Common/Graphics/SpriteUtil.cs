﻿using UnityEngine;
using System.Collections;

namespace SGGames
{
    public class SpriteUtil
    {

        public static Vector3 GetSizeInPixels(GameObject gameObject)
        {
            Vector2 sprite_size = gameObject.GetComponent<SpriteRenderer>().sprite.rect.size;
            Vector2 local_sprite_size = sprite_size / gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
            Vector3 world_size = local_sprite_size;
            world_size.x *= gameObject.transform.lossyScale.x;
            world_size.y *= gameObject.transform.lossyScale.y;

            //convert to screen space size
            Vector3 screen_size = 0.5f * world_size / Camera.main.orthographicSize;
            screen_size.y *= Camera.main.aspect;

            //size in pixels
            Vector3 in_pixels = new Vector3(screen_size.x * Camera.main.pixelWidth, screen_size.y * Camera.main.pixelHeight, 0) * 0.5f;
            return in_pixels;
        }

        public static Vector3 GetSizeScaled(GameObject gameObject)
        {
            Vector2 sprite_size = gameObject.GetComponent<SpriteRenderer>().sprite.rect.size;
            Vector2 local_sprite_size = sprite_size / gameObject.GetComponent<SpriteRenderer>().sprite.pixelsPerUnit;
            Vector3 world_size = local_sprite_size;
            world_size.x *= gameObject.transform.lossyScale.x;
            world_size.y *= gameObject.transform.lossyScale.y;


            return world_size;
        }
    }
}