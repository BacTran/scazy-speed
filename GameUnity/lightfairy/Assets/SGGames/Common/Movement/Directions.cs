﻿using UnityEngine;
using System.Collections;

namespace SGGames
{
    public enum FourDirection
    {
        Up, Right, Down, Left
    }

    public class Directions
    {
        public static int directionToInt(string direction)
        {
            switch (direction)
            {
                case "down": return 0;
                case "right": return 1;
                case "up": return 2;
                case "left": return 3;

                default: return -1;
            }
        }

        public static string intToDirection(int i)
        {
            switch (i % 4)
            {
                case 0: return "down";
                case 1: return "right";
                case 2: return "up";
                case 3: return "left";
                default: return "";
            }
        }
    }
}
