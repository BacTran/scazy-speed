﻿using UnityEngine;
using System.Collections;

public class TransformData
{
    public Vector3 position;
    public Quaternion rotation;

    public Vector3 localPosition;
    public Vector3 localScale;
    public Quaternion localRotation;

    public Transform parent;


    public void CopyTo(Transform transform)
    {

        transform.position = this.position;
        transform.localPosition = this.localPosition;

        transform.rotation = this.rotation;
        transform.localRotation = this.localRotation;

        transform.localScale = this.localScale;

        transform.parent = this.parent;
    }

    public override string ToString()
    {
        return position.ToString();
    }
}
