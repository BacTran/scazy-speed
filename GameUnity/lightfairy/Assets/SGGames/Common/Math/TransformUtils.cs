﻿using UnityEngine;
using System.Collections;



public static class TransformUtils
{
    public static TransformData Clone(this TransformData transform)
    {
        TransformData td = new TransformData();

        td.position = transform.position;
        td.localPosition = transform.localPosition;

        td.rotation = transform.rotation;
        td.localRotation = transform.localRotation;

        td.localScale = transform.localScale;

        td.parent = transform.parent;

        return td;
    }

    public static TransformData Clone(this Transform transform)
    {
        TransformData td = new TransformData();

        td.position = transform.position;
        td.localPosition = transform.localPosition;

        td.rotation = transform.rotation;
        td.localRotation = transform.localRotation;

        td.localScale = transform.localScale;

        td.parent = transform.parent;

        return td;
    }
    
}
