﻿using UnityEngine;
using System.Collections;
using SGGames;
namespace SGGames { 
    public class ExplosionMaker {

        public static IEnumerator explodePartIE(GameObject go,
                                                    float explodeDelayTime,
                                                    Vector3 hitPos,
                                                    float partsTTL,
                                                    int junkLayer,
                                                    float explosionPower,
                                                    float explosionRadius)
        {
            yield return new WaitForSeconds(explodeDelayTime);
            /*
            DamagableTrigger[] dt = go.GetComponentsInChildren<DamagableTrigger>();

            for (int i = 0; i < dt.Length; i++)
            {
                Transform t = dt[i].transform;
                t.parent = null;
                t.gameObject.layer = junkLayer;
                FadeOut fadeOut = t.gameObject.AddComponent<FadeOut>();
                if (fadeOut)
                {
                    fadeOut.startFadeOut(partsTTL);
                }

                if (t.GetComponent<Collider>())
                {
                    t.GetComponent<Collider>().isTrigger = false;
                }
                if (t.GetComponent<Rigidbody>())
                {
                    t.GetComponent<Rigidbody>().isKinematic = false;
                    t.GetComponent<Rigidbody>().AddExplosionForce(explosionPower, hitPos, explosionRadius, 3);
                }
                //DestroyObject(t.gameObject, partsTTL);
            }
            //Destroy(go);
             */ 
        }
    }
}