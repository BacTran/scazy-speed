﻿using UnityEngine;
using System.Collections;
namespace SGGames
{
    public interface IWeight
    {
        float Weight();
    }
}