﻿using UnityEngine;
using System.Collections;

namespace SGGames
{
    public class GameDescription 
    {
        public string title;
        public string subtitle;
        public string releaseDate;
        public string genre;
        public string description;
        public int price;
        public bool isFree;
        public string iconUrl;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}