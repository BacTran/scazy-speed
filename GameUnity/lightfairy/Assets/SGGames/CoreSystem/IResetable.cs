﻿using UnityEngine;
using System.Collections;

namespace SGGames
{
    public interface IResetable
    {

        void Reset();

        bool CanReset();
    }
}