﻿using UnityEngine;
using System.Collections;
using SGGames;
using SGGames.Levels.ObjectModel;
using SGGames.Levels;
using UnityEngine.SceneManagement;

public class NGUILevelBoard : MonoBehaviour
{
    public UIScrollView scrollView;
    public UITable table;
    public GameObject btnPrefab;
    public bool useGrid = false;
    void Start()
    {
        //Debug.Log("Levels" + GameManager.Instance.Levels.Items.Count);

        if (useGrid)
        {
            foreach (Level l in GameManager.Instance.Levels.Items)
            {
                GameObject newButton = NGUITools.AddChild(table.gameObject, btnPrefab);

                newButton.SetActive(true);
                newButton.transform.FindChild("Label").GetComponent<UILabel>().text = "" + l.Number;
            }

            table.Reposition();
        }
        else
        {
            UIButton[] buttons = GetComponentsInChildren<UIButton>();
            int index = 1;
            foreach (UIButton b in buttons)
            {
                if (b != null)
                {
                    b.gameObject.GetComponentInChildren<UILabel>().text = "" + index;
                    int startAtIndex = index - 1;
                    if (LevelManager.Instance.LevelItemsManager.Items[startAtIndex].IsUnlocked)
                    {
                        b.onClick.Add(new EventDelegate(() =>
                        {

                            LevelManager.Instance.SetStartAtLevel(startAtIndex);
                            LoadInGame();

                        }));
                    }
                    else
                    {
                        b.GetComponent<UISprite>().color = Color.gray;
                        b.enabled = false;
                    }
                    index++;
                }
            }
        }
    }

    private void LoadInGame()
    {
        SceneManager.LoadScene("ForestScene");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
