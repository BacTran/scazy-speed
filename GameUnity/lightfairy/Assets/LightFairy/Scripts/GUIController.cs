﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {


    public GameObject confirmDialog;
    public GameObject winDialog;
    //public AudioSource audioSource;
    public UISlider audioVolumeSlider;


	void Start () {
        /*
        if (audioSource == null)
        {
            audioSource = FindObjectOfType<AudioSource>();
        }
         */ 
        /*
        if (audioVolumeSlider != null)
        {
            audioVolumeSlider.onChange.Add(new EventDelegate(()=>{
                SetAudioVolume();
            }));
        }
         */ 
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void ToggleSound()
    {
        foreach (AudioSource audioSource in FindObjectsOfType<AudioSource>())
        {
            audioSource.mute = !audioSource.mute;
        }
    }
    public void SetAudioVolume()
    {
        foreach (AudioSource audioSource in FindObjectsOfType<AudioSource>())
        {
            audioSource.volume = audioVolumeSlider.value;
        }
    }

    public void ShowWinDialog(System.Action onNext,System.Action onReset)
    {
        winDialog.GetComponent<WinDialogController>().Show(onNext, onReset);
    }


}
