﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HelpDialogController : MonoBehaviour
{
    public List<UIWidget> widgetList;
    int currentPage = 0;
    // Use this for initialization
    void Start()
    {
        UpdatePages();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NextPage()
    {
        if (currentPage + 1 <= widgetList.Count - 1)
        {
            currentPage++;
        }
        else
        {
            currentPage = 0;
        }
        UpdatePages();

    }

    private void UpdatePages()
    {
        foreach (UIWidget w in widgetList)
        {
            if (widgetList.IndexOf(w) != currentPage)
            {
                w.gameObject.SetActive(false);
            }
            else
            {
                w.gameObject.SetActive(true);
            }
        }
    }

    public void PrevPage()
    {
        if (currentPage - 1 >= 0)
        {
            currentPage--;
        }
        else
        {
            currentPage = widgetList.Count - 1;
        }
        UpdatePages();
    }
}
