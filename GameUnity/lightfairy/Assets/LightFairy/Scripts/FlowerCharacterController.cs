﻿using UnityEngine;
using System.Collections;
using SGGames.RayTracing;

public class FlowerCharacterController : MonoBehaviour
{
    public string faceType;
    public Animator faceAnimator;
    public GameStage gameStage;

    // Use this for initialization
    void Start()
    {
        if (gameStage == null)
        {
            gameStage = FindObjectOfType<GameStage>();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetEmotion(string emotion)
    {
        faceAnimator.Play(faceType + "-" + emotion);
    }
    public void SetFace(string newFace)
    {
        //this.faceType = newFaceType;
    }
}
