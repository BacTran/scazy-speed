﻿using UnityEngine;
using System.Collections;
using SGGames.RayTracing;

public class FairyCharacter : MonoBehaviour
{
    public UISprite textBox;
    public UILabel text;
    public LightRayGame game;
    int introNum;
    private int levelNum;
    // Use this for initialization
    void Start()
    {
        if (game == null)
        {
            game = FindObjectOfType<LightRayGame>();
        }
        if (text == null)
        {
            text = GetComponentInChildren<UILabel>();
        }
        //MoveOut();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ShowIntro(int num)
    {
        this.levelNum = num;
        //this.GetComponent<UITexture>().alpha = 1f;
        //MoveIn();
        game.gameStage.HideHelps();
        if (levelNum == 1)
        {
            
            ShowText("Adventurer, please save our forest! Flowers need light!",5);
            LeanTween.delayedCall(5.5f, () =>
            {
                game.gameStage.ShowHelpAt(5, 3, "circle", Color.white, 0, 4);
                game.gameStage.ShowHelpAt(4, 7, "hand", Color.white, 0, 4);
                game.gameStage.ShowHelpAt(9, 3, "circle", Color.blue, 4, 4);
                ShowText("Use mirror to reflect light toward flower.");
            });
        }
        else if (levelNum == 2)
        {
            ShowText("Daisy only be saved with white light!",5);
            LeanTween.delayedCall(5.5f, () =>
            {
                ShowText("Likewise Catus needs Green. Rose needs Red. Tulip needs Blue.");
            });
        }
        else if (levelNum == 3)
        {
            ShowText("You can split the light also...",5);
            LeanTween.delayedCall(5.5f, () =>
            {
                ShowText("The splited lights will be in 3 colors separately.");
            });
        }
        else if (levelNum == 4)
        {
            ShowText("Color block will change light's color...");
        }
        else if (levelNum == 12)
        {
            ShowText("Use mirrors creatively to win.");
        }
    }

    private void MoveIn()
    {
        this.GetComponent<UITexture>().leftAnchor.absolute += 60;
        this.GetComponent<UITexture>().rightAnchor.absolute += 60;
    }

    private void MoveOut()
    {
        this.GetComponent<UITexture>().leftAnchor.absolute -= 60;
        this.GetComponent<UITexture>().rightAnchor.absolute -= 60;
    }
    public void NextIntro()
    {

    }

    void OnDestroy()
    {
        LeanTween.cancelAll();
    }
    public void ShowText(string content, float duration = 5)
    {
        if (textBox)
        {
            textBox.gameObject.SetActive(true);

            LeanTween.alpha(textBox.gameObject, 1, 0.5f);
            text.text = content;

            LeanTween.delayedCall(duration, () =>
            {
                if (textBox != null)
                {
                    HideText();
                }
            });
        }
    }

    public void HideText()
    {
        //MoveOut();
        //this.GetComponent<UITexture>().alpha =0.5f;
        text.text = "";
        LeanTween.alpha(textBox.gameObject, 0, 0.5f);
        textBox.gameObject.SetActive(false);

    }
}
