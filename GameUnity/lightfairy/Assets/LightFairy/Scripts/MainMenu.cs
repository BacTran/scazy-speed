﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
    public GameObject MainMenuScene;
    public GameObject LevelSelectScene;
    public GameObject CreditScene;
    public AudioSource audioSource;
    public FairyCharacter fairy;
    public AdsController adsController;


	// Use this for initialization
	void Start () {
        if (MainMenuScene == null)
        {
            //MainMenuScene = GameObject.Find("MainMenuScene");
        }

        if (LevelSelectScene == null)
        {
            //LevelSelectScene = GameObject.Find("LevelSelectScene");
        }

        if (CreditScene == null)
        {
            //LevelSelectScene = GameObject.Find("CreditScene");
        }
        if (audioSource == null)
        {
            //audioSource = FindObjectOfType<AudioSource>();
        }
        if (adsController == null)
        {
            adsController = GetComponent<AdsController>();
            if (adsController == null)
            {
                adsController = FindObjectOfType<AdsController>();
            }
        }
        ShowMainMenu();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void ToggleSound()
    {
        audioSource.mute = !audioSource.mute;
    }
    public void ShowMoreGames()
    {
        #if UNITY_IPHONE
                Application.OpenURL("https://play.google.com/store/apps/developer?id=SGGamesVN");
        #elif UNITY_ANDROID
                Application.OpenURL("https://play.google.com/store/apps/developer?id=SGGamesVN");
        #elif UNITY_WP8
                Application.OpenURL("https://play.google.com/store/apps/developer?id=SGGamesVN");
        #endif

    }
    public void ShowMainMenu()
    {
		//adsController.ShowInterstitial();
        adsController.ShowBanner();
        MainMenuScene.SetActive(true);
        LevelSelectScene.SetActive(false);
        CreditScene.SetActive(false);
    }

    public void ShowLevelSelect()
    {
        adsController.ShowBanner();
        MainMenuScene.SetActive(false);
        LevelSelectScene.SetActive(true);
    }

    public void ShowCreditScene()
    {
        adsController.ShowBanner();
        MainMenuScene.SetActive(false);
        CreditScene.SetActive(true);
    }
}
