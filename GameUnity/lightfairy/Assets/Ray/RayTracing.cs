﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SGGames.RayTracing
{
    public interface IRayProp
    {
        object Data();
    }
    public interface IRayCaster
    {
        IRayCaster Ray();
        void Start();
        bool CheckEnd();
    }
    public interface IRayUnit
    {
        object Data();

        IRayCaster Source();

        void Start();

        bool CheckEnd();

        IRayUnit PreviousUnit();
        float TimeTravel();

        List<IRayProp> Props();

        string Direction();
    }
    public interface IRayModifier
    {
        IRayUnit Change(IRayUnit original);
    }
    public interface IRaySpliter
    {
        List<IRayUnit> Split();
    }
    public interface IRayConverter
    {
        IRayUnit Convert(IRayUnit original);
    }

    public interface IRayCombiner
    {

    }

    public interface IRayPath
    {
        List<IRayUnit> Steps();
    }

    public interface IRayTracer
    {
        List<IRayCaster> Sources();

        List<IRayUnit> Rays();

        List<IRayPath> Paths();

        void CastAllRays();

        void ClearRays();

        bool CheckEnd();
    }
}