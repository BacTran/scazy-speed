﻿using UnityEngine;
using System.Collections;
using SGGames.Block;

namespace SGGames.RayTracing
{
    public enum MColor
    {
        Red, Green, Blue, White
    }

    public class MWall : MBlock
    {
        public MWall()
            : base("wall")
        {
            this.type = "wall";
        }

        public MWall cloneInstance()
        {
            MWall newInstance = new MWall();
            newInstance.copy(this);
            return newInstance;
        }
    }
    public class MMirror : MBlock
    {
        public MMirror()
            : base("mirror")
        {
            this.type = "mirror";
        }

        public MMirror cloneInstance()
        {
            MMirror newInstance = new MMirror();
            newInstance.copy(this);
            return newInstance;
        }
    }

    public class MSplit : MBlock
    {
        public MSplit()
            : base("split")
        {
            this.type = "split";
        }

        public MSplit cloneInstance()
        {
            MSplit newInstance = new MSplit();
            newInstance.copy(this);
            return newInstance;
        }
    }

    public class MTarget : MBlock
    {
        public MTarget()
            : base("target")
        {
            this.type = "target";
        }

        public MTarget cloneInstance()
        {
            MTarget newInstance = new MTarget();
            newInstance.copy(this);
            return newInstance;
        }
    }

    public class MLight : MBlock
    {

        public MLight()
            : base("light")
        {
            this.type = "light";
            this.color = Color.white;
        }

        public MLight cloneInstance()
        {
            MLight newInstance = new MLight();
            newInstance.copy(this);
            return newInstance;
        }
    }

    public class MColorBlock : MBlock
    {

        public MColorBlock()
            : base("color")
        {
            this.type = "color";
        }

        public MColorBlock cloneInstance()
        {
            MColorBlock newInstance = new MColorBlock();
            newInstance.copy(this);
            return newInstance;
        }
    }
}
