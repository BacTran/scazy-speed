﻿using UnityEngine;
using System.Collections;
using SGGames.Levels.ObjectModel;
using SGGames.RayTracing;

public class LightRayGameLevel : Level
{
    public LightRayMap map;

    public LightRayGameLevel(int num)
        : base(num)
    {

    }
}
