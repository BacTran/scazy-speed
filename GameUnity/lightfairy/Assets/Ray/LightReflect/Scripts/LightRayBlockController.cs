﻿using UnityEngine;
using System.Collections;
using SGGames;
using SGGames.Block;

namespace SGGames.RayTracing
{
    public class LightRayBlockController : MonoBehaviour
    {
        private Vector3 screenPoint;
        private Vector3 offset;

        private Vector3 oldPos;
        public GameStage gameStage;
        private Vector2 lastMapPos = new Vector2(-9999, -9999);
        private Vector2 mapPos;
        private Vector2 dragMapPos;

        private bool dropable = false;
        private bool updateOnDrag = false;
        public MBlock thisBlock;
        public Plane groundPlane;

        void OnMouseDown()
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            oldPos = gameObject.transform.position;

            //Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            float rayDistance;
            Vector3 pointOnPlane = new Vector3();
            if (gameStage.groundPlane.Raycast(ray, out rayDistance))
            {
                pointOnPlane = ray.GetPoint(rayDistance);
            }
            //Debug.Log("Point on plane" + pointOnPlane);
            offset = gameObject.transform.position - pointOnPlane;
            mapPos = gameStage.toMapPos(pointOnPlane);

            //Debug.Log("mapPos" + mapPos);
            gameStage.clearRays();

        }

        void OnMouseDrag()
        {
            if (gameStage.game.started && !gameStage.game.completed)
            {
                //Only 2d
                //Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
                //Vector3 curWorldPoint = Camera.main.ScreenToWorldPoint(curScreenPoint);
                //Vector3 curPosition = curWorldPoint + offset;
                //Vector3 mouse3dPos = curWorldPoint;
                // align the pos to map
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
                float rayDistance;
                Vector3 pointOnPlane = new Vector3();
                if (gameStage.groundPlane.Raycast(ray, out rayDistance))
                {
                    pointOnPlane = ray.GetPoint(rayDistance);
                }
                //Debug.Log("Point on plane" + pointOnPlane);
                dragMapPos = gameStage.toMapPos(pointOnPlane);
                /*
                if (!(mapPos.x==lastMapPos.x&&mapPos.y==lastMapPos.y)){
                    Debug.Log(" Mouse " + mapPos.x+ " "+mapPos.y);
                    lastMapPos.x= mapPos.x;
                    lastMapPos.y = mapPos.y;
                }
                */
                //Debug.Log(" Mouse " + mapPos.x + " " + mapPos.y);

                var block = gameStage.map.getBlock(Mathf.RoundToInt(dragMapPos.x), Mathf.RoundToInt(dragMapPos.y));
                // ask if that target pos is free
                if (block.type == "empty")
                {
                    //Vector3 newWorldPos = gameStage.toWorldPosFromGrid(Mathf.RoundToInt(mapPos.x), Mathf.RoundToInt(mapPos.y));

                    transform.position = pointOnPlane;
                    dropable = true;

                    if (updateOnDrag)
                    {
                        if (mapPos.x != dragMapPos.x || mapPos.y != dragMapPos.y)
                        {
                            //Debug.Log("Change map and update");
                            transform.position = gameStage.toWorldPosFromGrid(Mathf.RoundToInt(dragMapPos.x), Mathf.RoundToInt(dragMapPos.y));
                            gameStage.map.moveBlock(thisBlock, dragMapPos);

                            gameStage.updateMap();
                        }
                    }
                }
                else
                {
                    backToOldMapPos();
                    dropable = false;
                }

            }
            else
            {
                //Debug.Log("Game completed!");
            }

        }

        void OnMouseUp()
        {
            if (gameStage.game.started && !gameStage.game.completed)
            {
                if (dropable)
                {
                    // if yes, snap
                    transform.position = gameStage.toWorldPosFromGrid(Mathf.RoundToInt(dragMapPos.x), Mathf.RoundToInt(dragMapPos.y));
                    gameStage.map.moveBlock(thisBlock, dragMapPos);
                    //

                    if (gameStage.audioSource != null && gameStage.soundDrop != null)
                    {
                        gameStage.audioSource.PlayOneShot(gameStage.soundDrop);
                    }
                }
                else
                {
                    backToOldMapPos();
                }
                gameStage.updateMap();
            }
            else
            {
                Debug.Log("Game completed!");
            }
        }

        private void backToOldMapPos()
        {
            transform.position = gameStage.toWorldPosFromGrid(Mathf.RoundToInt(mapPos.x), Mathf.RoundToInt(mapPos.y));
            gameStage.map.moveBlock(thisBlock, mapPos);

            if (gameStage.audioSource != null && gameStage.soundBack != null)
            {
                //gameStage.audioSource.PlayOneShot(gameStage.soundBack);
            }
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}