﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;

namespace SGGames.RayTracing
{

    [CustomEditor(typeof(LightRayLevelManagerCustomization))]
    public class LevelManagerEditor : Editor
    {
        string t1 = "123";
        string t2 = "123";

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            t1 = EditorGUILayout.TextField("t1", t1);
            t2 = EditorGUILayout.TextField("t2", t2);

            if (GUILayout.Button("Convert to tmx"))
            {
                ConvertAllToTmx();
            }


        }

        private void ConvertAllToTmx()
        {
            LightRayLevelManagerCustomization levelCustom = target as LightRayLevelManagerCustomization;

            foreach (TextAsset textAsset in levelCustom.levelData)
            {
                StreamWriter writer = File.CreateText("H://TMX/Tile" + textAsset.name + ".xml");
                writer.Write(t1);
                writer.Write(ConvertToTileIndex(textAsset.text));
                writer.Write(t2);
                writer.Close();
            }

            //Debug.Log("Path ");

        }

        private string ConvertToTileIndex(string oldText)
        {
            string result = "";
            string[] datalines = oldText.Split(new char[] { '\n' });
            int width = datalines[0].Length - 1;
            int height = datalines.Length;

            //Debug.Log("CREATE LEVEL : " + width + " " + height);

            // Loop through the level data and build the level by examining each item's neighbors
            for (int j = 0; j < height; j++)
            {
                for (int i = 0; i < width; i++)
                {
                    //Debug.Log(" create block"+i+" "+j+" " + datalines[i][j]);
                    // Make walls
                    string aBlock = "" + datalines[i][j];
                    result += LightRayMap.fromTextToTiled(aBlock);
                    if (!(j == height-1 && i == width-1))
                    {
                        result += ",";
                    }
                }
                result += '\n';
            }
            //Debug.Log(result);
            return result;
        }

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}