﻿using UnityEngine;
using System;
using System.Collections.Generic;
using SGGames.Block;
using SGGames.Levels;

namespace SGGames.RayTracing
{
    public class LightRayGame : MonoBehaviour, IRayTracer
    {
        //public LightRayMap map;
        public List<MRay> rays = new List<MRay>();
        public List<MLight> lights = new List<MLight>();
        public List<MRayPath> paths = new List<MRayPath>();
        public UILabel levelLabel;
        public UILabel levelLabel2;
        public UILabel timeLabel;

        public GameStage gameStage;
        private GUIController guiController;

        private bool winState = false;
        public bool started = false;
        public bool completed = false;
        public bool paused = false;
        //public GameObject mouseSparkle;

        private float passedTime = 0;
        public LightRayGameLevel currentLevel;
        public AdsController adsController;

        void Awake()
        {
            guiController = FindObjectOfType<GUIController>();
            if (adsController == null)
            {
                adsController = GetComponent<AdsController>();
                if (adsController == null)
                {
                    adsController = FindObjectOfType<AdsController>();
                }
            }
        }

        void Start()
        {
            if (adsController != null)
            {
                adsController.HideBanner();
            }
            gameStage = GetComponent<GameStage>();
            StartGame(LevelManager.Instance.GetCurrentLevel() as LightRayGameLevel);
        }

        public bool CheckContinue()
        {
            return true;
        }
        public void RestartLevel()
        {
            if (adsController != null)
            {
                adsController.HideBanner();
            }
            StartGame(LevelManager.Instance.GetCurrentLevel() as LightRayGameLevel);
        }
        public void Reset()
        {
        }

        public void StartGame(LightRayGameLevel level)
        {
            if (adsController != null)
            {
                adsController.HideBanner();
            }
            passedTime = 0;
            gameStage.resetStage();

            if (level != null)
            {
                currentLevel = level;
                if (!level.IsUnlocked)
                {
                    level.Unlock();
                }
                if (levelLabel != null)
                {
                    levelLabel.text = "Level " + level.Number;
                }
                if (levelLabel2 != null)
                {
                    levelLabel2.text = "" + level.Number;
                }
                gameStage.startMap(level.map, () =>
                {
                    started = true;
                    completed = false;
                    paused = false;
                });
            }
        }

        void Update()
        {
            if (started)
            {
                if (!completed)
                {
                    if (!paused)
                    {
                        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2);
                        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint);
                        completed = CheckComplete();
                        UpdateTime();
                    }
                    else
                    {
                        timeLabel.text = "Paused";
                    }
                }
            }
        }

        private void UpdateTime()
        {
            passedTime += Time.deltaTime;

            if (timeLabel != null)
            {
                int timeInSeconds = Mathf.RoundToInt(passedTime);
                int min = (timeInSeconds / 60);
                int sec = timeInSeconds - 60 * min;

                if (min < 100)
                {
                    timeLabel.text = "" + ((min < 10) ? ("0" + min) : "" + min) + ":" + ((sec < 10) ? ("0" + sec) : "" + sec);
                }
                else
                {
                    timeLabel.text = "";
                }
            }
        }

        public void OnWin()
        {
            adsController.ShowBanner();
            //Show 
            LeanTween.delayedCall(1, () =>
            {

                guiController.ShowWinDialog(() =>
                {
                    LevelManager.Instance.NextLevel();
                    StartGame(LevelManager.Instance.Level as LightRayGameLevel);
                }, () =>
                {
                    StartGame(LevelManager.Instance.Level as LightRayGameLevel);
                });
            });
        }

        public bool CheckComplete()
        {
            gameStage.map.checkWinState();

            if (gameStage.map.winState)
            {
                Debug.Log("Win");
                this.winState = true;
                OnWin();
            }
            else
            {
                this.winState = false;
            }

            return this.winState;
        }

        public MRay CreateRay(int x, int y)
        {
            MRay newRay = new MRay();
            newRay.path = new MRayPath();
            newRay.currentBlock = gameStage.map.getBlock(x, y).clone();
            newRay.startBlock = gameStage.map.getBlock(x, y).clone();
            newRay.enable = true;
            newRay.finish = false;
            return newRay;
        }
        public MRay CreateRay(MLight aLight)
        {
            MRay newRay = CreateRay(aLight.x, aLight.y);
            newRay.name = "Light " + gameStage.map.lights.IndexOf(aLight) + "/Ray";
            newRay.direction = aLight.direction;
            newRay.color = Color.white;
            rays.Add(newRay);
            return newRay;
        }

        public MRay CreateChildRay(MRay pRay, String tail)
        {
            var newRay = CreateRay(pRay.currentBlock.x, pRay.currentBlock.y);
            newRay.name = pRay.name + "/Childray " + tail;
            newRay.parent = pRay;
            newRay.direction = pRay.direction;
            rays.Add(newRay);
            return newRay;
        }
        
        public MRay GetAllUncastRay()
        {
            foreach (MRay ray in rays)
            {
                if (ray.finish != true)
                {
                    return ray;
                }
            }
            return null;
        }

        public void CastAllRays()
        {
            foreach (MBlock target in gameStage.map.getAllBlockByType("target"))
            {
                //Reset to 0 before cast rays
                target.status = 0;
            }

            foreach (MLight aLight in gameStage.map.lights)
            {
                if (aLight != null)
                {
                    CreateRay(aLight);
                }
            }
            int countRay = 0;
            int countLoop = 0;

            while (GetAllUncastRay() != null)
            {
                countLoop++;
                if (countLoop > 1000) break;
                MRay ray = GetAllUncastRay();
                // loop through all the rays 
                int index = rays.IndexOf(ray);
                countRay++;
             
                ray.startBlock.outDir = ray.direction;
                ray.path.blocks.Add(ray.startBlock);
                while (ray.finish == false)
                {
                    //process the ray upward
                    MBlock block = new MBlock();
                    //test the next block
                    MBlock originBlock = gameStage.map.getNextBlock(ray);
                    
                    block.copy(originBlock);
                    ray.currentBlock = block;
                    ray.path.blocks.Add(block);
                    block.inDir = ray.direction;

                    if (block.type == "wall")
                    {
                        //end
                        ray.finish = true;
                    }
                    else if (block.type == "target")
                    {
                        ray.finish = true;
                        if (ray.color == block.color)
                        {
                            originBlock.status = 1;
                            block.status = 1;
                        }
                        else
                        {
                            originBlock.status = 0;
                            block.status = 0;
                        }
                    }
                    else if (block.type == "light")
                    {
                        ray.finish = true;
                    }
                    else if (block.type == "mirror")
                    {
                        // mirror
                        // first, detect the "infinite reflection" problem
                        // change the direction of the ray
                        //Debug.Log("Mirror " + ray.direction);
                        ray.finish = true;

                        MRay reflectRay = CreateChildRay(ray, "ref");
                        reflectRay.color = ray.color;
                        if (block.direction == "/")
                        {
                            switch (ray.direction)
                            {
                                case "up": reflectRay.direction = "left"; break;
                                case "right": reflectRay.direction = "down"; break;
                                case "down": reflectRay.direction = "right"; break;
                                case "left": reflectRay.direction = "up"; break;
                            }
                        }
                        else if (block.direction == "\\")
                        {
                            switch (ray.direction)
                            {
                                case "up": reflectRay.direction = "right"; break;
                                case "right": reflectRay.direction = "up"; break;
                                case "down": reflectRay.direction = "left"; break;
                                case "left": reflectRay.direction = "down"; break;
                            }
                        }
                        block.outDir = reflectRay.direction;
                    }
                    else if (block.type == "Xsplit")
                    {
                        // make three more rays with RGB colors
                        ray.finish = true;

                        if (ray.direction == block.direction)
                        {
                            MRay rayR = CreateChildRay(ray, "r");
                            MRay rayG = CreateChildRay(ray, "g");
                            MRay rayB = CreateChildRay(ray, "b");
                            rayR.color = Color.red;
                            rayG.color = Color.green;
                            rayB.color = Color.blue;
                            rayR.direction = Directions.intToDirection(Directions.directionToInt(ray.direction) + 1);
                            rayG.direction = Directions.intToDirection(Directions.directionToInt(ray.direction));
                            rayB.direction = Directions.intToDirection(Directions.directionToInt(ray.direction) + 3);
                        }
                    }
                    else if (block.type == "Tsplit")
                    {
                        // make 2 more rays
                        ray.finish = true;
                        if (ray.direction == block.direction)
                        {
                            var ray1 = CreateChildRay(ray, "1");
                            var ray2 = CreateChildRay(ray, "2");
                            ray1.color = ray.color;
                            ray2.color = ray.color;
                            ray1.direction = Directions.intToDirection(Directions.directionToInt(ray.direction) + 1);
                            ray2.direction = Directions.intToDirection(Directions.directionToInt(ray.direction) + 3);
                        }
                    }
                    else if (block.type == "color")
                    {
                        ray.finish = true;

                        if (ray.color == Color.white)
                        {
                            var newRay = CreateChildRay(ray, "color");
                            newRay.direction = ray.direction;
                            newRay.color = block.color;
                        }
                    }
                }
            }
            gameStage.map.checkWinState();
        }

        public void LetResume()
        {
            LeanTween.resumeAll();
            paused = false;
        }
        public void LetPause()
        {
            //LeanTween.pauseAll();
            if (paused)
            {
                paused = false;
            }
            else
            {
                paused = true;
            }
        }

        public bool NextLevel()
        {
            return false;
        }

        public List<IRayCaster> Sources()
        {
            throw new NotImplementedException();
        }

        public void ClearRays()
        {
            rays.Clear();
        }

        public bool CheckEnd()
        {
            throw new NotImplementedException();
        }


        public List<IRayUnit> Rays()
        {
            return rays.ConvertAll(r => (IRayUnit)r); ;
        }

        public List<IRayPath> Paths()
        {
            return paths.ConvertAll(p => (IRayPath)p); ;
        }
    }
}